package com.ag.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ag.service.SalesOrderManager;

@Controller
@RequestMapping("/manage/salesOrders*")
public class SalesOrderController {
	private SalesOrderManager salesOrderManager;
	
	@Autowired
	public void setSalesOrderManager(SalesOrderManager salesOrderManager) {
		this.salesOrderManager = salesOrderManager;
	}

	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showList(HttpServletRequest request,
									HttpServletResponse response){
		Model model = new ExtendedModelMap();
		model.addAttribute("salesOrders", salesOrderManager.getAll());
		
		return new ModelAndView("manage/salesOrders", model.asMap());
	}
}
