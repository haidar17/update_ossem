package com.ag.webapp.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.BillingAddress;
import com.ag.model.BusinessCustomer;
import com.ag.model.ContactPerson;
import com.ag.model.Customer;
import com.ag.model.CustomerForm;
import com.ag.model.DeliveryAddress;
import com.ag.service.CustomerManager;

@Controller
@RequestMapping("/manage/customersForm*")
public class CustomerFormController extends BaseFormController {
	private CustomerManager customerManager;
	
	@Autowired
    public void setCustomerManager(CustomerManager customerManager) {
		this.customerManager = customerManager;
	}

	public CustomerFormController() {
        //setCancelView("redirect:/home");
        setCancelView("redirect:/manage/customers");
        setSuccessView("redirect:/manage/customers");
    }

    @Override
    @InitBinder
    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
        super.initBinder(request, binder);
    }
    
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
									@RequestParam(required=false, value="method") final String method,
									@RequestParam(required=false, value="from")  final String from,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		
		Model model = new ExtendedModelMap();

		Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
		if (inputFlashMap != null){
			if (inputFlashMap.containsKey("customerForm")){
				model.addAttribute("customerForm", (CustomerForm)inputFlashMap.get("customerForm"));
			}
			if (inputFlashMap.containsKey("customerForm_errors")){
				model.addAttribute("org.springframework.validation.BindingResult.customerForm", (BindingResult)inputFlashMap.get("customerForm_errors"));
			}			
		}

		
		if (!model.containsAttribute("customerForm")){
			CustomerForm form =  new CustomerForm();
			if (id != null){
				Customer customer = customerManager.get(id);
				form =  new CustomerForm(customer);
			}			
			model.addAttribute("customerForm", form);
		}
		
		return new ModelAndView("manage/customerForm", model.asMap());
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(@ModelAttribute("customerForm") CustomerForm customerForm,
								BindingResult errors, 
								RedirectAttributes redirectAttributes,
								HttpServletRequest request,
								HttpServletResponse response) throws Exception {
		final Locale locale = request.getLocale();
		
		// if cancel button click
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
 
        // if delete button click
        if (request.getParameter("delete") != null) {  
        	try {
        		customerManager.remove(customerForm.getId());
            	saveMessage(request, getText("customerForm.deleted", customerForm.getFullName(), locale));
            	
	    	} catch (DataIntegrityViolationException e){	
	    		saveError(request, String.format("Only unreferenced customer record can be deleted."));	
	    		redirectAttributes.addFlashAttribute("customerForm", customerForm);
				redirectAttributes.addFlashAttribute("customerForm_errors", errors);
				
				return "redirect:/manage/customersForm";
				
	    	} catch (Exception e){
	    		saveError(request, String.format("Customer record could not be deleted due to %s", e.getMessage()));	
	    		redirectAttributes.addFlashAttribute("customerForm", customerForm);
				redirectAttributes.addFlashAttribute("customerForm_errors", errors);
				
				return "redirect:/manage/customersForm";       		
	    	}
            return getSuccessView();
        } 
        
		if (validator != null){
			validator.validate(customerForm, errors);
			
			if (errors.hasErrors()){
				redirectAttributes.addFlashAttribute("customerForm", customerForm);
				redirectAttributes.addFlashAttribute("customerForm_errors", errors);
				
				return "redirect:/manage/customersForm";
			}
		}
		
		boolean isNew = true;
		Customer customer = null;
		if (customerForm.getId() != null){
			isNew = false;
			customer = customerManager.get(customerForm.getId());
		} else {
			customer = new BusinessCustomer();
		}
		// manipulate form for saving new or update existing
		customer.setFullName(customerForm.getFullName());
		customer.setLegalIdentity(customerForm.getLegalIdentity());
		
		// Contact Person
		ContactPerson contactPerson = customer.getContactPerson();
		if (contactPerson == null) contactPerson = new ContactPerson();
		
		contactPerson.setFirstName(customerForm.getContactFirstName());
		contactPerson.setLastName(customerForm.getContactLastName());
		contactPerson.setFullName(String.format("%s %s", contactPerson.getFirstName(), contactPerson.getLastName()));
		contactPerson.setContactEmail(customerForm.getContactEmail());
		contactPerson.setContactMobilePhone(customerForm.getContactMobilePhone());		
		
		customer.setContactPerson(contactPerson);
		
		// Delivery Address
		DeliveryAddress deliveryAddress = new DeliveryAddress();
		if (customerForm.getDeliveryAddressId() != null){
			deliveryAddress = customerManager.getDeliveryAddressById(customerForm.getDeliveryAddressId());
		}
		deliveryAddress.setAddress(customerForm.getDeliveryAddress());
		deliveryAddress.setStreet(customerForm.getDeliveryStreet());
		deliveryAddress.setPostalCode(customerForm.getDeliveryPostalCode());
		deliveryAddress.setCity(customerForm.getDeliveryCity());
		
		customer.setDeliveryAddress(deliveryAddress);
		// Billing Address
		
		BillingAddress billingAddress = new BillingAddress();
		if (customerForm.getBillingAddressId() != null){
			billingAddress = customerManager.getBillingAddressById(customerForm.getBillingAddressId());
		}		
		billingAddress.setAddress(customerForm.getBillingAddress());
		billingAddress.setStreet(customerForm.getBillingStreet());
		billingAddress.setPostalCode(customerForm.getBillingPostalCode());
		billingAddress.setCity(customerForm.getBillingCity());
		
		customer.setBillingAddress(billingAddress);		
		
		try {		
			customerManager.save(customer);
			saveMessage(request, getText((isNew ? "customerForm.created":"customerForm.updated"), customerForm.getFullName(), locale));

//		} catch (DataIntegrityViolationException e){
//			errors.addError(new FieldError(errors.getObjectName(), "legalIdentity", "Customer already exists!"));
//			redirectAttributes.addFlashAttribute("customerForm", customerForm);
//			redirectAttributes.addFlashAttribute("customerForm_errors", errors);
//			return "redirect:/manage/customersForm";
			
		} catch (Exception e){
			saveError(request, e.getCause().getLocalizedMessage());
			e.printStackTrace();

		}
		
		return getSuccessView();
	}
}
