package com.ag.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ag.dao.SearchException;
import com.ag.service.TemplateManager;
/**
 * This is controller template for other domain
 * You may copy and build your own however you see it fit your intended domain. 
 * Important: Please do not edit or update any part of this file.
 * 
 * @author hamid
 *
 */
@Controller
@RequestMapping("/manage/templates*")
public class TemplateController {
	protected final static Log log = LogFactory.getLog(TemplateController.class);
	private TemplateManager templateManager;
	
	@Autowired
	public void setTemplateManager(TemplateManager templateManager) {
		this.templateManager = templateManager;
	}	
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showList(@RequestParam(required = false, value = "q") String query,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		log.debug("Entered showList...");
		Model model = new ExtendedModelMap();
		if (query != null && !StringUtils.isEmpty(query)){
			try {
				log.debug("perform searching of [" + query + "] ...");
	            model.addAttribute("templates", templateManager.search(query));
	            log.debug("search completed!");
	            log.debug(model.asMap().get("templates"));
	            
	        } catch (SearchException se) {
	            model.addAttribute("searchError", se.getMessage());
	            model.addAttribute("templates", templateManager.getAll());
	            
	        }	
		} else {
			// not performing search.
			model.addAttribute("templates", templateManager.getAll());
		}
	
		return new ModelAndView("manage/templates", model.asMap());
	}

}
