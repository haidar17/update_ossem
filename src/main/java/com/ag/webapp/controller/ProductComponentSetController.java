package com.ag.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ag.dao.SearchException;
import com.ag.service.ProductComponentSetManager;
/**
 * @author hamid
 *
 */
@Controller
@RequestMapping("/manage/productComponentSets*")
public class ProductComponentSetController {
	protected final static Log log = LogFactory.getLog(ProductComponentSetController.class);
	private ProductComponentSetManager productComponentSetManager;
	
	@Autowired
	public void setProductComponentSetManager(ProductComponentSetManager productComponentSetManager) {
		this.productComponentSetManager = productComponentSetManager;
	}	
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showList(@RequestParam(required = false, value = "q") String query,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		log.debug("Entered showList...");
		Model model = new ExtendedModelMap();
		if (query != null && !StringUtils.isEmpty(query)){
			try {
				log.debug("perform searching of [" + query + "] ...");
	            model.addAttribute("productComponentSets", productComponentSetManager.search(query));
	            log.debug("search completed!");
	            log.debug(model.asMap().get("productComponentSets"));
	            
	        } catch (SearchException se) {
	            model.addAttribute("searchError", se.getMessage());
	            model.addAttribute("productComponentSets", productComponentSetManager.getAllDistinct());
	            
	        }	
		} else {
			// not performing search.
			model.addAttribute("productComponentSets", productComponentSetManager.getAllDistinct());
		}
	
		return new ModelAndView("manage/productComponentSets", model.asMap());
	}

}
