package com.ag.webapp.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.Product;
import com.ag.model.ProductForm;
import com.ag.model.ProductSizeSet;
import com.ag.service.ProductManager;

@Controller
@RequestMapping("/manage/productsForm*")
public class ProductFormController extends BaseFormController {
	private ProductManager productManager;
	
	@Autowired
    public void setProductManager(ProductManager productManager) {
		this.productManager = productManager;
	}

	public ProductFormController() {
        //setCancelView("redirect:/home");
		setCancelView("redirect:/manage/products");
        setSuccessView("redirect:/manage/products");
    }

    @Override
    @InitBinder
    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
        super.initBinder(request, binder);
    }
    
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
									@RequestParam(required=false, value="method") final String method,
									@RequestParam(required=false, value="from")  final String from,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		
		Model model = new ExtendedModelMap();

		Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
		if (inputFlashMap != null){
			if (inputFlashMap.containsKey("productForm")){
				model.addAttribute("productForm", (ProductForm)inputFlashMap.get("productForm"));
			}
			if (inputFlashMap.containsKey("productForm_errors")){
				model.addAttribute("org.springframework.validation.BindingResult.productForm", (BindingResult)inputFlashMap.get("productForm_errors"));
			}			
		}

		if (!model.containsAttribute("productForm")){
			ProductForm form =  new ProductForm();
			if (id != null){
				Product product = productManager.get(id);
				form =  new ProductForm(product);
			}
			model.addAttribute("productForm", form);
		}
		
		model.addAttribute("productSizeSets", productManager.getAllDistinctProductSizeSets());
		
		model.addAttribute("productComponentSets", productManager.getAllDistinctProductComponentSets());
		
		return new ModelAndView("manage/productForm", model.asMap());
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(@ModelAttribute("productForm") ProductForm productForm,
								BindingResult errors, 
								RedirectAttributes redirectAttributes,
								HttpServletRequest request,
								HttpServletResponse response) throws Exception {
		final Locale locale = request.getLocale();
		
		// if cancel button click
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
 
        // if delete button click
        if (request.getParameter("delete") != null) {  
        	try {
        		productManager.remove(productForm.getId());
        		saveMessage(request, getText("productForm.deleted", productForm.getName(), locale));
        		
        	} catch (DataIntegrityViolationException e){	
        		saveError(request, String.format("Only unreferenced product record can be deleted."));	
        		redirectAttributes.addFlashAttribute("productForm", productForm);
				redirectAttributes.addFlashAttribute("productForm_errors", errors);
				
				return "redirect:/manage/productsForm";
				
        	} catch (Exception e){
        		saveError(request, String.format("Product record could not be deleted due to %s", e.getMessage()));	
        		redirectAttributes.addFlashAttribute("productForm", productForm);
				redirectAttributes.addFlashAttribute("productForm_errors", errors);
				
				return "redirect:/manage/productsForm";       		
        	}
            return getSuccessView();
        } 
        
		if (validator != null){
			validator.validate(productForm, errors);
			
			if (errors.hasErrors()){
				redirectAttributes.addFlashAttribute("productForm", productForm);
				redirectAttributes.addFlashAttribute("productForm_errors", errors);
				
				return "redirect:/manage/productsForm";
			}
		}
		
		boolean isNew = true;
		Product product = new Product();
		if (productForm.getId() != null){
			product = productManager.get(productForm.getId());
		}
		// manipulate form for saving new or update existing
		//product.setName(productForm.getName());
		product.setCode(productForm.getCode());
		product.setDescription(productForm.getDescription());
		
		if (productForm.getProductSizeSetId() != null){
			product.setProductSizeSet(productManager.getProductSizeSetById(productForm.getProductSizeSetId()));
		} else {
			product.setProductSizeSet(null);
		}
		
		if (productForm.getProductComponentSetId() != null){
			product.setProductComponentSet(productManager.getProductComponentSetById(productForm.getProductComponentSetId()));
		} else {
			product.setProductComponentSet(null);
		}
		
		
		
		try {
			productManager.save(product);
			saveMessage(request, getText((isNew ? "productForm.created":"productForm.updated"), productForm.getName(), locale));

		} catch (DataIntegrityViolationException e){
			errors.addError(new FieldError(errors.getObjectName(), "code", "Product code already exists!"));
    		redirectAttributes.addFlashAttribute("productForm", productForm);
			redirectAttributes.addFlashAttribute("productForm_errors", errors);			
			return "redirect:/manage/productsForm";
			
		} catch (Exception e){
			saveError(request, e.getCause().getLocalizedMessage());
			e.printStackTrace();

		}
		
		return getSuccessView();
	}
}
