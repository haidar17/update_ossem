package com.ag.webapp.controller;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.Product;
import com.ag.model.ProductSize;
import com.ag.model.ProductSizeSet;
import com.ag.model.ProductSizeSetForm;
import com.ag.model.SalesOrder;
import com.ag.model.SalesOrderItem;
import com.ag.service.ProductSizeSetManager;

/**
 * @author hamid
 *
 */
@Controller
@RequestMapping("/manage/productSizeSetsForm*")
public class ProductSizeSetFormController extends BaseFormController {
		private static Pattern PRODUCT_SIZE_ID_PATTERN = Pattern.compile("productSize-[0-9]+-id$", Pattern.MULTILINE);

		private ProductSizeSetManager productSizeSetManager;
		
		@Autowired
	    public void setProductSizeSetManager(ProductSizeSetManager productSizeSetManager) {
			this.productSizeSetManager = productSizeSetManager;
		}

		public ProductSizeSetFormController() {
	        //setCancelView("redirect:/home");
			setCancelView("redirect:/manage/productSizeSets");
	        setSuccessView("redirect:/manage/productSizeSets");
	    }

	    @Override
	    @InitBinder
	    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
	        super.initBinder(request, binder);
	    }
	    
		@RequestMapping(method=RequestMethod.GET)
		public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
										@RequestParam(required=false, value="method") final String method,
										@RequestParam(required=false, value="from")  final String from,
										HttpServletRequest request,
										HttpServletResponse response) throws Exception {
			
			Model model = new ExtendedModelMap();

			Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
			if (inputFlashMap != null){
				if (inputFlashMap.containsKey("productSizeSetForm")){
					model.addAttribute("productSizeSetForm", (ProductSizeSetForm)inputFlashMap.get("productSizeSetForm"));
				}
				if (inputFlashMap.containsKey("productSizeSetForm_errors")){
					model.addAttribute("org.springframework.validation.BindingResult.productSizeSetForm", (BindingResult)inputFlashMap.get("productSizeSetForm_errors"));
				}			
			}

			if (!model.containsAttribute("productSizeSetForm")){
				ProductSizeSetForm form =  new ProductSizeSetForm();
				if (id != null){
					form = new ProductSizeSetForm(productSizeSetManager.get(id));
				}
				model.addAttribute("productSizeSetForm", form);
			}
			
			return new ModelAndView("manage/productSizeSetForm", model.asMap());
			
		}
		
		@RequestMapping(method=RequestMethod.POST)
		public String onSubmit(@ModelAttribute("productSizeSetForm") ProductSizeSetForm productSizeSetForm,
									BindingResult errors, 
									RedirectAttributes redirectAttributes,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
			final Locale locale = request.getLocale();
			logRequestParams(request);
			// if cancel button click
	        if (request.getParameter("cancel") != null) {
	            if (!StringUtils.equals(request.getParameter("from"), "list")) {
	                return getCancelView();
	            } else {
	                return getSuccessView();
	            }
	        }
	 
	        // if delete button click
	        if (request.getParameter("delete") != null) {  
	        	try {
	        		productSizeSetManager.remove(productSizeSetForm.getId());
	        		saveMessage(request, getText("productSizeSetForm.deleted", productSizeSetForm.getName(), locale));
	        		
	        	} catch (Exception e){
		    		saveError(request, String.format("productSizeSet record could not be deleted due to %s", e.getMessage()));	
		    		redirectAttributes.addFlashAttribute("productSizeSetForm", productSizeSetForm);
					redirectAttributes.addFlashAttribute("productSizeSetForm_errors", errors);
					
					return "redirect:/manage/productSizeSetsForm";        		
	        	}
	            return getSuccessView();
	        } 
	        
			if (validator != null){
				validator.validate(productSizeSetForm, errors);
				
				if (errors.hasErrors()){
					redirectAttributes.addFlashAttribute("productSizeSetForm", productSizeSetForm);
					redirectAttributes.addFlashAttribute("productSizeSetForm_errors", errors);
					
					return "redirect:/manage/productSizeSetsForm";
				}
			}
			
			boolean isNew = true;
			ProductSizeSet productSizeSet = new ProductSizeSet();
			if (productSizeSetForm.getId() != null){
				productSizeSet = productSizeSetManager.get(productSizeSetForm.getId());
				isNew = false;
			}
			// manipulate form for saving new or update existing
			productSizeSet.setName(productSizeSetForm.getName());
			productSizeSet.setDescription(productSizeSetForm.getDescription());
			
			try {
				productSizeSet.setProductSizes(null);
				productSizeSet = productSizeSetManager.save(productSizeSet);
				
				// add product sizes
				productSizeSet.setProductSizes(getProductSizes(request, productSizeSet));
				
				// save product sizes
				productSizeSetManager.save(productSizeSet);
				
				saveMessage(request, getText((isNew ? "productSizeSetForm.created":"productSizeSetForm.updated"), productSizeSetForm.getName(), locale));

			} catch (Exception e){
				saveError(request, e.getCause().getMessage());
				e.printStackTrace();

			}
			
			return getSuccessView();
		}
		
		private Set<ProductSize> getProductSizes(
				HttpServletRequest request, ProductSizeSet productSizeSet) {
			log.debug("Entered getSalesOrderItems...");
			Set<ProductSize> productSizes = new HashSet<ProductSize>();
			Enumeration en = request.getParameterNames();
			
			while (en.hasMoreElements()){
				String name = (String)en.nextElement();
				log.debug("name=" + name);
				log.debug("PRODUCT_SIZE_ID_PATTERN.matcher(name).find()=" + PRODUCT_SIZE_ID_PATTERN.matcher(name).find());
				if (PRODUCT_SIZE_ID_PATTERN.matcher(name).find()){
					String prefix = name.replace("-code", "");
					String productSizeId = request.getParameter(name);
					log.debug("productSizeId=" + productSizeId);
					try {
						
						ProductSize productSize = productSizeSetManager.getProductSizeWithSetsById(productSizeId);
						if (productSize != null){
							//productSize.addProductSizeSet(productSizeSet);
							productSizes.add(productSize);
						}
						log.debug("productSizes.size=" + productSizes.size());
					} catch (Exception e){
						e.printStackTrace();
					}
				} 
			}
			return productSizes;
		}		
}
