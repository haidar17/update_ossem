package com.ag.webapp.controller;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.ProductComponent;
import com.ag.model.ProductComponentSet;
import com.ag.model.ProductComponentSetForm;
import com.ag.service.ProductComponentSetManager;

/**
 * @author hamid
 *
 */
@Controller
@RequestMapping("/manage/productComponentSetsForm*")
public class ProductComponentSetFormController extends BaseFormController {
		private static Pattern PRODUCT_COMPONENT_ID_PATTERN = Pattern.compile("productComponent-[0-9]+-id$", Pattern.MULTILINE);

		private ProductComponentSetManager productComponentSetManager;
		
		@Autowired
	    public void setProductComponentSetManager(ProductComponentSetManager productComponentSetManager) {
			this.productComponentSetManager = productComponentSetManager;
		}

		public ProductComponentSetFormController() {
	        //setCancelView("redirect:/home");
			setCancelView("redirect:/manage/productComponentSets");
	        setSuccessView("redirect:/manage/productComponentSets");
	    }

	    @Override
	    @InitBinder
	    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
	        super.initBinder(request, binder);
	    }
	    
		@RequestMapping(method=RequestMethod.GET)
		public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
										@RequestParam(required=false, value="method") final String method,
										@RequestParam(required=false, value="from")  final String from,
										HttpServletRequest request,
										HttpServletResponse response) throws Exception {
			
			Model model = new ExtendedModelMap();

			Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
			if (inputFlashMap != null){
				if (inputFlashMap.containsKey("productComponentSetForm")){
					model.addAttribute("productComponentSetForm", (ProductComponentSetForm)inputFlashMap.get("productComponentSetForm"));
				}
				if (inputFlashMap.containsKey("productComponentSetForm_errors")){
					model.addAttribute("org.springframework.validation.BindingResult.productComponentSetForm", (BindingResult)inputFlashMap.get("productComponentSetForm_errors"));
				}			
			}

			if (!model.containsAttribute("productComponentSetForm")){
				ProductComponentSetForm form =  new ProductComponentSetForm();
				if (id != null){
					form = new ProductComponentSetForm(productComponentSetManager.get(id));
				}
				model.addAttribute("productComponentSetForm", form);
			}
			
			return new ModelAndView("manage/productComponentSetForm", model.asMap());
			
		}
		
		@RequestMapping(method=RequestMethod.POST)
		public String onSubmit(@ModelAttribute("productComponentSetForm") ProductComponentSetForm productComponentSetForm,
									BindingResult errors, 
									RedirectAttributes redirectAttributes,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
			final Locale locale = request.getLocale();
			logRequestParams(request);
			// if cancel button click
	        if (request.getParameter("cancel") != null) {
	            if (!StringUtils.equals(request.getParameter("from"), "list")) {
	                return getCancelView();
	            } else {
	                return getSuccessView();
	            }
	        }
	 
	        // if delete button click
	        if (request.getParameter("delete") != null) {  
	        	try {
	        		productComponentSetManager.remove(productComponentSetForm.getId());
	        		saveMessage(request, getText("productComponentSetForm.deleted", productComponentSetForm.getName(), locale));
	        		
	        	} catch (Exception e){
		    		saveError(request, String.format("productComponentSet record could not be deleted due to %s", e.getMessage()));	
		    		redirectAttributes.addFlashAttribute("productComponentSetForm", productComponentSetForm);
					redirectAttributes.addFlashAttribute("productComponentSetForm_errors", errors);
					
					return "redirect:/manage/productComponentSetsForm";        		
	        	}
	            return getSuccessView();
	        } 
	        
			if (validator != null){
				validator.validate(productComponentSetForm, errors);
				
				if (errors.hasErrors()){
					redirectAttributes.addFlashAttribute("productComponentSetForm", productComponentSetForm);
					redirectAttributes.addFlashAttribute("productComponentSetForm_errors", errors);
					
					return "redirect:/manage/productComponentSetsForm";
				}
			}
			
			boolean isNew = true;
			ProductComponentSet productComponentSet = new ProductComponentSet();
			if (productComponentSetForm.getId() != null){
				productComponentSet = productComponentSetManager.get(productComponentSetForm.getId());
				isNew = false;
			}
			// manipulate form for saving new or update existing
			productComponentSet.setName(productComponentSetForm.getName());
			productComponentSet.setDescription(productComponentSetForm.getDescription());
			
			try {
				productComponentSet.setProductComponents(null);
				productComponentSet = productComponentSetManager.save(productComponentSet);
				
				// add product Components
				productComponentSet.setProductComponents(getProductComponents(request, productComponentSet));
				
				// save product Components
				productComponentSetManager.save(productComponentSet);
				
				saveMessage(request, getText((isNew ? "productComponentSetForm.created":"productComponentSetForm.updated"), productComponentSetForm.getName(), locale));

			} catch (Exception e){
				saveError(request, e.getCause().getMessage());
				e.printStackTrace();

			}
			
			return getSuccessView();
		}
		
		private Set<ProductComponent> getProductComponents(
				HttpServletRequest request, ProductComponentSet productComponentSet) {
			log.debug("Entered getSalesOrderItems...");
			Set<ProductComponent> productComponents = new HashSet<ProductComponent>();
			Enumeration en = request.getParameterNames();
			
			while (en.hasMoreElements()){
				String name = (String)en.nextElement();
				log.debug("name=" + name);
				log.debug("PRODUCT_COMPONENT_ID_PATTERN.matcher(name).find()=" + PRODUCT_COMPONENT_ID_PATTERN.matcher(name).find());
				if (PRODUCT_COMPONENT_ID_PATTERN.matcher(name).find()){
					String prefix = name.replace("-code", "");
					String productComponentId = request.getParameter(name);
					log.debug("productComponentId=" + productComponentId);
					try {
						
						ProductComponent productComponent = productComponentSetManager.getProductComponentWithSetsById(productComponentId);
						if (productComponent != null){
							productComponent.addProductComponentSet(productComponentSet);
							productComponents.add(productComponent);
						}
						log.debug("productComponents.component=" + productComponents);
					} catch (Exception e){
						e.printStackTrace();
					}
				} 
			}
			return productComponents;
		}		
}
