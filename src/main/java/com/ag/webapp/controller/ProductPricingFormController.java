package com.ag.webapp.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.Customer;
import com.ag.model.ProductPricing;
import com.ag.model.ProductPricingForm;
import com.ag.service.ProductPricingManager;

/**
 * @author hamid
 *
 */
@Controller
@RequestMapping("/manage/productPricingsForm*")
public class ProductPricingFormController extends BaseFormController {
		private ProductPricingManager productPricingManager;
		
		@Autowired
	    public void setProductPricingManager(ProductPricingManager productPricingManager) {
			this.productPricingManager = productPricingManager;
		}

		public ProductPricingFormController() {
	        //setCancelView("redirect:/home");
			setCancelView("redirect:/manage/productPricings");
	        setSuccessView("redirect:/manage/productPricings");
	    }

	    @Override
	    @InitBinder
	    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
	        super.initBinder(request, binder);
	    }
	    
		@RequestMapping(method=RequestMethod.GET)
		public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
										@RequestParam(required=false, value="customerId") final Long customerId,
										@RequestParam(required=false, value="method") final String method,
										@RequestParam(required=false, value="from")  final String from,
										HttpServletRequest request,
										HttpServletResponse response) throws Exception {
			
			Model model = new ExtendedModelMap();
			ProductPricingForm form =  new ProductPricingForm();
			
			Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
			if (inputFlashMap != null){
				if (inputFlashMap.containsKey("productPricingForm")){
					model.addAttribute("productPricingForm", (ProductPricingForm)inputFlashMap.get("productPricingForm"));
				}
				if (inputFlashMap.containsKey("productPricingForm_errors")){
					model.addAttribute("org.springframework.validation.BindingResult.productPricingForm", (BindingResult)inputFlashMap.get("productPricingForm_errors"));
				}			
			}

			if (!model.containsAttribute("productPricingForm")){
				if (id != null){
					form = new ProductPricingForm(productPricingManager.get(id));
					
					// find product pricings by customer
					model.addAttribute("customerProductPricings", productPricingManager.getByCustomer(form.getCustomerId()));					
				}
				model.addAttribute("productPricingForm", form);
			}
			
			if (customerId != null){
				Customer customer = productPricingManager.getCustomerById(customerId);
				model.addAttribute("productPricingForm", new ProductPricingForm(customer));
				model.addAttribute("customerProductPricings", productPricingManager.getByCustomer(customerId));	
			}

			
			return new ModelAndView("manage/productPricingForm", model.asMap());
			
		}
		
		@RequestMapping(method=RequestMethod.POST)
		public String onSubmit(@ModelAttribute("productPricingForm") ProductPricingForm productPricingForm,
				
									BindingResult errors, 
									RedirectAttributes redirectAttributes,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
			final Locale locale = request.getLocale();
			
			// if cancel button click
	        if (request.getParameter("cancel") != null) {
	            if (!StringUtils.equals(request.getParameter("from"), "list")) {
	                return getCancelView();
	            } else {
	                return getSuccessView();
	            }
	        }
	        
	        if (request.getParameter("form-cancel") != null) {
	        	if (productPricingForm.getCustomerId() != null){
	        		return String.format("redirect:/manage/productPricingsForm?customerId=%s&from=list&method=View", productPricingForm.getCustomerId());
	        	} else {
		        	return "redirect:/manage/productPricingsForm?from=list&method=View";
	        	}
	        }	        
	        
	        // if delete button click
	        if (request.getParameter("delete") != null) {  
	        	try {
	        		productPricingManager.remove(productPricingForm.getId());
	        		saveMessage(request, getText("productPricingForm.deleted", productPricingForm.getCustomerName(), locale));
	        	
	        	} catch (DataIntegrityViolationException e){	
	        		saveError(request, String.format("Only unreferenced product pricing record can be deleted."));	
	        		redirectAttributes.addFlashAttribute("productPricingForm", productPricingForm);
					redirectAttributes.addFlashAttribute("productPricingForm_errors", errors);
					
					return String.format("redirect:/manage/productPricingsForm?customerId=%s&from=list&method=Delete", productPricingForm.getCustomerId());
					
	        	} catch (Exception e){
		    		saveError(request, String.format("Product pricing record could not be deleted due to %s", e.getMessage()));	
		    		redirectAttributes.addFlashAttribute("productPricingForm", productPricingForm);
					redirectAttributes.addFlashAttribute("productPricingForm_errors", errors);
					
					return String.format("redirect:/manage/productPricingsForm?customerId=%s&from=list&method=Delete", productPricingForm.getCustomerId());        		
	        	}
	        	
				return String.format("redirect:/manage/productPricingsForm?customerId=%s&from=list&method=View", productPricingForm.getCustomerId());	        	
	            //return getSuccessView();
	        } 
	        
			if (validator != null){
				validator.validate(productPricingForm, errors);
				
				if (errors.hasErrors()){
					redirectAttributes.addFlashAttribute("productPricingForm", productPricingForm);
					redirectAttributes.addFlashAttribute("productPricingForm_errors", errors);
					
					return String.format("redirect:/manage/productPricingsForm?customerId=%s&form=list&method=Edit", (productPricingForm.getCustomerId() != null ? productPricingForm.getCustomerId():""));
				}
			}
			
			boolean isNew = true;
			ProductPricing productPricing = new ProductPricing();
			if (productPricingForm.getId() != null){
				productPricing = productPricingManager.get(productPricingForm.getId());
				isNew = false;
			}
			// manipulate form for saving new or update existing
			productPricing.setCustomer(productPricingManager.getCustomerById(productPricingForm.getCustomerId()));
			productPricing.setProduct(productPricingManager.getProductById(productPricingForm.getProductId()));
			productPricing.setPrice(productPricingForm.getPrice());
			
			try {
				productPricing = productPricingManager.save(productPricing);
				saveMessage(request, getText((isNew ? "productPricingForm.created":"productPricingForm.updated"), productPricingForm.getCustomerName(), locale));

			} catch (DataIntegrityViolationException e){	
	    		//saveError(request, String.format("There is an exiting customer's product price."));	
				errors.addError(new FieldError(errors.getObjectName(), "productCode", "Price with this product code existed!"));
	    		redirectAttributes.addFlashAttribute("productPricingForm", productPricingForm);
				redirectAttributes.addFlashAttribute("productPricingForm_errors", errors);
				
				return String.format("redirect:/manage/productPricingsForm?customerId=%s&from=list&method=Edit", productPricingForm.getCustomerId());
				
			} catch (Exception e){
				saveError(request, e.getCause().getLocalizedMessage());
				e.printStackTrace();

			}
			
			//return getSuccessView();
			return String.format("redirect:/manage/productPricingsForm?id=%s&customerId=%s&from=list&method=View", productPricing.getId(), productPricing.getCustomer().getId());
		}
}
