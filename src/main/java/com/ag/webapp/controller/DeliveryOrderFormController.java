package com.ag.webapp.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.BillingAddress;
import com.ag.model.DeliveryAddress;
import com.ag.model.DeliveryOrder;
import com.ag.model.DeliveryOrderForm;
import com.ag.model.SalesOrder;
import com.ag.model.SalesOrderForm;
import com.ag.service.DeliveryOrderManager;
import com.ag.service.DeliveryOrderNumberBinManager;

/**
 * 
 * @author hamid
 *
 */
@Controller
@RequestMapping("/manage/deliveryOrdersForm*")
public class DeliveryOrderFormController extends BaseFormController {
	private DeliveryOrderManager deliveryOrderManager;
	private DeliveryOrderNumberBinManager deliveryOrderNumberBinManager;	
	
	@Autowired
    public void setDeliveryOrderManager(DeliveryOrderManager deliveryOrderManager) {
		this.deliveryOrderManager = deliveryOrderManager;
	}

	@Autowired
	public void setDeliveryOrderNumberBinManager(DeliveryOrderNumberBinManager deliveryOrderNumberBinManager) {
		this.deliveryOrderNumberBinManager = deliveryOrderNumberBinManager;
	}
	
    @Value("#{deliveryOrderNumberBinProps['useLeadingZeros']}")
    private boolean isLeadingZeros;
    
    @Value("#{deliveryOrderNumberBinProps['numberOfLeadingZeros']}")
    private Integer numberOfLeadingZeros;
    
    @Value("#{deliveryOrderNumberBinProps['orderNumberPrefix']}")
    private String orderNoPrefix;
    
	public DeliveryOrderFormController() {
        //setCancelView("redirect:/home");
		setCancelView("redirect:/manage/deliveryOrders");
        setSuccessView("redirect:/manage/deliveryOrders");
    }

    @Override
    @InitBinder
    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
        super.initBinder(request, binder);
    }
    
    /**
     * 
     * @param id
     * @param method
     * @param from
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
									@RequestParam(required=false, value="method") final String method,
									@RequestParam(required=false, value="from")  final String from,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		
		log.debug("Entering DeliveryOrderFormController:showForm...");
		DeliveryOrderForm form = null;
		Model model = new ExtendedModelMap();

		Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
		if (inputFlashMap != null){
			if (inputFlashMap.containsKey("deliveryOrderForm")){
				model.addAttribute("deliveryOrderForm", (DeliveryOrderForm)inputFlashMap.get("deliveryOrderForm"));
			}
			if (inputFlashMap.containsKey("deliveryOrderForm_errors")){
				model.addAttribute("org.springframework.validation.BindingResult.deliveryOrderForm", (BindingResult)inputFlashMap.get("deliveryOrderForm_errors"));
			}			
		}

		if (!model.containsAttribute("deliveryOrderForm")){
			form =  new DeliveryOrderForm();
			if (id != null){
				form = new DeliveryOrderForm(deliveryOrderManager.getWithConfirmedSalesOrders(id));
			} else {
				form.setStatus('N');
				form.setOrderNo(String.format("%s-%s", orderNoPrefix, deliveryOrderNumberBinManager.getNextOrderNumber(isLeadingZeros, numberOfLeadingZeros)));
			}
			model.addAttribute("deliveryOrderForm", form);
		}	
		
		form = (DeliveryOrderForm)model.asMap().get("deliveryOrderForm");
		if (!form.isNewOrder()){
			// existing
			Set<SalesOrderForm> existingConfirmedSalesOrders = form.getConfirmedSalesOrders();
			
			// look for newer confirm sales orders
			if (form.isChangeConfirmedSalesOrderAllowed()){
				if (form.getCustomerId() != null){
					if (existingConfirmedSalesOrders == null) existingConfirmedSalesOrders = new HashSet<SalesOrderForm>();
					List<SalesOrder> newConfirmedSalesOrders = deliveryOrderManager.getConfirmedSalesOrdersByCustomerId(form.getCustomerId());
					if (newConfirmedSalesOrders != null && !newConfirmedSalesOrders.isEmpty()){
						for (SalesOrder salesOrder : newConfirmedSalesOrders){
							SalesOrderForm salesOrderForm = new SalesOrderForm(salesOrder);
							existingConfirmedSalesOrders.add(salesOrderForm);
						}
					} 
				}
			}
			
			model.addAttribute("confirmedSalesOrders", existingConfirmedSalesOrders);	
		}	

		
		log.debug("Leaving DeliveryOrderFormController:showForm...");
		return new ModelAndView("manage/deliveryOrderForm", model.asMap());
		
	}
	
	/**
	 * 
	 * @param deliveryOrderForm
	 * @param errors
	 * @param redirectAttributes
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(@ModelAttribute("deliveryOrderForm") DeliveryOrderForm deliveryOrderForm,
								BindingResult errors, 
								RedirectAttributes redirectAttributes,
								HttpServletRequest request,
								HttpServletResponse response) throws Exception {
		final Locale locale = request.getLocale();
		
		logRequestParams(request);
		// if cancel button click
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
 
        // if delete button click
        if (request.getParameter("delete") != null) {  
        	try {
        		deliveryOrderManager.remove(deliveryOrderForm.getId());
        		saveMessage(request, getText("deliveryOrderForm.deleted", deliveryOrderForm.getCustomerFullName(), locale));
        		
        	} catch (Exception e){
	    		saveError(request, String.format("Product delivery order could not be deleted due to %s", e.getMessage()));	
	    		redirectAttributes.addFlashAttribute("deliveryOrderForm", deliveryOrderForm);
				redirectAttributes.addFlashAttribute("deliveryOrderForm_errors", errors);
				
				return "redirect:/manage/deliveryOrdersForm";        		
        	}
            return getSuccessView();
        } 
        
		if (validator != null){
			validator.validate(deliveryOrderForm, errors);
			
			if (errors.hasErrors()){
				redirectAttributes.addFlashAttribute("deliveryOrderForm", deliveryOrderForm);
				redirectAttributes.addFlashAttribute("deliveryOrderForm_errors", errors);
				
				return "redirect:/manage/deliveryOrdersForm";
			}
		}
		
		boolean isNew = true;
		DeliveryOrder deliveryOrder = new DeliveryOrder();
		if (deliveryOrderForm.getId() != null){
			deliveryOrder = deliveryOrderManager.getWithConfirmedSalesOrders(deliveryOrderForm.getId());
			isNew = false;
		} else {
			deliveryOrderForm.setStatus('D');
		}
		// manipulate form for saving new or update existing
		deliveryOrder.setOrderNo(deliveryOrderForm.getOrderNo());
		deliveryOrder.setOrderDate(deliveryOrderForm.getOrderDate());
		deliveryOrder.setReference(deliveryOrderForm.getReference());
		
		deliveryOrder.setCustomer(deliveryOrderForm.getCustomerId() != null ? deliveryOrderManager.getCustomerById(deliveryOrderForm.getCustomerId()):null);

		// Delivery Address
		if (deliveryOrderForm.getCustomerId() != null){
			DeliveryAddress deliveryAddress = new DeliveryAddress();
			if (deliveryOrderForm.getDeliveryAddressId() != null){
				deliveryAddress = deliveryOrderManager.getDeliveryAddressById(deliveryOrderForm.getDeliveryAddressId());
			}		
			deliveryAddress.setAddress(deliveryOrderForm.getDeliveryAddress());
			deliveryAddress.setStreet(deliveryOrderForm.getDeliveryStreet());
			deliveryAddress.setPostalCode(deliveryOrderForm.getDeliveryPostalCode());
			deliveryAddress.setCity(deliveryOrderForm.getDeliveryCity());
			
			deliveryOrder.setDeliveryAddress(deliveryAddress);
			// Billing Address
			
			BillingAddress billingAddress = new BillingAddress();
			if (deliveryOrderForm.getBillingAddressId() != null){
				billingAddress = deliveryOrderManager.getBillingAddressById(deliveryOrderForm.getBillingAddressId());
			}		
			billingAddress.setAddress(deliveryOrderForm.getBillingAddress());
			billingAddress.setStreet(deliveryOrderForm.getBillingStreet());
			billingAddress.setPostalCode(deliveryOrderForm.getBillingPostalCode());
			billingAddress.setCity(deliveryOrderForm.getBillingCity());
			deliveryOrder.setBillingAddress(billingAddress);	
		}
		
		if (deliveryOrderForm.getStatus() != null){
			if (deliveryOrderForm.getStatus() == 'A') {
				if (deliveryOrderForm.getCustomerId() == null){
					// reset status
					deliveryOrderForm.setStatus('N');
					if (deliveryOrder != null) deliveryOrderForm.setStatus(deliveryOrder.getStatus());
					
					errors.addError(new FieldError(errors.getObjectName(), "customerFullName", "A customer must be assigned to accept this delivery order!."));
					redirectAttributes.addFlashAttribute("deliveryOrderForm", deliveryOrderForm);
					redirectAttributes.addFlashAttribute("deliveryOrderForm_errors", errors);
					
					return String.format("redirect:/manage/deliveryOrdersForm?from=list&method=Edit&id=%s", (deliveryOrderForm.getId() != null ? deliveryOrderForm.getId():""));				
				}
			}
			if (deliveryOrderForm.getStatus() == 'C') {
				String[] selectedConfirmedSalesOrderIds = request.getParameterValues("selectedConfirmedSalesOrderId");
				if (selectedConfirmedSalesOrderIds == null){
					
					if (deliveryOrder != null) {
						deliveryOrderForm.setStatus(deliveryOrder.getStatus());
						deliveryOrderForm.setCustomerFullName(deliveryOrder.getCustomer().getFullName());
					}
					saveError(request, "At least one (1) sales order must be selected to confirm this delivery order!");	
					redirectAttributes.addFlashAttribute("deliveryOrderForm", deliveryOrderForm);
					redirectAttributes.addFlashAttribute("deliveryOrderForm_errors", errors);
					
					return String.format("redirect:/manage/deliveryOrdersForm?from=list&method=Edit&id=%s", (deliveryOrderForm.getId() != null ? deliveryOrderForm.getId():""));								
				}
			}
		}
		
		// set onlly after checking
		boolean isChangeConfirmedSalesOrderAllowed = deliveryOrder.isChangeConfirmedSalesOrderAllowed();
		deliveryOrder.setStatus(deliveryOrderForm.getStatus());
		

		try {
			deliveryOrder = deliveryOrderManager.save(deliveryOrder);
			
			// Set all selected sales order status to completed upon completion of this delivery order
			if (deliveryOrder.isCompletedOrder()){
				if (deliveryOrder.isCompletedOrder()){
					for (SalesOrder salesOrder:deliveryOrder.getConfirmedSalesOrders()){
						salesOrder.setStatus('E');
					}
					deliveryOrder = deliveryOrderManager.save(deliveryOrder);
				}	
			}
			
			// 
			if (isChangeConfirmedSalesOrderAllowed){
				// if confirm sales order selected
				// selected Confirm Sales Orders
				String[] selectedConfirmedSalesOrderIds = request.getParameterValues("selectedConfirmedSalesOrderId");
				if (selectedConfirmedSalesOrderIds != null && selectedConfirmedSalesOrderIds.length > 0){
					Set<SalesOrder> salesOrders = new HashSet<SalesOrder>();
					for (String id:selectedConfirmedSalesOrderIds){
						SalesOrder salesOrder = deliveryOrderManager.getSalesOrderById(Long.valueOf(id));
						salesOrder.setDeliveryOrder(deliveryOrder);
						salesOrders.add(salesOrder);
					}

					if (deliveryOrder.getConfirmedSalesOrders() != null && !deliveryOrder.getConfirmedSalesOrders().isEmpty()){
						Set<SalesOrder> diff = new HashSet<SalesOrder>(deliveryOrder.getConfirmedSalesOrders());
						diff.removeAll(salesOrders);
						for (SalesOrder so:diff){
							so.setDeliveryOrder(null);
						}
						salesOrders.addAll(diff);
					}	
					deliveryOrder.getConfirmedSalesOrders().addAll(salesOrders);
				} else {
					if (deliveryOrder.getConfirmedSalesOrders() != null && !deliveryOrder.getConfirmedSalesOrders().isEmpty()){
						for (SalesOrder so:deliveryOrder.getConfirmedSalesOrders()){
							so.setDeliveryOrder(null);
						}
					}
				}
				deliveryOrder = deliveryOrderManager.save(deliveryOrder);
				
			}

			saveMessage(request, getText((isNew ? "deliveryOrderForm.created":"deliveryOrderForm.updated"), deliveryOrderForm.getCustomerFullName(), locale));

		} catch (Exception e){
			saveError(request, e.getCause().getLocalizedMessage());
			e.printStackTrace();

		}
		
		//return getSuccessView();
		return String.format("redirect:/manage/deliveryOrdersForm?from=list&method=Edit&id=%s", (deliveryOrder.getId() != null ? deliveryOrder.getId():""));
	}	
}
