package com.ag.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/manage/salesReceipts*")
public class SalesReceiptController {
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showList(HttpServletRequest request,
									HttpServletResponse response){
		Model model = new ExtendedModelMap();
		
		return new ModelAndView("manage/salesReceipts", model.asMap());
	}
}
