package com.ag.webapp.controller;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.Product;
import com.ag.model.ProductionOrder;
import com.ag.model.ProductionOrderForm;
import com.ag.model.ProductionOrderItem;
import com.ag.model.ProductionOrderItemAdjustment;
import com.ag.model.ProductionOrderQuantityBySize;
import com.ag.model.QtyBySize;
import com.ag.service.ProductionOrderManager;
import com.ag.service.ProductionOrderNumberBinManager;
import com.google.gson.Gson;

@Controller
@RequestMapping("/manage/productionOrdersForm*")
public class ProductionOrderFormController extends BaseFormController {
	private static Pattern PRODUCT_CODE_PATTERN = Pattern.compile("orderProduct-[0-9]+-code$", Pattern.MULTILINE);
	private static Pattern ADJUSTMENT_CODE_PATTERN = Pattern.compile("adjustment-[0-9]+-code$", Pattern.MULTILINE);
	private ProductionOrderManager productionOrderManager;
	private ProductionOrderNumberBinManager productionOrderNumberBinManager;

	@Autowired
    public void setProductionOrderManager(
			ProductionOrderManager productionOrderManager) {
		this.productionOrderManager = productionOrderManager;
	}

	@Autowired
	public void setProductionOrderNumberBinManager(
			ProductionOrderNumberBinManager productionOrderNumberBinManager) {
		this.productionOrderNumberBinManager = productionOrderNumberBinManager;
	}
	
	@Value("#{productionOrderNumberBinProps['useLeadingZeros']}")
    private boolean isLeadingZeros;
    
    @Value("#{productionOrderNumberBinProps['numberOfLeadingZeros']}")
    private Integer numberOfLeadingZeros;

    @Value("#{productionOrderNumberBinProps['orderNumberPrefix']}")
    private String orderNoPrefix;
    
	public ProductionOrderFormController() {
        //setCancelView("redirect:/home");
		setCancelView("redirect:/manage/productionOrders");
        setSuccessView("redirect:/manage/productionOrders");
    }
	
    @Override
    @InitBinder
    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
        super.initBinder(request, binder);
    }
    
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
									@RequestParam(required=false, value="method") final String method,
									@RequestParam(required=false, value="from")  final String from,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		
		Model model = new ExtendedModelMap();

		Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
		if (inputFlashMap != null){
			if (inputFlashMap.containsKey("productionOrderForm")){
				model.addAttribute("productionOrderForm", (ProductionOrderForm)inputFlashMap.get("productionOrderForm"));
			}
			if (inputFlashMap.containsKey("productionOrderForm_errors")){
				model.addAttribute("org.springframework.validation.BindingResult.productionOrderForm", (BindingResult)inputFlashMap.get("productionOrderForm_errors"));
			}			
		}

		if (!model.containsAttribute("productionOrderForm")){
			ProductionOrderForm form =  new ProductionOrderForm();
			if (id != null){
				form = new ProductionOrderForm(productionOrderManager.getWithOrderItemsAndAdjustments(id));
			} else {
				form.setStatus('N');
				form.setOrderNo(String.format("%s-%s", orderNoPrefix, productionOrderNumberBinManager.getNextOrderNumber(isLeadingZeros, numberOfLeadingZeros)));
			}
			model.addAttribute("productionOrderForm", form);
		}
		
		return new ModelAndView("manage/productionOrderForm", model.asMap());
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(@ModelAttribute("productionOrderForm") ProductionOrderForm productionOrderForm,
								BindingResult errors, 
								RedirectAttributes redirectAttributes,
								HttpServletRequest request,
								HttpServletResponse response) throws Exception {
		final Locale locale = request.getLocale();
		//logRequestParams(request);
		// if cancel button click
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
 
        // if delete button click
        if (request.getParameter("delete") != null) {  
        	try {
        		productionOrderManager.remove(productionOrderForm.getId());
        		saveMessage(request, getText("productionOrderForm.deleted", productionOrderForm.getOrderNo(), locale));
        	
        	} catch (DataIntegrityViolationException e){	
    	    		saveError(request, String.format("Only unreferenced production order can be deleted."));	
    	    		redirectAttributes.addFlashAttribute("productionOrderForm", productionOrderForm);
    				redirectAttributes.addFlashAttribute("productionOrderForm_errors", errors);
    				
    				return "redirect:/manage/productionOrdersForm";
    				
        		
        	} catch (Exception e){
	    		saveError(request, String.format("Product inventory record could not be deleted due to %s", e.getMessage()));	
	    		redirectAttributes.addFlashAttribute("productionOrderForm", productionOrderForm);
				redirectAttributes.addFlashAttribute("productionOrderForm_errors", errors);
				
				return "redirect:/manage/productionOrdersForm";        		
        	}
            return getSuccessView();
        } 
        
		if (validator != null){
			validator.validate(productionOrderForm, errors);
			
			if (errors.hasErrors()){
				redirectAttributes.addFlashAttribute("productionOrderForm", productionOrderForm);
				redirectAttributes.addFlashAttribute("productionOrderForm_errors", errors);
				
				return "redirect:/manage/productionOrdersForm";
			}
		}
		
		boolean isNew = true;
		ProductionOrder productionOrder = new ProductionOrder();
		if (productionOrderForm.getId() != null){
			productionOrder = productionOrderManager.getWithOrderItemsAndAdjustments(productionOrderForm.getId());
			isNew = false;
		} else {
			productionOrderForm.setStatus('D');//set to draft
		}
		// manipulate form for saving new or update existing
		productionOrder.setOrderNo(productionOrderForm.getOrderNo());
		productionOrder.setOrderDate(productionOrderForm.getOrderDate());
		productionOrder.setReference(productionOrderForm.getReference());
		
		// check status update first
		if (productionOrderForm.getStatus() != null && (productionOrderForm.getStatus() == 'A' || productionOrderForm.getStatus() == 'C')){
			// 1. check if there is at least one order items
			if (!isAtLeastOneProductionOrderItemAdded(request)){
				// reset status
				productionOrderForm.setStatus('N');
				if (productionOrder != null) productionOrderForm.setStatus(productionOrder.getStatus());
				
				// add error messge
	    		saveError(request, "At least one (1) order item must be added to accept/confirm an order!");	
	    		redirectAttributes.addFlashAttribute("productionOrderForm", productionOrderForm);
				redirectAttributes.addFlashAttribute("productionOrderForm_errors", errors);
				
				return "redirect:/manage/productionOrdersForm"; 				
			}
		}		
		// then the set the status update
		productionOrder.setStatus(productionOrderForm.getStatus());
		
		try {
			productionOrder = productionOrderManager.save(productionOrder);
			
			// add order item if any
			productionOrder.setProductionOrderItems(getProductionOrderItems(request, productionOrder));

			// add order item adjustment if any
			productionOrder.setAdjustments(getAdjustments(request, productionOrder));
			
			// save with sales order items
			productionOrder = productionOrderManager.save(productionOrder);
			
			saveMessage(request, getText((isNew ? "productionOrderForm.created":"productionOrderForm.updated"), productionOrderForm.getOrderNo(), locale));

		} catch (Exception e){
			saveError(request, e.getCause().getLocalizedMessage());
			e.printStackTrace();

		}
		
		//return getSuccessView();
		return String.format("redirect:/manage/productionOrdersForm?id=%s&from=list&method=Edit", (productionOrderForm.getId() != null ? productionOrderForm.getId():""));
	}
    
	private Set<ProductionOrderItem> getProductionOrderItems(
			HttpServletRequest request, ProductionOrder productionOrder) {
		log.debug("Entered getProductionOrderItems...");
		Set<ProductionOrderItem> productionOrderItems = new HashSet<ProductionOrderItem>();
		Enumeration en = request.getParameterNames();
		
		while (en.hasMoreElements()){
			String name = (String)en.nextElement();
			if (PRODUCT_CODE_PATTERN.matcher(name).find()){
				String prefix = name.replace("-code", "");
				String productCode = request.getParameter(name);
				String productQty = request.getParameter(String.format("%s-qty", prefix));
				String productQtyBySize = request.getParameter(String.format("%s-qtyBySize", prefix));
				log.debug("productCode=" + productCode);
				log.debug("productQty=" + productQty);
				log.debug("productQtyBySize=" + productQtyBySize);
				
				// parse json in qtyBySize
				List<QtyBySize> qtyBySizes = parseProductQtyBySize(productQtyBySize);
				
				Product p = productionOrderManager.getProductByCode(productCode);
				if (p != null){
					ProductionOrderItem productionOrderItem = new ProductionOrderItem(p, productionOrder, Integer.valueOf(productQty));
					
					if (qtyBySizes != null && !qtyBySizes.isEmpty()){
						Set<ProductionOrderQuantityBySize> qbzSet = new HashSet<ProductionOrderQuantityBySize>();
						for (QtyBySize qtyBySize:qtyBySizes){
							ProductionOrderQuantityBySize qbz = new ProductionOrderQuantityBySize();
							qbz.setOrderQuantity(qtyBySize.getOrderQuantity());
							qbz.setProductionOrderItem(productionOrderItem);
							qbz.setSize(qtyBySize.getSize());						
							qbzSet.add(qbz);
						}
						productionOrderItem.setProductionOrderQuantityBySizes(qbzSet);
					}
					
					productionOrderItems.add(productionOrderItem);
				}
				log.debug(productionOrderItems);
			} 
		}
		return productionOrderItems;
	}
		
	private Set<ProductionOrderItemAdjustment> getAdjustments(
			HttpServletRequest request, ProductionOrder productionOrder) throws NumberFormatException, ParseException {
		log.debug("Entered getAdjustments...");
		Set<ProductionOrderItemAdjustment> adjustments = new HashSet<ProductionOrderItemAdjustment>();
		Enumeration en = request.getParameterNames();
		
		while (en.hasMoreElements()){
			String name = (String)en.nextElement();
			if (ADJUSTMENT_CODE_PATTERN.matcher(name).find()){
				String prefix = name.replace("-code", "");
				String adjustmentCode = request.getParameter(name);
				String adjustmentDate = request.getParameter(String.format("%s-date", prefix));
				String adjustmentQty = request.getParameter(String.format("%s-qty", prefix));
				String adjustmentReference = request.getParameter(String.format("%s-reference", prefix));
				log.debug("adjustmentCode=" + adjustmentCode);
				log.debug("adjustmentQty=" + adjustmentQty);
				log.debug("adjustmentReference=" + adjustmentReference);

				ProductionOrderItemAdjustment adjustment = new ProductionOrderItemAdjustment(productionOrder, adjustmentCode, DateUtils.parseDate(adjustmentDate, "MM/dd/yyyy"), adjustmentReference, Integer.valueOf(adjustmentQty));
				adjustments.add(adjustment);
			} 
		}
		log.debug(adjustments);
		
		return adjustments;
	}
	
	private boolean isAtLeastOneProductionOrderItemAdded(HttpServletRequest request ){
		Enumeration en = request.getParameterNames();
		
		while (en.hasMoreElements()){
			String name = (String)en.nextElement();
			if (PRODUCT_CODE_PATTERN.matcher(name).find()){
				return true;
			}
		}
		
		return false;
	}	
	
	private List<QtyBySize> parseProductQtyBySize(String jsonStr) {
		List<QtyBySize> result = null;
		try {
			Gson gson = new Gson();
			QtyBySize[] temp = gson.fromJson(jsonStr, QtyBySize[].class);
			if (temp != null){
				result = Arrays.asList(temp);
			}
		
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
}
