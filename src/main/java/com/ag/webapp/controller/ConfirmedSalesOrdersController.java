package com.ag.webapp.controller;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ag.model.SalesOrder;
import com.ag.service.SalesOrderManager;

@Controller
@RequestMapping("/manage/confirmedSalesOrders*")
public class ConfirmedSalesOrdersController {
	protected final static Log log = LogFactory.getLog(ConfirmedSalesOrdersController.class);
	private SalesOrderManager salesOrderManager;
	
	@Autowired
	public void setSalesOrderManager(SalesOrderManager salesOrderManager) {
		this.salesOrderManager = salesOrderManager;
	}


	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView handleRequest(@RequestParam(value="customerId", required=false) Long customerId){
		log.debug("Entering ConfirmedSalesOrdersController:handleRequest()...");
		Model model = new ExtendedModelMap();
		
		if (customerId != null) {
			model.addAttribute("confirmedSalesOrders", salesOrderManager.getConfirmedSalesOrderByCustomerId(customerId));
		}
		if (!model.containsAttribute("confirmedSalesOrders"))
			model.addAttribute("confirmedSalesOrders", new ArrayList<SalesOrder>());
		
		log.debug(model.asMap().get("confirmedSalesOrders"));
		return new ModelAndView("manage/_confirmedSalesOrders", model.asMap());
	}
}
