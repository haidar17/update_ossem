package com.ag.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ag.dao.SearchException;
import com.ag.service.ProductionOrderManager;

@Controller
@RequestMapping("/manage/productionOrders*")
public class ProductionOrderController {

	protected final static Log log = LogFactory.getLog(ProductionOrderController.class);
	private ProductionOrderManager productionOrderManager;
	
	@Autowired
	public void setProductionOrderManager(ProductionOrderManager productionOrderManager) {
		this.productionOrderManager = productionOrderManager;
	}	
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showList(@RequestParam(required = false, value = "q") String query,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		log.debug("Entered showList...");
		Model model = new ExtendedModelMap();
		if (query != null && !StringUtils.isEmpty(query)){
			try {
				log.debug("perform searching of [" + query + "] ...");
	            model.addAttribute("productionOrders", productionOrderManager.search(query));
	            log.debug("search completed!");
	            log.debug(model.asMap().get("productionOrders"));
	            
	        } catch (SearchException se) {
	            model.addAttribute("searchError", se.getMessage());
	            model.addAttribute("productionOrders", productionOrderManager.getAll());
	            
	        }	
		} else {
			// not performing search.
			model.addAttribute("productionOrders", productionOrderManager.getAll());
		}
	
		return new ModelAndView("manage/productionOrders", model.asMap());
	}	
}
