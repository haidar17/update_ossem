package com.ag.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ag.service.ProductManager;

@Controller
@RequestMapping("/manage/products*")
public class ProductController {
	private ProductManager productManager;
	
	@Autowired
	public void setProductManager(ProductManager productManager) {
		this.productManager = productManager;
	}

	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showList(HttpServletRequest request,
									HttpServletResponse response){
		Model model = new ExtendedModelMap();
		model.addAttribute("products", productManager.getAllDistinct());
		
		return new ModelAndView("manage/products", model.asMap());
	}
}
