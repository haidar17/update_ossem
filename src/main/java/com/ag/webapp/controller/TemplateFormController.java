package com.ag.webapp.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.Template;
import com.ag.model.TemplateForm;
import com.ag.service.TemplateManager;

/**
 * This is form controller template for other domain
 * You may copy and build your own however you see it fit your intended domain. 
 * Important: Please do not edit or update any part of this file.
 * @author hamid
 *
 */
@Controller
@RequestMapping("/manage/templatesForm*")
public class TemplateFormController extends BaseFormController {
		private TemplateManager templateManager;
		
		@Autowired
	    public void setTemplateManager(TemplateManager templateManager) {
			this.templateManager = templateManager;
		}

		public TemplateFormController() {
	        //setCancelView("redirect:/home");
			setCancelView("redirect:/manage/templates");
	        setSuccessView("redirect:/manage/templates");
	    }

	    @Override
	    @InitBinder
	    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
	        super.initBinder(request, binder);
	    }
	    
		@RequestMapping(method=RequestMethod.GET)
		public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
										@RequestParam(required=false, value="method") final String method,
										@RequestParam(required=false, value="from")  final String from,
										HttpServletRequest request,
										HttpServletResponse response) throws Exception {
			
			Model model = new ExtendedModelMap();

			Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
			if (inputFlashMap != null){
				if (inputFlashMap.containsKey("templateForm")){
					model.addAttribute("templateForm", (TemplateForm)inputFlashMap.get("templateForm"));
				}
				if (inputFlashMap.containsKey("templateForm_errors")){
					model.addAttribute("org.springframework.validation.BindingResult.templateForm", (BindingResult)inputFlashMap.get("templateForm_errors"));
				}			
			}

			if (!model.containsAttribute("templateForm")){
				TemplateForm form =  new TemplateForm();
				if (id != null){
					form = new TemplateForm(templateManager.get(id));
				}
				model.addAttribute("templateForm", form);
			}
			
			return new ModelAndView("manage/templateForm", model.asMap());
			
		}
		
		@RequestMapping(method=RequestMethod.POST)
		public String onSubmit(@ModelAttribute("templateForm") TemplateForm templateForm,
									BindingResult errors, 
									RedirectAttributes redirectAttributes,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
			final Locale locale = request.getLocale();
			
			// if cancel button click
	        if (request.getParameter("cancel") != null) {
	            if (!StringUtils.equals(request.getParameter("from"), "list")) {
	                return getCancelView();
	            } else {
	                return getSuccessView();
	            }
	        }
	 
	        // if delete button click
	        if (request.getParameter("delete") != null) {  
	        	try {
	        		templateManager.remove(templateForm.getId());
	        		saveMessage(request, getText("templateForm.deleted", templateForm.getName(), locale));
	        		
	        	} catch (Exception e){
		    		saveError(request, String.format("template record could not be deleted due to %s", e.getMessage()));	
		    		redirectAttributes.addFlashAttribute("templateForm", templateForm);
					redirectAttributes.addFlashAttribute("templateForm_errors", errors);
					
					return "redirect:/manage/templatesForm";        		
	        	}
	            return getSuccessView();
	        } 
	        
			if (validator != null){
				validator.validate(templateForm, errors);
				
				if (errors.hasErrors()){
					redirectAttributes.addFlashAttribute("templateForm", templateForm);
					redirectAttributes.addFlashAttribute("templateForm_errors", errors);
					
					return "redirect:/manage/templatesForm";
				}
			}
			
			boolean isNew = true;
			Template template = new Template();
			if (templateForm.getId() != null){
				template = templateManager.get(templateForm.getId());
				isNew = false;
			}
			// manipulate form for saving new or update existing
			template.setName(templateForm.getName());
			template.setDescription(templateForm.getDescription());
			
			try {
				templateManager.save(template);
				saveMessage(request, getText((isNew ? "templateForm.created":"templateForm.updated"), templateForm.getName(), locale));

			} catch (Exception e){
				saveError(request, e.getCause().getLocalizedMessage());
				e.printStackTrace();

			}
			
			return getSuccessView();
		}
}
