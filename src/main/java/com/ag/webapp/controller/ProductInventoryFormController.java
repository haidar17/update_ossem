package com.ag.webapp.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.ProductInventory;
import com.ag.model.ProductInventoryForm;
import com.ag.service.ProductInventoryManager;

@Controller
@RequestMapping("/manage/productInventoryForm*")
public class ProductInventoryFormController extends BaseFormController {
	private ProductInventoryManager productInventoryManager;
	
	@Autowired
    public void setProductInventoryManager(ProductInventoryManager productInventoryManager) {
		this.productInventoryManager = productInventoryManager;
	}

	public ProductInventoryFormController() {
        //setCancelView("redirect:/home");
		setCancelView("redirect:/manage/productInventory");
        setSuccessView("redirect:/manage/productInventory");
    }

    @Override
    @InitBinder
    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
        super.initBinder(request, binder);
    }
    
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
									@RequestParam(required=false, value="method") final String method,
									@RequestParam(required=false, value="from")  final String from,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		
		Model model = new ExtendedModelMap();

		Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
		if (inputFlashMap != null){
			if (inputFlashMap.containsKey("productInventoryForm")){
				model.addAttribute("productInventoryForm", (ProductInventoryForm)inputFlashMap.get("productInventoryForm"));
			}
			if (inputFlashMap.containsKey("productInventoryForm_errors")){
				model.addAttribute("org.springframework.validation.BindingResult.productInventoryForm", (BindingResult)inputFlashMap.get("productInventoryForm_errors"));
			}			
		}

		if (!model.containsAttribute("productInventoryForm")){
			ProductInventoryForm form =  new ProductInventoryForm();
			if (id != null){
				form = new ProductInventoryForm(productInventoryManager.get(id));
			}
			model.addAttribute("productInventoryForm", form);
		}
		
		return new ModelAndView("manage/productInventoryForm", model.asMap());
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(@ModelAttribute("productInventoryForm") ProductInventoryForm productInventoryForm,
								BindingResult errors, 
								RedirectAttributes redirectAttributes,
								HttpServletRequest request,
								HttpServletResponse response) throws Exception {
		final Locale locale = request.getLocale();
		
		// if cancel button click
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
 
        // if delete button click
        if (request.getParameter("delete") != null) {  
        	try {
        		productInventoryManager.remove(productInventoryForm.getId());
        		saveMessage(request, getText("productInventoryForm.deleted", productInventoryForm.getEntryNote(), locale));

	    	} catch (Exception e){
	    		saveError(request, String.format("Product inventory record could not be deleted due to %s", e.getMessage()));	
	    		redirectAttributes.addFlashAttribute("productInventoryForm", productInventoryForm);
				redirectAttributes.addFlashAttribute("productInventoryForm_errors", errors);
				
				return "redirect:/manage/productInventoryForm";       		
	    	}
            return getSuccessView();
        } 
        
		if (validator != null){
			validator.validate(productInventoryForm, errors);
			
			if (errors.hasErrors()){
				redirectAttributes.addFlashAttribute("productInventoryForm", productInventoryForm);
				redirectAttributes.addFlashAttribute("productInventoryForm_errors", errors);
				
				return "redirect:/manage/productInventoryForm";
			}
		}
		
		boolean isNew = true;
		ProductInventory productInventory = new ProductInventory();
		if (productInventoryForm.getId() != null){
			productInventory = productInventoryManager.get(productInventoryForm.getId());
			isNew = false;
		}
		// manipulate form for saving new or update existing
		
		try {
			productInventoryManager.save(productInventory);
			saveMessage(request, getText((isNew ? "productInventoryForm.created":"productInventoryForm.updated"), productInventoryForm.getEntryNote(), locale));

		} catch (Exception e){
			saveError(request, e.getCause().getLocalizedMessage());
			e.printStackTrace();

		}
		
		return getSuccessView();
	}
}
