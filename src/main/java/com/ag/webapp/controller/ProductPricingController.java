package com.ag.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ag.dao.SearchException;
import com.ag.service.ProductPricingManager;
/**
 * @author hamid
 *
 */
@Controller
@RequestMapping("/manage/productPricings*")
public class ProductPricingController {
	protected final static Log log = LogFactory.getLog(ProductPricingController.class);
	private ProductPricingManager productPricingManager;
	
	@Autowired
	public void setProductPricingManager(ProductPricingManager productPricingManager) {
		this.productPricingManager = productPricingManager;
	}	
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showList(@RequestParam(required = false, value = "q") String query,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		log.debug("Entered showList...");
		Model model = new ExtendedModelMap();
		if (query != null && !StringUtils.isEmpty(query)){
			try {
				log.debug("perform searching of [" + query + "] ...");
	            model.addAttribute("productPricings", productPricingManager.search(query));
	            log.debug("search completed!");
	            log.debug(model.asMap().get("productPricings"));
	            
	        } catch (SearchException se) {
	            model.addAttribute("searchError", se.getMessage());
	            model.addAttribute("customerProductPricings", productPricingManager.getAllDistinctByCustomer());
	            
	        }	
		} else {
			// not performing search.
			model.addAttribute("customerProductPricings", productPricingManager.getAllDistinctByCustomer());
		}
	
		return new ModelAndView("manage/productPricings", model.asMap());
	}

}
