package com.ag.webapp.controller;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.ag.model.Product;
import com.ag.model.SalesOrderQuantityBySize;
import com.ag.model.QtyBySize;
import com.ag.model.SalesOrder;
import com.ag.model.SalesOrderForm;
import com.ag.model.SalesOrderItem;
import com.ag.service.SalesOrderManager;
import com.ag.service.SalesOrderNumberBinManager;
import com.ag.webapp.util.StringUtil;
import com.google.gson.Gson;

@Controller
@RequestMapping("/manage/salesOrdersForm*")
public class SalesOrderFormController extends BaseFormController {
	private static Pattern PRODUCT_ID_PATTERN = Pattern.compile("orderProduct-[0-9]+-code$", Pattern.MULTILINE);
	private SalesOrderManager salesOrderManager;
	private SalesOrderNumberBinManager salesOrderNumberBinManager;
	
	
	@Autowired
    public void setSalesOrderManager(SalesOrderManager salesOrderManager) {
		this.salesOrderManager = salesOrderManager;
	}

	@Autowired
	public void setSalesOrderNumberBinManager(SalesOrderNumberBinManager salesOrderNumberBinManager) {
		this.salesOrderNumberBinManager = salesOrderNumberBinManager;
	}

    @Value("#{salesOrderNumberBinProps['useLeadingZeros']}")
    private boolean isLeadingZeros;
    
    @Value("#{salesOrderNumberBinProps['numberOfLeadingZeros']}")
    private Integer numberOfLeadingZeros;
    
    @Value("#{salesOrderNumberBinProps['orderNumberPrefix']}")
    private String orderNoPrefix;  
    
	public SalesOrderFormController() {
        //setCancelView("redirect:/home");
        setCancelView("redirect:/manage/salesOrders");
        setSuccessView("redirect:/manage/salesOrders");
    }

    @Override
    @InitBinder
    protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
        super.initBinder(request, binder);
    }
    
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showForm(@RequestParam(required=false, value="id") final Long id,
									@RequestParam(required=false, value="method") final String method,
									@RequestParam(required=false, value="from")  final String from,
									HttpServletRequest request,
									HttpServletResponse response) throws Exception {
		
		Model model = new ExtendedModelMap();

		Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
		if (inputFlashMap != null){
			if (inputFlashMap.containsKey("salesOrderForm")){
				model.addAttribute("salesOrderForm", (SalesOrderForm)inputFlashMap.get("salesOrderForm"));
			}
			if (inputFlashMap.containsKey("salesOrderForm_errors")){
				model.addAttribute("org.springframework.validation.BindingResult.salesOrderForm", (BindingResult)inputFlashMap.get("salesOrderForm_errors"));
			}			
		}

		if (!model.containsAttribute("salesOrderForm")){
			SalesOrderForm form =  new SalesOrderForm();
			
			if (id != null){
				form =  new SalesOrderForm(salesOrderManager.getWithSalesOrderItems(id));
			} else {
				form.setStatus('N');
				form.setDeliveryStatus('N');
				form.setOrderNo(String.format("%s-%s", orderNoPrefix, salesOrderNumberBinManager.getNextOrderNumber(isLeadingZeros, numberOfLeadingZeros)));
			}
			
			
			model.addAttribute("salesOrderForm", form);
		}
		
		return new ModelAndView("manage/salesOrderForm", model.asMap());
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(@ModelAttribute("salesOrderForm") SalesOrderForm salesOrderForm,
								BindingResult errors, 
								RedirectAttributes redirectAttributes,
								HttpServletRequest request,
								HttpServletResponse response) throws Exception {
		final Locale locale = request.getLocale();
		logRequestParams(request);
		// if cancel button click
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
 
        // if delete button click
        if (request.getParameter("delete") != null) {  
        	try {
        		salesOrderManager.remove(salesOrderForm.getId());
        		saveMessage(request, getText("salesOrderForm.deleted", salesOrderForm.getCustomerFullName(), locale));
        		
	    	} catch (DataIntegrityViolationException e){	
	    		saveError(request, String.format("Only unreferenced sales order can be deleted."));	
	    		redirectAttributes.addFlashAttribute("salesOrderForm", salesOrderForm);
				redirectAttributes.addFlashAttribute("salesOrderForm_errors", errors);
				
				return "redirect:/manage/salesOrdersForm";
				
	    	} catch (Exception e){
	    		saveError(request, String.format("Sales order could not be deleted due to %s", e.getMessage()));	
	    		redirectAttributes.addFlashAttribute("salesOrderForm", salesOrderForm);
				redirectAttributes.addFlashAttribute("salesOrderForm_errors", errors);
				
				return "redirect:/manage/salesOrdersForm";       		
	    	}
            return getSuccessView();
        } 
        
		if (validator != null){
			validator.validate(salesOrderForm, errors);
			
			if (errors.hasErrors()){
				redirectAttributes.addFlashAttribute("salesOrderForm", salesOrderForm);
				redirectAttributes.addFlashAttribute("salesOrderForm_errors", errors);
				
				return "redirect:/manage/salesOrdersForm";
			}
		}
		
		
		boolean isNew = true;
		SalesOrder salesOrder = new SalesOrder();
		if (salesOrderForm.getId() != null){
			isNew = false;
			salesOrder = salesOrderManager.getWithSalesOrderItems(salesOrderForm.getId());
		} else {
			salesOrderForm.setStatus('D');//sets as draft
			salesOrderForm.setDeliveryStatus('D');
		}
		// manipulate form for saving new or update existing
		
		salesOrder.setCustomer(salesOrderManager.getCustomerById(salesOrderForm.getCustomerId()));
		salesOrder.setOrderDate(salesOrderForm.getOrderDate());
		salesOrder.setOrderNo(salesOrderForm.getOrderNo());
	
		if (salesOrderForm.getStatus() != null && (salesOrderForm.getStatus() == 'A' || salesOrderForm.getStatus() == 'C')){
			// 1. check if there is at least one order items
			if (!isAtLeastOneSalesOrderItemAdded(request)){
				// reset status
				salesOrderForm.setStatus('N');
				if (salesOrder != null) salesOrderForm.setStatus(salesOrder.getStatus());
				
				// add error messge
	    		saveError(request, "At least one (1) order item must be added to accept/confirm an order!");	
	    		redirectAttributes.addFlashAttribute("salesOrderForm", salesOrderForm);
				redirectAttributes.addFlashAttribute("salesOrderForm_errors", errors);
				
				return "redirect:/manage/salesOrdersForm"; 				
			}
		}
		
		// must be set after checking 
		salesOrder.setStatus(salesOrderForm.getStatus());
		salesOrder.setDeliveryStatus(salesOrderForm.getDeliveryStatus());
		
		try {
			salesOrder = salesOrderManager.save(salesOrder);
			
			// add order item if any
			salesOrder.setSalesOrderItems(getSalesOrderItems(request, salesOrder));
			
			// save with sales order items
			salesOrderManager.save(salesOrder);
			
			saveMessage(request, getText((isNew ? "salesOrderForm.created":"salesOrderForm.updated"), salesOrderForm.getCustomerFullName(), locale));
			
			// update sales order number bin
			//salesOrderNumberBinManager.updateOrderNumberBin(salesOrder);
			
		} catch (Exception e){
			saveError(request, e.getCause().getLocalizedMessage());
			e.printStackTrace();

		}
		
		return String.format("redirect:/manage/salesOrdersForm?from=list&method=Edit&id=%s", (salesOrder.getId() != null ? salesOrder.getId():"")); 
		//return getSuccessView();
	}
	
	private Set<SalesOrderItem> getSalesOrderItems(
			HttpServletRequest request, SalesOrder salesOrder) {
		log.debug("Entered getSalesOrderItems...");
		Set<SalesOrderItem> salesOrderItems = new HashSet<SalesOrderItem>();
		Enumeration en = request.getParameterNames();
		
		while (en.hasMoreElements()){
			String name = (String)en.nextElement();
			if (PRODUCT_ID_PATTERN.matcher(name).find()){
				String prefix = name.replace("-code", "");
				String productCode = request.getParameter(name);
				String orderQty = request.getParameter(String.format("%s-orderQty", prefix));
				String deliveryQty = request.getParameter(String.format("%s-deliveryQty", prefix));
				String unitPrice = request.getParameter(String.format("%s-unit-price", prefix));
				String productQtyBySize = request.getParameter(String.format("%s-qtyBySize", prefix));
				log.debug("productCode=" + productCode);
				log.debug("orderQty=" + orderQty);
				log.debug("deliveryQty=" + deliveryQty);
				log.debug("unitPrice=" + unitPrice);
				
				// parse json in qtyBySize
				List<QtyBySize> qtyBySizes = parseProductQtyBySize(productQtyBySize);

				Product product = salesOrderManager.getProductByCode(productCode);
				if (product != null){
					log.debug("Masuk salesOrderManager");
					SalesOrderItem salesOrderItem = new SalesOrderItem(product, 
																		salesOrder, 
																		Integer.valueOf(orderQty), 
																		StringUtil.parseIntWithDefault(deliveryQty, 0), 
																		Double.valueOf(unitPrice));
					if (qtyBySizes != null && !qtyBySizes.isEmpty()){
						log.debug("Masuk qtybySizes");
						Set<SalesOrderQuantityBySize> qbzSet = new HashSet<SalesOrderQuantityBySize>();
						for (QtyBySize qtyBySize:qtyBySizes){
							log.debug("qtybySizes=" + qtyBySize.getOrderQuantity());
							log.debug("sizes=" + qtyBySize.getSize());
							SalesOrderQuantityBySize qbz = new SalesOrderQuantityBySize();
							qbz.setOrderQuantity(qtyBySize.getOrderQuantity());
							qbz.setSalesOrderItem(salesOrderItem);
							qbz.setSize(qtyBySize.getSize());						
							qbzSet.add(qbz);
						}
						salesOrderItem.setSalesOrderQuantityBySizes(qbzSet);
					}
					
					salesOrderItems.add(salesOrderItem);
				}				
				log.debug(salesOrderItems);
			} 
		}
		return salesOrderItems;
	}	
	
	private boolean isAtLeastOneSalesOrderItemAdded(HttpServletRequest request ){
		Enumeration en = request.getParameterNames();
		
		while (en.hasMoreElements()){
			String name = (String)en.nextElement();
			if (PRODUCT_ID_PATTERN.matcher(name).find()){
				return true;
			}
		}
		
		return false;
	}
	
	private List<QtyBySize> parseProductQtyBySize(String jsonStr) {
		List<QtyBySize> result = null;
		try {
			Gson gson = new Gson();
			QtyBySize[] temp = gson.fromJson(jsonStr, QtyBySize[].class);
			if (temp != null){
				result = Arrays.asList(temp);
			}
		
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
}
