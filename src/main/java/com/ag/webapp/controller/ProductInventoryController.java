package com.ag.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ag.service.ProductInventoryManager;

@Controller
@RequestMapping("/manage/productInventory*")
public class ProductInventoryController {
	private ProductInventoryManager productInventoryManager;
	
	@Autowired
	public void setProductInventoryManager(
			ProductInventoryManager productInventoryManager) {
		this.productInventoryManager = productInventoryManager;
	}	
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView showList(HttpServletRequest request,
									HttpServletResponse response){
		Model model = new ExtendedModelMap();
		model.addAttribute("productInventory", productInventoryManager.getAll());
		
		return new ModelAndView("manage/productInventory", model.asMap());
	}


}
