package com.ag.webapp.util;

public class StringUtil {
	private StringUtil(){
		throw new AssertionError();
	}
	
	
	public static Integer parseIntWithDefault(String str, Integer defaultInt){	
		try {
			return Integer.parseInt(str);
			
		} catch (NumberFormatException e){
			//ignore			
		}
		
		return defaultInt;
	}
}
