package com.ag.model;

public interface Person {
	public void setFirstName(String firstName);
	public String getFirstName();
	public void setLastName(String lastName);
	public String getLastName();
	public void setGender(Character gender);
	public Character getGender();	
}
