package com.ag.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeanUtils;

public class DeliveryOrderForm {
	private Long id;
	private String reference;
	private String note;
	private String orderNo;
	private Date orderDate;
	private String customerFullName;
	private Long customerId;
	private Long billingAddressId;
	private String billingAddress;
	private String billingStreet;
	private String billingPostalCode;
	private String billingCity;
	private Long billingStateId;
	private Long billingCountryId;
	private Long deliveryAddressId;
	private String deliveryAddress;
	private String deliveryStreet;
	private String deliveryPostalCode;
	private String deliveryCity;
	private Long deliveryStateId;
	private Long deliveryCountryId;	
	
	private Set<SalesOrderForm> confirmedSalesOrders;
	private Set<DeliveryOrderItem> deliveryOrderItems;
	private Character status;
	
	public DeliveryOrderForm() {
		super();
	}

	public DeliveryOrderForm(DeliveryOrder deliveryOrder) {
		if (deliveryOrder != null){
			BeanUtils.copyProperties(deliveryOrder, this, new String[]{"customer", "confirmedSalesOrders"});
			if (deliveryOrder.getCustomer() != null){
				this.customerId = deliveryOrder.getCustomer().getId();
				this.customerFullName = deliveryOrder.getCustomer().getFullName();
			}
			
			if (deliveryOrder.getConfirmedSalesOrders() != null && !deliveryOrder.getConfirmedSalesOrders().isEmpty()){
				confirmedSalesOrders = new HashSet<SalesOrderForm>();
				for (SalesOrder salesOrder : deliveryOrder.getConfirmedSalesOrders()){
					SalesOrderForm salesOrderForm = new SalesOrderForm(salesOrder);
					salesOrderForm.setSelectedSalesOrder(true);
					confirmedSalesOrders.add(salesOrderForm);
				}
			}
			
			if (deliveryOrder.getBillingAddress() != null){
				this.billingAddressId = deliveryOrder.getBillingAddress().getId();
				this.billingAddress = deliveryOrder.getBillingAddress().getAddress();
				this.billingStreet = deliveryOrder.getBillingAddress().getStreet();
				this.billingCity = deliveryOrder.getBillingAddress().getCity();
				this.billingPostalCode = deliveryOrder.getDeliveryAddress().getPostalCode();
				if (deliveryOrder.getBillingAddress().getState() != null){
					this.billingStateId = deliveryOrder.getBillingAddress().getState().getId();
					if (deliveryOrder.getBillingAddress().getState().getCountry() != null){
						this.billingCountryId = deliveryOrder.getBillingAddress().getState().getCountry().getId();
					}
				}
			}
			
			if (deliveryOrder.getDeliveryAddress() != null){
				this.deliveryAddressId = deliveryOrder.getDeliveryAddress().getId();
				this.deliveryAddress = deliveryOrder.getDeliveryAddress().getAddress();
				this.deliveryStreet = deliveryOrder.getDeliveryAddress().getStreet();
				this.deliveryCity = deliveryOrder.getDeliveryAddress().getCity();
				this.deliveryPostalCode = deliveryOrder.getDeliveryAddress().getPostalCode();
				if (deliveryOrder.getDeliveryAddress().getState() != null){
					this.deliveryStateId = deliveryOrder.getDeliveryAddress().getState().getId();
					if (deliveryOrder.getDeliveryAddress().getState().getCountry() != null){
						this.deliveryCountryId = deliveryOrder.getDeliveryAddress().getState().getCountry().getId();
					}
				}
			}			
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getCustomerFullName() {
		return customerFullName;
	}

	public void setCustomerFullName(String customerFullName) {
		this.customerFullName = customerFullName;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Set<DeliveryOrderItem> getDeliveryOrderItems() {
		return deliveryOrderItems;
	}

	public void setDeliveryOrderItems(Set<DeliveryOrderItem> deliveryOrderItems) {
		this.deliveryOrderItems = deliveryOrderItems;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}	
	
	public boolean isNewOrder(){
		return (status != null && status == 'N');
	}
	
	public boolean isDraftOrder(){
		return (status != null && status == 'D');
	}
	
	public boolean isConfirmedOrder(){
		return (status != null && status == 'C');
	}
	

	public boolean isCompletedOrder(){
		return (status != null && status == 'E');
	}	
	public boolean isAcceptedOrder(){
		return (status != null && status == 'A');
	}
	
	public boolean isRejectedOrder(){
		return (status != null && status == 'R');
	}	
	
	
	public String getStatusName(){
		String statusName = "Unknown";
		switch (status){
			case 'N': statusName = "New"; break; 
			case 'D': statusName = "Draft"; break; 
			case 'A': statusName = "Accepted"; break;
			case 'R': statusName = "Rejected"; break;
			case 'C': statusName = "Confirmed"; break;
			case 'E': statusName = "Completed"; break;			
		}
		
		return statusName;
	}	
	
	public boolean getHasDeliveryOrderItems(){
		return (deliveryOrderItems != null && !deliveryOrderItems.isEmpty());
	}

	public Set<SalesOrderForm> getConfirmedSalesOrders() {
		return confirmedSalesOrders;
	}

	public void setConfirmedSalesOrders(Set<SalesOrderForm> confirmedSalesOrders) {
		this.confirmedSalesOrders = confirmedSalesOrders;
	}
	
	public boolean getHasConfirmedSalesOrders(){
		return (confirmedSalesOrders != null && !confirmedSalesOrders.isEmpty());
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBillingStreet() {
		return billingStreet;
	}

	public void setBillingStreet(String billingStreet) {
		this.billingStreet = billingStreet;
	}

	public String getBillingPostalCode() {
		return billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public Long getBillingStateId() {
		return billingStateId;
	}

	public void setBillingStateId(Long billingStateId) {
		this.billingStateId = billingStateId;
	}

	public Long getBillingCountryId() {
		return billingCountryId;
	}

	public void setBillingCountryId(Long billingCountryId) {
		this.billingCountryId = billingCountryId;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getDeliveryStreet() {
		return deliveryStreet;
	}

	public void setDeliveryStreet(String deliveryStreet) {
		this.deliveryStreet = deliveryStreet;
	}

	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}

	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public Long getDeliveryStateId() {
		return deliveryStateId;
	}

	public void setDeliveryStateId(Long deliveryStateId) {
		this.deliveryStateId = deliveryStateId;
	}

	public Long getDeliveryCountryId() {
		return deliveryCountryId;
	}

	public void setDeliveryCountryId(Long deliveryCountryId) {
		this.deliveryCountryId = deliveryCountryId;
	}

	public Long getBillingAddressId() {
		return billingAddressId;
	}

	public void setBillingAddressId(Long billingAddressId) {
		this.billingAddressId = billingAddressId;
	}

	public Long getDeliveryAddressId() {
		return deliveryAddressId;
	}

	public void setDeliveryAddressId(Long deliveryAddressId) {
		this.deliveryAddressId = deliveryAddressId;
	}	
	
	public boolean getContainsSalesOrderForm(Long id){
		if (id == null) return false;
		if (confirmedSalesOrders != null && !confirmedSalesOrders.isEmpty()){
			for (SalesOrderForm sof:confirmedSalesOrders){
				if (sof.getId().intValue() == id.intValue()){
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	public boolean isChangeCustomerAllowed(){
		return (isNewOrder() || isDraftOrder());
	}
	
	public boolean isChangeConfirmedSalesOrderAllowed(){
		return (isNewOrder() || isDraftOrder() || isAcceptedOrder());
	}	
	
	public boolean isChangeBillingAddressAllowed(){
		return (isNewOrder() || isDraftOrder() || isAcceptedOrder());
	}
	
	public boolean isChangeDeliveryAddressAllowed(){
		return (isNewOrder() || isDraftOrder() || isAcceptedOrder());
	}	
	
	public boolean getHasCustomer(){
		return (this.customerId != null);
	}
}
