package com.ag.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Check;

@Entity
@Table(name="delivery_orders")
public class DeliveryOrder {
	private Long id;
	private String reference;
	private String note;
	private String orderNo;
	private Date orderDate;
	private Customer customer;
	private BillingAddress billingAddress;
	private DeliveryAddress deliveryAddress;
//	private Set<DeliveryOrderItem> deliveryOrderItems;
	private Set<SalesOrder> confirmedSalesOrders;
	private Character status;

	public DeliveryOrder() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="delivery_order_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="delivery_order_no", nullable=false)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name="delivery_order_date", nullable=false)
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

//	@OneToMany(fetch=FetchType.LAZY, mappedBy="deliveryOrder", cascade=CascadeType.ALL, orphanRemoval=true)
//	public Set<DeliveryOrderItem> getDeliveryOrderItems() {
//		return deliveryOrderItems;
//	}
//
//	public void setDeliveryOrderItems(Set<DeliveryOrderItem> deliveryOrderItems) {
//		this.deliveryOrderItems = deliveryOrderItems;
//	}

	@Column(name="delivery_order_reference", nullable=false)
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Column(name="delivery_order_note", nullable=true)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	@JoinColumn(name="customer_id", nullable=true)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="billing_address_id", unique=true, nullable=true, updatable=true)	
	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="delivery_address_id", unique=true, nullable=true, updatable=true)
	public DeliveryAddress getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="deliveryOrder", cascade=CascadeType.MERGE)
	public Set<SalesOrder> getConfirmedSalesOrders() {
		return confirmedSalesOrders;
	}

	public void setConfirmedSalesOrders(Set<SalesOrder> confirmedSalesOrders) {
		this.confirmedSalesOrders = confirmedSalesOrders;
	}

	/**
	 * Possible values: 'N' - New, 'D' -Draft, 'A' - Accepted, 'R' - Rejected
	 * @return
	 */
	@Check(constraints="delivery_order_status IN ('N', 'D', 'A', 'C', 'E', 'R')")
	@Column(name="delivery_order_status", nullable=false, columnDefinition="CHAR(1) default 'D'")
	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Transient
	public boolean isNewOrder(){
		return (status != null && status == 'N');
	}
	
	@Transient
	public boolean isDraftOrder(){
		return (status != null && status == 'D');
	}
	
	@Transient
	public boolean isAcceptedOrder(){
		return (status != null && status == 'A');
	}
	
	@Transient
	public boolean isConfirmedOrder(){
		return (status != null && status == 'C');
	}
	
	@Transient
	public boolean isCompletedOrder(){
		return (status != null && status == 'E');
	}	
	
	@Transient
	public boolean isRejectedOrder(){
		return (status != null && status == 'R');
	}	
	
	
	@Transient
	public String getStatusName(){
		String statusName = "Unknown";
		switch (status){
			case 'N': statusName = "New"; break; 
			case 'D': statusName = "Draft"; break; 
			case 'A': statusName = "Accepted"; break;
			case 'R': statusName = "Rejected"; break;
			case 'C': statusName = "Confirmed"; break;
			case 'E': statusName = "Completed"; break;
		}
		
		return statusName;
	}
	
	@Transient
	public String getStatusCss(){
		String statusName = "label-default";
		switch (status){
			case 'N': statusName = "label-default"; break; 
			case 'D': statusName = "label-primary"; break; 
			case 'A': statusName = "label-warning"; break;
			case 'R': statusName = "label-danger"; break;
			case 'C': statusName = "label-success"; break;
			case 'E': statusName = "label-info"; break;
		}
		
		return statusName;
	}	
	
	@Transient
	public boolean isChangeCustomerAllowed(){
		return (isNewOrder() || isDraftOrder());
	}
	
	@Transient
	public boolean isChangeConfirmedSalesOrderAllowed(){
		return (isNewOrder() || isDraftOrder() || isAcceptedOrder());
	}	
	
	@Transient
	public boolean isChangeBillingAddressAllowed(){
		return (isNewOrder() || isDraftOrder() || isAcceptedOrder());
	}
	
	@Transient
	public boolean isChangeDeliveryAddressAllowed(){
		return (isNewOrder() || isDraftOrder() || isAcceptedOrder());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryOrder other = (DeliveryOrder) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DeliveryOrder [id=" + id + "]";
	}
}
