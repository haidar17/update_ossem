package com.ag.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="production_order_quantity_by_size")
public class ProductionOrderQuantityBySize extends BaseObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7231069997055172805L;
	private Long id;
	private String size;
	private Integer orderQuantity;
	private ProductionOrderItem productionOrderItem;
	
	public ProductionOrderQuantityBySize() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="po_quantity_by_size_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="size", nullable=false)
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	@Column(name="po_quantity", nullable=false)
	public Integer getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(Integer orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="production_order_item_id", nullable=false)
	public ProductionOrderItem getProductionOrderItem() {
		return productionOrderItem;
	}

	public void setProductionOrderItem(ProductionOrderItem productionOrderItem) {
		this.productionOrderItem = productionOrderItem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((productionOrderItem == null) ? 0 : productionOrderItem
						.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductionOrderQuantityBySize other = (ProductionOrderQuantityBySize) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (productionOrderItem == null) {
			if (other.productionOrderItem != null)
				return false;
		} else if (!productionOrderItem.equals(other.productionOrderItem))
			return false;
		if (size == null) {
			if (other.size != null)
				return false;
		} else if (!size.equals(other.size))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductionOrderQuantityBySize [id=" + id + ", size=" + size
				+ ", orderQuantity=" + orderQuantity + ", salesOrderItem="
				+ productionOrderItem + "]";
	}
}
