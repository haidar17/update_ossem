package com.ag.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="parties")
@Inheritance(strategy=InheritanceType.JOINED)  
//@DiscriminatorColumn(name="type",discriminatorType=DiscriminatorType.STRING)  
@DiscriminatorValue(value="party") 
public class Party extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1236116772600403393L;
	private Long id;
	private String fullName;
	private String legalIdentity;
	
	public Party() {
		super();
	}

	public Party(String fullName, String legalIdentity) {
		super();
		this.fullName = fullName;
		this.legalIdentity = legalIdentity;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="party_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="full_name", nullable=false)
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Column(name="legal_identity", nullable=true)//, unique=true)
	public String getLegalIdentity() {
		return legalIdentity;
	}

	public void setLegalIdentity(String legalIdentity) {
		this.legalIdentity = legalIdentity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Party other = (Party) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Party [id=" + id + ", fullName=" + fullName
				+ ", legalIdentity=" + legalIdentity + "]";
	}
}
