package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="email")
public class Email extends Contact {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3351490690204828194L;

	public Email() {
		super();
	}

	public Email(String contactValue) {
		super(contactValue);
	}
}
