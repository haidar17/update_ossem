package com.ag.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="product_inventory")
public class ProductInventory extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -910130872520562737L;
	private Long id;
	private Product product;
	private Date entryDate;
	private String entryNote;
	private Long entryCount;
	private Long currentCount;
	private Integer lowerLimitCount;
	private Integer upperLimitCount;
	
	public ProductInventory() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="product_inventory_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="product_id", nullable=true)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name="product_entry_date", nullable=false)
	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	@Column(name="product_entry_count", nullable=false)
	public Long getEntryCount() {
		return entryCount;
	}

	public void setEntryCount(Long entryCount) {
		this.entryCount = entryCount;
	}

	@Column(name="product_entry_note", nullable=true)
	public String getEntryNote() {
		return entryNote;
	}

	public void setEntryNote(String entryNote) {
		this.entryNote = entryNote;
	}

	public Long getCurrentCount() {
		return currentCount;
	}

	public void setCurrentCount(Long currentCount) {
		this.currentCount = currentCount;
	}

	@Column(name="product_lower_limit_count", nullable=true)
	public Integer getLowerLimitCount() {
		return lowerLimitCount;
	}

	public void setLowerLimitCount(Integer lowerLimitCount) {
		this.lowerLimitCount = lowerLimitCount;
	}

	@Column(name="product_upper_limit_count", nullable=true)
	public Integer getUpperLimitCount() {
		return upperLimitCount;
	}

	
	public void setUpperLimitCount(Integer upperLimitCount) {
		this.upperLimitCount = upperLimitCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductInventory other = (ProductInventory) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductInventory [id=" + id + ", product=" + product
				+ ", entryDate=" + entryDate + ", entryCount=" + entryCount
				+ ", entryNote=" + entryNote + ", currentCount=" + currentCount
				+ ", lowerLimitCount=" + lowerLimitCount + ", upperLimitCount="
				+ upperLimitCount + "]";
	}	
}
