package com.ag.model;

public class SimpleProductComponentForm {
	private String id;
	private String description;
	private String message;
	
	
	public SimpleProductComponentForm() {
		super();
	}	
	
	public SimpleProductComponentForm(ProductComponent productComponent) {
		super();
		if (productComponent != null){
			this.id =  productComponent.getId();
			this.description = productComponent.getDescription();
			this.message = "Product Component found!";
		} else {
			this.message = "Product Component could not be found!";
		}
	}		
	
	public SimpleProductComponentForm(String id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
