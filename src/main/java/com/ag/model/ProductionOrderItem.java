package com.ag.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="production_order_items")
public class ProductionOrderItem extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8924336735588735613L;
	private Long id;
	private Product product;
	private Integer quantity;
	private ProductionOrder productionOrder;
	private Set<ProductionOrderQuantityBySize> productionOrderQuantityBySizes;
	
	public ProductionOrderItem() {
		super();
	}
	
	public ProductionOrderItem(Product product, ProductionOrder productionOrder, Integer quantity) {
		super();
		this.product = product;
		this.productionOrder = productionOrder;
		this.quantity = quantity;
	}	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="po_item_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="product_id", nullable=false)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name="po_item_quantity", nullable=false)
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="production_order_id", nullable=false)
	public ProductionOrder getProductionOrder() {
		return productionOrder;
	}

	public void setProductionOrder(ProductionOrder productionOrder) {
		this.productionOrder = productionOrder;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="productionOrderItem", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<ProductionOrderQuantityBySize> getProductionOrderQuantityBySizes() {
		return productionOrderQuantityBySizes;
	}

	public void setProductionOrderQuantityBySizes(
			Set<ProductionOrderQuantityBySize> productionOrderQuantityBySizes) {
		this.productionOrderQuantityBySizes = productionOrderQuantityBySizes;
	}

	@Transient
	public boolean getHashProductionOrderQuantityBySizes(){
		return (productionOrderQuantityBySizes != null && !productionOrderQuantityBySizes.isEmpty());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductionOrderItem other = (ProductionOrderItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductionOrderItem [id=" + id + ", product=" + product
				+ ", quantity=" + quantity + ", productionOrder="
				+ (productionOrder != null? productionOrder.getOrderNo():"[null]") + ", productionOrderQuantityBySizes="
				+ (productionOrderQuantityBySizes != null ? productionOrderQuantityBySizes.size() : "[null]") + "]";
	}
}
