package com.ag.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="products")
public class Product extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6675260526832477131L;
	private Long id;
	private String name;
	private String code;
	private String description;
	private ProductSizeSet productSizeSet;
	private ProductComponentSet productComponentSet;
	
	public Product() {
		super();
	}

	public Product(String name, String code, String description) {
		super();
		this.name = name;
		this.code = code;
		this.description = description;
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="product_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="product_name", nullable=true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="product_code", nullable=false, unique=true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name="product_description", nullable=true)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="product_size_set_id", nullable=true)
	public ProductSizeSet getProductSizeSet() {
		return productSizeSet;
	}

	public void setProductSizeSet(ProductSizeSet productSizeSet) {
		this.productSizeSet = productSizeSet;
	}
	
	//START PRODUCT COMPONENT ---------------------------------
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="product_component_set_id", nullable=true)
	public ProductComponentSet getProductComponentSet() {
		return productComponentSet;
	}

	public void setProductComponentSet(ProductComponentSet productComponentSet) {
		this.productComponentSet = productComponentSet;
	}
	
	//END PRODUCT COMPONENT ----------------------------

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", code=" + code
				+ ", description=" + description + "]";
	}

}
