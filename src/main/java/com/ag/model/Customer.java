package com.ag.model;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="customers")
@DiscriminatorValue(value="customer")
public class Customer extends Party {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5298214598069110585L;
	private ContactPerson contactPerson;
	private BillingAddress billingAddress;
	private DeliveryAddress deliveryAddress;
	private Email email;
	
	public Customer() {
		super();
	}

	public Customer(String fullName, String legalIdentity, ContactPerson contactPerson) {
		super(fullName, legalIdentity);
		this.contactPerson =  contactPerson;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=false)
    @JoinColumn(name="contact_person_id", unique=true, nullable=false, updatable=true)
	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}
	
	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="email_id", unique=true, nullable=true, updatable=true)
	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}		
	
	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="billing_address_id", unique=true, nullable=true, updatable=true)	
	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="delivery_address_id", unique=true, nullable=true, updatable=true)
	public DeliveryAddress getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	@Transient
	public String getCustomerEmail(){
		if (this.email != null){
			return this.email.getContactValue();
		}
		
		return null;
	}
}
