package com.ag.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="delivery_order_items")
public class DeliveryOrderItem extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8924336735588735613L;
	private Long id;
	private Product product;
	private Integer quantity;
	private DeliveryOrder deliveryOrder;
	
	public DeliveryOrderItem() {
		super();
	}
	
	public DeliveryOrderItem(Product product, DeliveryOrder deliveryOrder, Integer quantity) {
		super();
		this.product = product;
		this.deliveryOrder = deliveryOrder;
		this.quantity = quantity;
	}	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="delivery_order_item_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="product_id", nullable=false)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name="delivery_order_item_quantity", nullable=false)
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="delivery_order_id", nullable=false)
	public DeliveryOrder getDeliveryOrder() {
		return deliveryOrder;
	}

	public void setDeliveryOrder(DeliveryOrder deliveryOrder) {
		this.deliveryOrder = deliveryOrder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryOrderItem other = (DeliveryOrderItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DeliveryOrderItem [id=" + id + ", product=" + product
				+ ", quantity=" + quantity + "]";
	}
}
