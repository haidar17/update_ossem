package com.ag.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Check;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

@Entity
@Table(name="production_orders")
@Indexed
public class ProductionOrder extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8294086643640549161L;
	private Long id;
	private String note;
	private String reference;
	private String orderNo;
	private Date orderDate;
	private Set<ProductionOrderItem> productionOrderItems;
	private Set<ProductionOrderItemAdjustment> adjustments;
	private SalesOrder salesOrder;
	private Character status;
	
	public ProductionOrder() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="production_order_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="production_order_note", nullable=true)
	@Field
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name="production_order_reference", nullable=true)
	@Field
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Column(name="production_order_no", nullable=false)
	@Field
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name="production_order_date", nullable=false)
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="productionOrder", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<ProductionOrderItem> getProductionOrderItems() {
		return productionOrderItems;
	}

	public void setProductionOrderItems(
			Set<ProductionOrderItem> productionOrderItems) {
		this.productionOrderItems = productionOrderItems;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="productionOrder", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<ProductionOrderItemAdjustment> getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(Set<ProductionOrderItemAdjustment> adjustments) {
		this.adjustments = adjustments;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="sales_order_id", nullable=true)
	public SalesOrder getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(SalesOrder salesOrder) {
		this.salesOrder = salesOrder;
	}

	/**
	 * Possible values: 'N' - New, 'D' -Draft, 'A' - Accepted, 'R' - Rejected,
	 * 'C' -Confirmed, 'E' - completed
	 * @return
	 */
	@Check(constraints="production_order_status IN ('N', 'D', 'A', 'R')")
	@Column(name="production_order_status", nullable=false, columnDefinition="CHAR(1) default 'D'")
	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Transient
	public boolean isNewOrder(){
		return (status != null && status == 'N');
	}
	
	@Transient
	public boolean isDraftOrder(){
		return (status != null && status == 'D');
	}
	
	@Transient
	public boolean isAcceptedOrder(){
		return (status != null && status == 'A');
	}

	@Transient
	public boolean isConfirmedOrder(){
		return (status != null && status == 'C');
	}
	
	@Transient
	public boolean isCompletedOrder(){
		return (status != null && status == 'E');
	}
	
	@Transient
	public boolean isRejectedOrder(){
		return (status != null && status == 'R');
	}	
	
	
	@Transient
	public String getStatusName(){
		String statusName = "Unknown";
		switch (status){
			case 'N': statusName = "New"; break; 
			case 'D': statusName = "Draft"; break; 
			case 'A': statusName = "Accepted"; break;
			case 'R': statusName = "Rejected"; break;
			case 'C': statusName = "Confirmed"; break;
			case 'E': statusName = "Completed"; break;
		}
		
		return statusName;
	}
	
	@Transient
	public String getStatusCss(){
		String statusName = "label-default";
		switch (status){
			case 'N': statusName = "label-default"; break; 
			case 'D': statusName = "label-primary"; break; 
			case 'A': statusName = "label-warning"; break;
			case 'R': statusName = "label-danger"; break;
			case 'C': statusName = "label-success"; break;
			case 'E': statusName = "label-info"; break;
		}
		
		return statusName;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductionOrder other = (ProductionOrder) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductionOrder [id=" + id + ", note=" + note + ", orderNo="
				+ orderNo + ", orderDate=" + orderDate
				+ ", productionOrderItems=" + (productionOrderItems != null ? productionOrderItems.size():0)
				+ ", adjustments=" + (adjustments != null ? adjustments.size():0) + ", status=" + status + "]";
	}	
}
