package com.ag.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="sales_order_items")
public class SalesOrderItem extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8924336735588735613L;
	private Long id;
	private Product product;
	private Integer orderQuantity;
	private Integer deliveryQuantity;
	private Double unitPrice;
	private SalesOrder salesOrder;
	private Set<SalesOrderQuantityBySize> salesOrderQuantityBySizes;  
	
	public SalesOrderItem() {
		super();
	}
	
	public SalesOrderItem(Product product, SalesOrder salesOrder, Integer orderQuantity, Integer deliveryQuantity, Double unitPrice) {
		super();
		this.product = product;
		this.salesOrder = salesOrder;
		this.orderQuantity = orderQuantity;
		this.deliveryQuantity = deliveryQuantity;
		this.unitPrice = unitPrice;
	}	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="sales_order_item_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="product_id", nullable=false)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name="sales_order_item_order_quantity", nullable=false)
	public Integer getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(Integer orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	@Column(name="sales_order_item_delivery_quantity", nullable=true, columnDefinition="int default 0")
	public Integer getDeliveryQuantity() {
		return deliveryQuantity;
	}

	public void setDeliveryQuantity(Integer deliveryQuantity) {
		this.deliveryQuantity = deliveryQuantity;
	}

	@Column(name="sales_order_item_unit_price", nullable=false)
	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="sales_order_id", nullable=false)
	public SalesOrder getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(SalesOrder salesOrder) {
		this.salesOrder = salesOrder;
	}

	@OneToMany(fetch=FetchType.EAGER, mappedBy="salesOrderItem", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<SalesOrderQuantityBySize> getSalesOrderQuantityBySizes() {
		return salesOrderQuantityBySizes;
	}

	public void setSalesOrderQuantityBySizes(
			Set<SalesOrderQuantityBySize> salesOrderQuantityBySizes) {
		this.salesOrderQuantityBySizes = salesOrderQuantityBySizes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesOrderItem other = (SalesOrderItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SalesOrderItem [id=" + id + ", product=" + product
				+ ", orderQuantity=" + orderQuantity + ", deliveryQuantity="
				+ deliveryQuantity + ", unitPrice=" + unitPrice
				+ ", salesOrder=" + salesOrder + ", salesOrderQuantityBySizes="
				+ salesOrderQuantityBySizes + "]";
	}
}
