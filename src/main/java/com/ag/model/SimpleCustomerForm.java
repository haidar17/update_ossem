package com.ag.model;

public class SimpleCustomerForm {
	private Long id;
	private String fullName;
	private String legalIdentity;
	private String firstName;
	private String lastName;
	private String message;
	
	public SimpleCustomerForm() {
		super();
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLegalIdentity() {
		return legalIdentity;
	}

	public void setLegalIdentity(String legalIdentity) {
		this.legalIdentity = legalIdentity;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
