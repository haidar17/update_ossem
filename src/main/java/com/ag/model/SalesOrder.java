package com.ag.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Check;

@Entity
@Table(name="sales_orders")
public class SalesOrder extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8589851189343540096L;
	private Long id;
	private String note;
	private String reference;
	private String orderNo;
	private Date orderDate;
	private Customer customer;
	private Set<SalesOrderItem> salesOrderItems;
	private Character status;
	private Character deliveryStatus;
	private DeliveryOrder deliveryOrder;
	
	public SalesOrder() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="sales_order_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="sales_order_note", nullable=true)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name="sales_order_reference", nullable=true)
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Column(name="sales_order_no", nullable=false)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name="sales_order_date", nullable=false)
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	@JoinColumn(name="customer_id", nullable=false)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="salesOrder", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<SalesOrderItem> getSalesOrderItems() {
		return salesOrderItems;
	}

	public void setSalesOrderItems(Set<SalesOrderItem> salesOrderItems) {
		this.salesOrderItems = salesOrderItems;
	}

	/**
	 * Possible values: 'N' - New, 'D' -Draft, 'A' - Accepted, 'R' - Rejected, 'E' - Completed
	 * @return
	 */
	@Check(constraints="order_status IN ('N', 'D', 'A', 'R', 'C', 'E')")
	@Column(name="order_status", nullable=false, columnDefinition="CHAR(1) default 'D'")
	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	/**
	 * Possible values: 'N' - new, 'W' - In warehouse, 'D' - Delivery In Progress, 
	 * 'd' - Delivered, 'R' - Returned, 'U' - Undelivered
	 * @return
	 */
	@Check(constraints="order_delivery_status IN ('N', 'W', 'D', 'd', 'R', 'U')")
	@Column(name="order_delivery_status", nullable=false, columnDefinition="CHAR(1) default 'W'")
	public Character getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Character deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	
	@Transient
	public boolean isNewOrder(){
		return (status != null && status == 'N');
	}
	
	@Transient
	public boolean isDraftOrder(){
		return (status != null && status == 'D');
	}
	
	@Transient
	public boolean isAcceptedOrder(){
		return (status != null && status == 'A');
	}
	
	@Transient
	public boolean isRejectedOrder(){
		return (status != null && status == 'R');
	}	
	
	@Transient
	public boolean isConfirmedOrder(){
		return (status != null && status == 'C');
	}
	
	@Transient
	public boolean isCompletedOrder(){
		return (status != null && status == 'E');
	}	
	
	@Transient
	public boolean isDeliveryInProcess(){
		return (deliveryStatus != null && deliveryStatus == 'W');
	}
	
	@Transient
	public boolean isDeliveryInProgress(){
		return (deliveryStatus != null && deliveryStatus == 'D');
	}
	
	@Transient
	public boolean isDelivered(){
		return (deliveryStatus != null && deliveryStatus == 'd');
	}
	
	@Transient
	public boolean isReturned(){
		return (deliveryStatus != null && deliveryStatus == 'R');
	}
	
	@Transient
	public boolean isUndelivered(){
		return (deliveryStatus != null && deliveryStatus == 'U');
	}
	
	@Transient
	public String getStatusName(){
		String statusName = "Unknown";
		switch (status){
			case 'N': statusName = "New"; break; 
			case 'D': statusName = "Draft"; break; 
			case 'A': statusName = "Accepted"; break;
			case 'R': statusName = "Rejected"; break;
			case 'C': statusName = "Confirmed"; break;
			case 'E': statusName = "Completed"; break;
		}
		
		return statusName;
	}
	
	@Transient
	public String getStatusCss(){
		String statusName = "label-default";
		switch (status){
			case 'N': statusName = "label-default"; break; 
			case 'D': statusName = "label-primary"; break; 
			case 'A': statusName = "label-warning"; break;
			case 'R': statusName = "label-danger"; break;
			case 'C': statusName = "label-success"; break;
			case 'E': statusName = "label-info"; break;
		}
		
		return statusName;
	}	
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	@JoinColumn(name="delivery_order_id", nullable=true)
	public DeliveryOrder getDeliveryOrder() {
		return deliveryOrder;
	}

	public void setDeliveryOrder(DeliveryOrder deliveryOrder) {
		this.deliveryOrder = deliveryOrder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((orderNo == null) ? 0 : orderNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesOrder other = (SalesOrder) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (orderNo == null) {
			if (other.orderNo != null)
				return false;
		} else if (!orderNo.equals(other.orderNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SalesOrder [id=" + id + ", note=" + note + ", orderNo="
				+ orderNo + ", orderDate=" + orderDate + ", customer="
				+ customer + ", salesOrderItems=" + (salesOrderItems != null ? salesOrderItems.size() : "null" )+ "]";
	}
}
