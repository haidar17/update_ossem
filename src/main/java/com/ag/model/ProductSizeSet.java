package com.ag.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
/**
 * @author hamid
 *
 */
@Entity
@Table(name="product_size_sets")
@Indexed
public class ProductSizeSet extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2056684405453071249L;
	private Long id;
	private String name;
	private String description;
	private Set<ProductSize> productSizes;
	
	public ProductSizeSet() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="product_size_set_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="product_size_set_name", nullable=false)
	@Field
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="product_size_set_description", nullable=true)
	@Field
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	//@OneToMany(fetch=FetchType.EAGER, mappedBy="productSizeSet")
	@ManyToMany(fetch=FetchType.EAGER, cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@Fetch(FetchMode.JOIN)
	@JoinTable(
			name="product_size_set_sizes",
			joinColumns={@JoinColumn(name="product_size_set_id")},
			inverseJoinColumns={@JoinColumn(name="product_size_id")})
	public Set<ProductSize> getProductSizes() {
		return productSizes;
	}

	public void setProductSizes(Set<ProductSize> productSizes) {
		this.productSizes = productSizes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductSizeSet other = (ProductSizeSet) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductSizeSet [id=" + id + ", name=" + name + ", description="
				+ description + "]";
	}

}
