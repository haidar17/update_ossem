package com.ag.model;

public class SimpleProductSizeForm {
	private String id;
	private String description;
	private String message;
	
	
	public SimpleProductSizeForm() {
		super();
	}	
	
	public SimpleProductSizeForm(ProductSize productSize) {
		super();
		if (productSize != null){
			this.id =  productSize.getId();
			this.description = productSize.getDescription();
			this.message = "Product Size found!";
		} else {
			this.message = "Product Size could not be found!";
		}
	}		
	
	public SimpleProductSizeForm(String id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
