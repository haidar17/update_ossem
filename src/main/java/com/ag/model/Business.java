package com.ag.model;

public interface Business {
	public void setOfficeAddress(OfficeAddress officeAddress);
	public OfficeAddress getOfficeAddress();
	
	public void setOfficePhone(OfficePhone officePhone);
	public OfficePhone getOfficePhone();
	
	public void setOfficeFax(OfficeFax officeFax);
	public OfficeFax getOfficeFax();	
}
