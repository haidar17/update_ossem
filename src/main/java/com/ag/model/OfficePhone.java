package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="officePhone")
public class OfficePhone extends Contact {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5938768991445882568L;

	public OfficePhone() {
		super();
	}

	public OfficePhone(String contactValue) {
		super(contactValue);
	}
}
