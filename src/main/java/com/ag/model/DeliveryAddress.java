package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="delivery")
public class DeliveryAddress extends Addresss {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3943264006888550831L;

	public DeliveryAddress() {
		super();
	}

	public DeliveryAddress(String address) {
		super(address);
	}

}
