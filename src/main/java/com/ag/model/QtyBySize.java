package com.ag.model;

import java.io.Serializable;

public class QtyBySize implements Serializable, AutoCloseable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 552525479399100544L;
	private String id;
	private String size;
	private Integer orderQuantity;
	
	public QtyBySize() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Integer getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(Integer orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	@Override
	public String toString() {
		return "QtyBySize [id=" + id + ", size=" + size + ", orderQuantity="
				+ orderQuantity + "]";
	}

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub	
	}
}
