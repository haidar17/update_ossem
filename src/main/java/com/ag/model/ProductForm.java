package com.ag.model;

import org.springframework.beans.BeanUtils;

public class ProductForm {
	private Long id;
	private String name;
	private String code;
	private String description;
	private Long productSizeSetId;
	private String productSizeSetName;
	private Long productComponentSetId;
	private String productComponentSetName;
	
	public ProductForm() {
		super();
	}

	public ProductForm(Product product) {
		super();
		if (product != null){
			BeanUtils.copyProperties(product, this, new String[]{"productSizeSet"});
			
			if (product.getProductSizeSet() != null){
				this.productSizeSetId = product.getProductSizeSet() .getId();
				this.productSizeSetName = product.getProductSizeSet().getName();
			}
			
			//START PRODUCT COMPONENT ---------------------------------
			BeanUtils.copyProperties(product, this, new String[]{"productComponentSet"});
			
			if (product.getProductComponentSet() != null){
				this.productComponentSetId = product.getProductComponentSet() .getId();
				this.productComponentSetName = product.getProductComponentSet().getName();
			}
			//END PRODUCT COMPONENT ----------------------------
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getProductSizeSetId() {
		return productSizeSetId;
	}

	public void setProductSizeSetId(Long productSizeSetId) {
		this.productSizeSetId = productSizeSetId;
	}

	public String getProductSizeSetName() {
		return productSizeSetName;
	}

	public void setProductSizeSetName(String productSizeSetName) {
		this.productSizeSetName = productSizeSetName;
	}
	
	//START PRODUCT COMPONENT --------------------------------- 
	
	public Long getProductComponentSetId() {
		return productComponentSetId;
	}

	public void setProductComponentSetId(Long productComponentSetId) {
		this.productComponentSetId = productComponentSetId;
	}

	public String getProductComponentSetName() {
		return productComponentSetName;
	}

	public void setProductComponentSetName(String productComponentSetName) {
		this.productComponentSetName = productComponentSetName;
	}
	
	//END PRODUCT COMPONENT ---------------------------------
}
