package com.ag.model;

public class CustomerProductPricing {
	private Customer customer;
	private Long rowCount;
	
	public CustomerProductPricing() {
		super();
	}
	
	public CustomerProductPricing(Customer customer, Long rowCount) {
		super();
		this.customer = customer;
		this.rowCount = rowCount;
	}	

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getRowCount() {
		return rowCount;
	}

	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}
		
}
