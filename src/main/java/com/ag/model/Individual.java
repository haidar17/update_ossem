package com.ag.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="individuals")
@DiscriminatorValue(value="individual")
public class Individual extends Party implements Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = -953430247204579353L;
	private String firstName;
	private String lastName;
	private Character gender;
	
	public Individual() {
		super();
	}

	public Individual(String firstName, String lastName, String legalIdentity) {
		super(String.format("%s %s", firstName, lastName), legalIdentity);
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	@Column(name="first_name", nullable=false)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name="last_name", nullable=false)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name="gender")
	public Character getGender() {
		return gender;
	}

	public void setGender(Character gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName
				+ ", gender=" + gender + ", toString()=" + super.toString()
				+ "]";
	}

}
