package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="website")
public class Website extends Contact {

	/**
	 * 
	 */
	private static final long serialVersionUID = -540640056746750435L;

	public Website() {
		super();
	}

	public Website(String contactValue) {
		super(contactValue);
	}
}
