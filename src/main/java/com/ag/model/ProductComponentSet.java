package com.ag.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
/**
 * @author hamid
 *
 */
@Entity
@Table(name="product_component_sets")
@Indexed
public class ProductComponentSet extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2056684405453071249L;
	private Long id;
	private String name;
	private String description;
	private Set<ProductComponent> productComponents;
	
	public ProductComponentSet() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="product_component_set_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="product_component_set_name", nullable=false)
	@Field
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="product_component_set_description", nullable=true)
	@Field
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	//@OneToMany(fetch=FetchType.EAGER, mappedBy="productComponentSet")
	@ManyToMany(fetch=FetchType.EAGER, cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@Fetch(FetchMode.JOIN)
	@JoinTable(
			name="product_component_set_components",
			joinColumns={@JoinColumn(name="product_component_set_id")},
			inverseJoinColumns={@JoinColumn(name="product_component_id")})
	public Set<ProductComponent> getProductComponents() {
		return productComponents;
	}

	public void setProductComponents(Set<ProductComponent> productComponents) {
		this.productComponents = productComponents;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductComponentSet other = (ProductComponentSet) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductComponentSet [id=" + id + ", name=" + name + ", description="
				+ description + "]";
	}

}
