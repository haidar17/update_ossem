package com.ag.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="production_order_item_adjustments")
public class ProductionOrderItemAdjustment {
	private Long id;
	private String code;
	private Date date;
	private String reference;
	private String note;
	private Integer quantity;
	private ProductionOrder productionOrder;
	
	public ProductionOrderItemAdjustment() {
		super();
	}

	public ProductionOrderItemAdjustment(ProductionOrder productionOrder, 
											String code, 
											Date date,
											String reference, 
											Integer quantity) {
		super();
		this.productionOrder = productionOrder;
		this.quantity = quantity;
		this.code = code;
		this.date = date;
		this.reference = reference;
		this.quantity = quantity;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="po_item_adjustment_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="po_item_adjustment_code", nullable=false)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name="po_item_adjustment_date", nullable=false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date Date) {
		this.date = Date;
	}

	@Column(name="po_item_adjustment_reference", nullable=true)
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Column(name="po_item_adjustment_note", nullable=true)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name="po_item_adjustment_quantity", nullable=false)
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="production_order_id", nullable=false)
	public ProductionOrder getProductionOrder() {
		return productionOrder;
	}

	public void setProductionOrder(ProductionOrder productionOrder) {
		this.productionOrder = productionOrder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductionOrderItemAdjustment other = (ProductionOrderItemAdjustment) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductionOrderItemAdjustment [id=" + id + ", Date="
				+ date + ", Reference="
				+ reference + ", Note=" + note
				+ ", Amount=" + quantity
				+ ", productionOrder=" + productionOrder + "]";
	}	
}
