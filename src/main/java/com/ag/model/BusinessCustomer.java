package com.ag.model;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="business_customers")
@DiscriminatorValue(value="businessCustomer")
public class BusinessCustomer extends Customer implements Business {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5298214598069110585L;
	private OfficeAddress officeAddress;
	private OfficePhone officePhone;
	private OfficeFax officeFax;	
	private Website website;

	
	public BusinessCustomer() {
		super();
	}

	public BusinessCustomer(String fullName, String legalIdentity, ContactPerson contactPerson) {
		super(fullName, legalIdentity, contactPerson);
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="office_address_id", unique=true, nullable=true, updatable=true)
	public OfficeAddress getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(OfficeAddress officeAddress) {
		this.officeAddress = officeAddress;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="office_phone_id", unique=true, nullable=true, updatable=true)
	public OfficePhone getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(OfficePhone officePhone) {
		this.officePhone = officePhone;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="office_fax_id", unique=true, nullable=true, updatable=true)
	public OfficeFax getOfficeFax() {
		return officeFax;
	}

	public void setOfficeFax(OfficeFax officeFax) {
		this.officeFax = officeFax;
	}
	
	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="website_id", unique=true, nullable=true, updatable=true)
	public Website getWebsite() {
		return website;
	}

	public void setWebsite(Website website) {
		this.website = website;
	}
}
