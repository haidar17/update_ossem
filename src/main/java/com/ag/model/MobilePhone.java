package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="mobilePhone")
public class MobilePhone extends Contact {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3085835446276110270L;

	public MobilePhone() {
		super();
	}

	public MobilePhone(String contactValue) {
		super(contactValue);
	}
}
