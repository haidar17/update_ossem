package com.ag.model;

import java.util.Set;

import org.springframework.beans.BeanUtils;

public class ProductComponentSetForm {
	private Long id;
	private String name;
	private String description;
	private Set<ProductComponent> productComponents;
	
	public ProductComponentSetForm() {
		super();
	}

	public ProductComponentSetForm(ProductComponentSet productComponentSet) {
		super();
		if (productComponentSet != null){
			BeanUtils.copyProperties(productComponentSet, this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<ProductComponent> getProductComponents() {
		return productComponents;
	}

	public void setProductComponents(Set<ProductComponent> productComponents) {
		this.productComponents = productComponents;
	}
	
	public Integer getProductComponentCount(){
		return (productComponents != null ? productComponents.size():0);
	}
	
	public boolean getHasProductComponents(){
		return (productComponents != null && !productComponents.isEmpty());
	}
}
