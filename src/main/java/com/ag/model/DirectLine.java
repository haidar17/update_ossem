package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="directLine")
public class DirectLine extends Contact {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7710671490800078079L;

	public DirectLine() {
		super();
	}

	public DirectLine(String contactValue) {
		super(contactValue);
	}

}
