package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="homeAddress")
public class HomeAddress extends Addresss {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3513871519797424666L;

	public HomeAddress() {
		super();
	}

	public HomeAddress(String address) {
		super(address);
	}
	
}
