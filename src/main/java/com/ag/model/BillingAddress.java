package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="billing")
public class BillingAddress extends Addresss {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6839228538981986305L;

	public BillingAddress() {
		super();
	}

	public BillingAddress(String address) {
		super(address);
	}

}
