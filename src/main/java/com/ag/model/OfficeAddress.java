package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="officeAddress")
public class OfficeAddress extends Addresss {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8224890006997410312L;

	public OfficeAddress() {
		super();
	}

	public OfficeAddress(String address) {
		super(address);
	}
}
