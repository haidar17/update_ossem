package com.ag.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
/**
 * This is model template for other domain model
 * You may copy and build your own however you see it fit your intended domain model. 
 * Important: Please do not edit or update any part of this file.
 * 
 * @author hamid
 *
 */
@Entity
@Table(name="product_sizes")
@Indexed
public class ProductSize extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2056684405453071249L;
	private String id;
	private String description;
	private Set<ProductSizeSet> productSizeSets;
	
	public ProductSize() {
		super();
	}

	public ProductSize(String id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	@Id
	@Column(name="product_size_id", nullable=false, unique=true)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name="product_size_description", nullable=true)
	@Field
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

//	@ManyToMany(fetch=FetchType.EAGER, mappedBy="productSizes")
//	public Set<ProductSizeSet> getProductSizeSets() {
//		return productSizeSets;
//	}
//
//	public void setProductSizeSets(Set<ProductSizeSet> productSizeSets) {
//		this.productSizeSets = productSizeSets;
//	}
//	
//	@Transient
//	public void addProductSizeSet(ProductSizeSet productSizeSet){
//		if (productSizeSet == null) return;
//		if (productSizeSets == null) productSizeSets = new HashSet<ProductSizeSet>(); 
//		
//		productSizeSets.add(productSizeSet);
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductSize other = (ProductSize) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductSize [id=" + id + ", description=" + description + "]";
	}
}
