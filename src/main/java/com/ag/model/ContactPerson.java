package com.ag.model;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="contact_persons")
@DiscriminatorValue(value="contactPerson")
public class ContactPerson extends Individual {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3624963071763673142L;
	private Email email;
	private MobilePhone mobilePhone;
	private DirectLine directLine;
	
	public ContactPerson() {
		super();
	}

	public ContactPerson(String firstName, String lastName, String legalIdentity) {
		super(firstName, lastName, legalIdentity);
	}
	

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="email_id", unique=true, nullable=true, updatable=true)
	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="mobile_phone_id", unique=true, nullable=true, updatable=true)
	public MobilePhone getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(MobilePhone mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="direct_line_id", unique=true, nullable=true, updatable=true)
	public DirectLine getDirectLine() {
		return directLine;
	}

	public void setDirectLine(DirectLine directLine) {
		this.directLine = directLine;
	}

	@Transient
	public void setContactEmail(String email){
		if (this.email == null) this.email = new Email();
		this.email.setContactValue(email != null ? email:"");
	}
	
	@Transient
	public String getContactEmail(){
		if (this.email != null){
			return this.email.getContactValue();
		}
		
		return null;
	}
	
	@Transient
	public void setContactMobilePhone(String mobilePhone){
		if (this.mobilePhone == null) this.mobilePhone = new MobilePhone();
		this.mobilePhone.setContactValue(mobilePhone != null ? mobilePhone :"");
	}
	
	@Transient
	public String getContactMobilePhone(){
		if (this.mobilePhone != null){
			return this.mobilePhone.getContactValue();
		}
		
		return null;
	}
	
	@Transient
	public String getContactDirectLine(){
		if (this.directLine != null){
			return this.directLine.getContactValue();
		}
		
		return null;
	}	
	
	@Override
	public String toString() {
		return "ContactPerson [email=" + email + ", mobilePhone=" + mobilePhone
				+ ", toString()=" + super.toString() + "]";
	}

}
