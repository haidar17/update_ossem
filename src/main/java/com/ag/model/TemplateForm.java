package com.ag.model;

import org.springframework.beans.BeanUtils;

public class TemplateForm {
	private Long id;
	private String name;
	private String description;
	
	public TemplateForm() {
		super();
	}

	public TemplateForm(Template template) {
		super();
		if (template != null){
			BeanUtils.copyProperties(template, this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
