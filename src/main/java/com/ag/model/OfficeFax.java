package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="officeFax")
public class OfficeFax extends Contact {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6822878212754820627L;

	public OfficeFax() {
		super();
	}

	public OfficeFax(String contactValue) {
		super(contactValue);
	}
}
