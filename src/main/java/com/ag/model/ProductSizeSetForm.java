package com.ag.model;

import java.util.Set;

import org.springframework.beans.BeanUtils;

public class ProductSizeSetForm {
	private Long id;
	private String name;
	private String description;
	private Set<ProductSize> productSizes;
	
	public ProductSizeSetForm() {
		super();
	}

	public ProductSizeSetForm(ProductSizeSet productSizeSet) {
		super();
		if (productSizeSet != null){
			BeanUtils.copyProperties(productSizeSet, this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<ProductSize> getProductSizes() {
		return productSizes;
	}

	public void setProductSizes(Set<ProductSize> productSizes) {
		this.productSizes = productSizes;
	}
	
	public Integer getProductSizeCount(){
		return (productSizes != null ? productSizes.size():0);
	}
	
	public boolean getHasProductSizes(){
		return (productSizes != null && !productSizes.isEmpty());
	}
}
