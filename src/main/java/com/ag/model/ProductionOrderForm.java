package com.ag.model;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.BeanUtils;

public class ProductionOrderForm {
	private Long id;
	private String note;
	private String reference;
	private String orderNo;
	private Date orderDate;
	private Set<ProductionOrderItem> productionOrderItems;
	private Set<ProductionOrderItemAdjustment> adjustments;
	private Character status;
	
	public ProductionOrderForm() {
		super();
	}

	public ProductionOrderForm(ProductionOrder productionOrder) {
		super();
		if (productionOrder != null){
			BeanUtils.copyProperties(productionOrder, this);
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Set<ProductionOrderItem> getProductionOrderItems() {
		return productionOrderItems;
	}

	public void setProductionOrderItems(
			Set<ProductionOrderItem> productionOrderItems) {
		this.productionOrderItems = productionOrderItems;
	}

	public Set<ProductionOrderItemAdjustment> getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(Set<ProductionOrderItemAdjustment> adjustments) {
		this.adjustments = adjustments;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}	
	
	public boolean isNewOrder(){
		return (status != null && status == 'N');
	}
	
	public boolean isDraftOrder(){
		return (status != null && status == 'D');
	}
	
	public boolean isAcceptedOrder(){
		return (status != null && status == 'A');
	}
	
	public boolean isRejectedOrder(){
		return (status != null && status == 'R');
	}	
	
	public boolean isConfirmedOrder(){
		return (status != null && status == 'C');
	}
	
	public boolean isCompletedOrder(){
		return (status != null && status == 'E');
	}
	
	public String getStatusName(){
		String statusName = "Unknown";
		switch (status){
			case 'N': statusName = "New"; break; 
			case 'D': statusName = "Draft"; break; 
			case 'A': statusName = "Accepted"; break;
			case 'R': statusName = "Rejected"; break;
			case 'C': statusName = "Confirmed"; break;
			case 'E': statusName = "Completed"; break;			
		}
		
		return statusName;
	}
	
	public boolean getHasProductionOrderItems(){
		return (productionOrderItems != null && !productionOrderItems.isEmpty());
	}	
	
	public boolean getHasAdjustments(){
		return (adjustments != null && !adjustments.isEmpty());
	}	
	
	public Integer getProductionOrderItemCount(){
		return (productionOrderItems != null ? productionOrderItems.size() : 0);
	}	
	
	public Integer getAdjustmentCount(){
		return (adjustments != null ? adjustments.size() : 0);
	}	
	
	public boolean isAddOrderItemAllowed(){
		return (isDraftOrder() || isNewOrder() || isAcceptedOrder());
	}
	
	public boolean isAddAdjustmentAllowed(){
		return (isAcceptedOrder() || isConfirmedOrder());
	}	
}
