package com.ag.model;

import org.springframework.beans.BeanUtils;

public class ProductPricingForm {
	private Long id;
	private Long customerId;
	private String customerName;
	private Long productId;
	private String productName;
	private String productCode;
	private Double price;
	
	public ProductPricingForm() {
		super();
	}

	public ProductPricingForm(Customer customer) {
		super();
		if (customer != null){
			this.customerId = customer.getId();
			this.customerName = customer.getFullName();
		}		
	}
	
	public ProductPricingForm(ProductPricing productPricing) {
		super();
		if (productPricing != null){
			BeanUtils.copyProperties(productPricing, this, new String[]{"customer", "product"});
			if (productPricing.getCustomer() != null){
				this.customerId = productPricing.getCustomer().getId();
				this.customerName = productPricing.getCustomer().getFullName();
			}
			if (productPricing.getProduct() != null){
				this.productId = productPricing.getProduct().getId();
				this.productName = productPricing.getProduct().getName();
				this.productCode = productPricing.getProduct().getCode();
			}			
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}	
}
