package com.ag.model;

import org.springframework.beans.BeanUtils;

public class CustomerForm {
	private Long id;
	private Email email;
	private String fullName;
	private String legalIdentity;
	private Long contactId;
	private String contactFirstName;
	private String contactLastName;
	private String contactEmail;
	private String contactMobilePhone;
	private Long billingAddressId;
	private String billingAddress;
	private String billingStreet;
	private String billingPostalCode;
	private String billingCity;
	private Long billingStateId;
	private Long billingCountryId;
	private Long deliveryAddressId;
	private String deliveryAddress;
	private String deliveryStreet;
	private String deliveryPostalCode;
	private String deliveryCity;
	private Long deliveryStateId;
	private Long deliveryCountryId;	

	public CustomerForm() {
		super();
	}

	public CustomerForm(Customer customer) {
		super();
		if (customer != null){
			BeanUtils.copyProperties(customer, this, new String[]{"contactPerson", "billingAddres", "deliveryAddress"});
			if (customer.getContactPerson() !=  null){
				this.contactId=customer.getContactPerson().getId();
				this.contactFirstName=customer.getContactPerson().getFirstName();
				this.contactLastName=customer.getContactPerson().getLastName();
				this.contactEmail = customer.getContactPerson().getContactEmail();
				this.contactMobilePhone = customer.getContactPerson().getContactMobilePhone();
			}
			
			if (customer.getBillingAddress() != null){
				this.billingAddressId = customer.getBillingAddress().getId();
				this.billingAddress = customer.getBillingAddress().getAddress();
				this.billingStreet = customer.getBillingAddress().getStreet();
				this.billingCity = customer.getBillingAddress().getCity();
				this.billingPostalCode = customer.getDeliveryAddress().getPostalCode();
				if (customer.getBillingAddress().getState() != null){
					this.billingStateId = customer.getBillingAddress().getState().getId();
					if (customer.getBillingAddress().getState().getCountry() != null){
						this.billingCountryId = customer.getBillingAddress().getState().getCountry().getId();
					}
				}
			}
			
			if (customer.getDeliveryAddress() != null){
				this.deliveryAddressId = customer.getDeliveryAddress().getId();
				this.deliveryAddress = customer.getDeliveryAddress().getAddress();
				this.deliveryStreet = customer.getDeliveryAddress().getStreet();
				this.deliveryCity = customer.getDeliveryAddress().getCity();
				this.deliveryPostalCode = customer.getDeliveryAddress().getPostalCode();
				if (customer.getDeliveryAddress().getState() != null){
					this.deliveryStateId = customer.getDeliveryAddress().getState().getId();
					if (customer.getDeliveryAddress().getState().getCountry() != null){
						this.deliveryCountryId = customer.getDeliveryAddress().getState().getCountry().getId();
					}
				}
			}			
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLegalIdentity() {
		return legalIdentity;
	}

	public void setLegalIdentity(String legalIdentity) {
		this.legalIdentity = legalIdentity;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactMobilePhone() {
		return contactMobilePhone;
	}

	public void setContactMobilePhone(String contactMobilePhone) {
		this.contactMobilePhone = contactMobilePhone;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBillingStreet() {
		return billingStreet;
	}

	public void setBillingStreet(String billingStreet) {
		this.billingStreet = billingStreet;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public Long getBillingStateId() {
		return billingStateId;
	}

	public void setBillingStateId(Long billingStateId) {
		this.billingStateId = billingStateId;
	}

	public Long getBillingCountryId() {
		return billingCountryId;
	}

	public void setBillingCountryId(Long billingCountryId) {
		this.billingCountryId = billingCountryId;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getDeliveryStreet() {
		return deliveryStreet;
	}

	public void setDeliveryStreet(String deliveryStreet) {
		this.deliveryStreet = deliveryStreet;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public Long getDeliveryStateId() {
		return deliveryStateId;
	}

	public void setDeliveryStateId(Long deliveryStateId) {
		this.deliveryStateId = deliveryStateId;
	}

	public Long getDeliveryCountryId() {
		return deliveryCountryId;
	}

	public void setDeliveryCountryId(Long deliveryCountryId) {
		this.deliveryCountryId = deliveryCountryId;
	}

	public String getBillingPostalCode() {
		return billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}

	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}

	public Long getBillingAddressId() {
		return billingAddressId;
	}

	public void setBillingAddressId(Long billingAddressId) {
		this.billingAddressId = billingAddressId;
	}

	public Long getDeliveryAddressId() {
		return deliveryAddressId;
	}

	public void setDeliveryAddressId(Long deliveryAddressId) {
		this.deliveryAddressId = deliveryAddressId;
	}

	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
}
