package com.ag.model;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.BeanUtils;

public class SalesOrderForm {
	private Long id;
	private String note;
	private String orderNo;
	private Date orderDate;
	private String customerFullName;
	private Long customerId;
	private Set<SalesOrderItem> salesOrderItems;
	private Character status;
	private Character deliveryStatus;
	private boolean selectedSalesOrder;
	
	public SalesOrderForm() {
		super();
	}

	public SalesOrderForm(SalesOrder salesOrder) {
		if (salesOrder != null){
			BeanUtils.copyProperties(salesOrder, this, new String[]{"customer"});

			if (salesOrder.getCustomer() != null){
				this.customerId = salesOrder.getCustomer().getId();
				this.customerFullName = salesOrder.getCustomer().getFullName();
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getCustomerFullName() {
		return customerFullName;
	}

	public void setCustomerFullName(String customerFullName) {
		this.customerFullName = customerFullName;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Set<SalesOrderItem> getSalesOrderItems() {
		return salesOrderItems;
	}

	public void setSalesOrderItems(Set<SalesOrderItem> salesOrderItems) {
		this.salesOrderItems = salesOrderItems;
	}
	
	public boolean getHasSalesOrderItems(){
		return (salesOrderItems != null && !salesOrderItems.isEmpty());
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public Character getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Character deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	
	
	public boolean isDeliveryInProcess(){
		return (deliveryStatus != null && deliveryStatus == 'W');
	}
	
	public boolean isDeliveryInProgress(){
		return (deliveryStatus != null && deliveryStatus == 'D');
	}
	
	public boolean isDelivered(){
		return (deliveryStatus != null && deliveryStatus == 'd');
	}
	
	public boolean isReturned(){
		return (deliveryStatus != null && deliveryStatus == 'R');
	}
	
	public boolean isUndelivered(){
		return (deliveryStatus != null && deliveryStatus == 'U');
	}	
	
	public boolean isNewOrder(){
		return (status != null && status == 'N');
	}
	
	public boolean isDraftOrder(){
		return (status != null && status == 'D');
	}
	
	public boolean isAcceptedOrder(){
		return (status != null && status == 'A');
	}
	
	public boolean isConfirmedOrder(){
		return (status != null && status == 'C');
	}	
	
	public boolean isRejectedOrder(){
		return (status != null && status == 'R');
	}	
	
	public boolean isCompletedOrder(){
		return (status != null && status == 'E');
	}
	
	public String getStatusName(){
		String statusName = "Unknown";
		switch (status){
			case 'N': statusName = "New"; break; 
			case 'D': statusName = "Draft"; break; 
			case 'A': statusName = "Accepted"; break;
			case 'R': statusName = "Rejected"; break;
			case 'C': statusName = "Confirmed"; break;
			case 'E': statusName = "Completed"; break;
		}
		
		return statusName;
	}
	
	public String getStatusCss(){
		String statusName = "label-default";
		switch (status){
			case 'N': statusName = "label-default"; break; 
			case 'D': statusName = "label-primary"; break; 
			case 'A': statusName = "label-warning"; break;
			case 'R': statusName = "label-danger"; break;
			case 'C': statusName = "label-success"; break;
			case 'E': statusName = "label-info"; break;
		}
		
		return statusName;
	}
	
	public Integer getSalesOrderItemCount(){
		return (salesOrderItems != null ? salesOrderItems.size() : 0);
	}
	
	public boolean isAddOrderItemAllowed(){
		return (isNewOrder() || isDraftOrder() || isAcceptedOrder());
	}

	public boolean isSelectedSalesOrder() {
		return selectedSalesOrder;
	}

	public void setSelectedSalesOrder(boolean selectedSalesOrder) {
		this.selectedSalesOrder = selectedSalesOrder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((orderNo == null) ? 0 : orderNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesOrderForm other = (SalesOrderForm) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (orderNo == null) {
			if (other.orderNo != null)
				return false;
		} else if (!orderNo.equals(other.orderNo))
			return false;
		return true;
	}
}
