package com.ag.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="homePhone")
public class HomePhone extends Contact {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5614663496755467492L;

	public HomePhone() {
		super();
	}

	public HomePhone(String contactValue) {
		super(contactValue);
	}

}
