package com.ag.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name="product_pricings",
	uniqueConstraints=@UniqueConstraint(name="product_pricings__UN", columnNames={"customer_id", "product_id"})
)
public class ProductPricing extends BaseObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4294440371695484558L;
	private Long id;
	private Customer customer;
	private Product product;
	private Double price;
	
	public ProductPricing() {
		super();
	}
	
	public ProductPricing(Customer customer, Product product, Double price) {
		super();
		this.customer = customer;
		this.product = product;
		this.price = price;
	}	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="product_pricings_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="customer_id", nullable=false)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="product_id", nullable=false)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name="product_price", nullable=false)
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductPricing other = (ProductPricing) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductPricing [id=" + id + ", customer=" + customer
				+ ", product=" + product + ", price=" + price + "]";
	}


}
