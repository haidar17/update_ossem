package com.ag.model;

import java.util.Date;

import org.springframework.beans.BeanUtils;

public class ProductInventoryForm {
	private Long id;
	private String entryNote;
	private Date entryDate;
	private Long entryCount;
	private Long currentCount;
	private Integer lowerLimitCount;
	private Integer upperLimitCount;
	private Long productId;
	private String productCode;
	private String productName;
	
	public ProductInventoryForm() {
		super();
	}

	public ProductInventoryForm(ProductInventory productInventory) {
		super();
		if (productInventory != null){
			BeanUtils.copyProperties(productInventory, this, new String[]{"product"});
			if (productInventory.getProduct() != null){
				this.productId = productInventory.getProduct().getId();
				this.productCode = productInventory.getProduct().getCode();
				this.productName = productInventory.getProduct().getName();
			}
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntryNote() {
		return entryNote;
	}

	public void setEntryNote(String entryNote) {
		this.entryNote = entryNote;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public Long getEntryCount() {
		return entryCount;
	}

	public void setEntryCount(Long entryCount) {
		this.entryCount = entryCount;
	}

	public Long getCurrentCount() {
		return currentCount;
	}

	public void setCurrentCount(Long currentCount) {
		this.currentCount = currentCount;
	}

	public Integer getLowerLimitCount() {
		return lowerLimitCount;
	}

	public void setLowerLimitCount(Integer lowerLimitCount) {
		this.lowerLimitCount = lowerLimitCount;
	}

	public Integer getUpperLimitCount() {
		return upperLimitCount;
	}

	public void setUpperLimitCount(Integer upperLimitCount) {
		this.upperLimitCount = upperLimitCount;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}	
}
