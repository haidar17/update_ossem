package com.ag.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="delivery_order_number_bin")
public class DeliveryOrderNumberBin {
	private Long id;
	private boolean leadingZeros;
	private boolean used;

	public DeliveryOrderNumberBin() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="delivery_order_number_id", nullable=false, unique=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="is_leading_zeros", nullable=true)
	public boolean isLeadingZeros() {
		return leadingZeros;
	}

	public void setLeadingZeros(boolean leadingZeros) {
		this.leadingZeros = leadingZeros;
	}

	@Column(name="is_used", nullable=false)
	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryOrderNumberBin other = (DeliveryOrderNumberBin) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SalesOrderNumberBin [id=" + id + ", leadingZeros="
				+ leadingZeros + ", used=" + used + "]";
	}
}
