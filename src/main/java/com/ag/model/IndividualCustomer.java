package com.ag.model;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="individual_customers")
@DiscriminatorValue(value="individualCustomer")
public class IndividualCustomer extends Customer implements Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7949923659169585045L;
	private HomeAddress homeAddress;
	private MobilePhone mobilePhone;
	
	public IndividualCustomer() {
		super();
	}

	public IndividualCustomer(String firstName, String lastName, String legalIdentity){
		this(firstName, lastName, legalIdentity, new ContactPerson(firstName, lastName, legalIdentity));
	}
	
	public IndividualCustomer(String firstName, String lastName, String legalIdentity, ContactPerson contactPerson) {
		super(String.format("%s %s", firstName, lastName), legalIdentity, contactPerson);
	}
	

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="home_address_id", unique=true, nullable=true, updatable=true)
	public HomeAddress getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(HomeAddress homeAddress) {
		this.homeAddress = homeAddress;
	}
	

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, optional=true)
    @JoinColumn(name="mobile_phone_id", unique=true, nullable=true, updatable=true)
	public MobilePhone getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(MobilePhone mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@Override
	public void setFirstName(String firstName) {
		getContactPerson().setFirstName(firstName);
	}

	@Override
	@Transient
	public String getFirstName() {
		return getContactPerson().getFirstName();
	}

	@Override
	public void setLastName(String lastName) {
		getContactPerson().setLastName(lastName);
	}

	@Override
	@Transient
	public String getLastName() {
		return getContactPerson().getLastName();
	}

	@Override
	public void setGender(Character gender) {
		getContactPerson().setGender(gender);
	}

	@Override
	@Transient
	public Character getGender() {
		return getContactPerson().getGender();
	}	
}
