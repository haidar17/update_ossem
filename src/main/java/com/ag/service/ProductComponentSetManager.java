package com.ag.service;

import java.util.List;

import com.ag.model.ProductComponent;
import com.ag.model.ProductComponentSet;
/**
 * @author hamid
 *
 */
public interface ProductComponentSetManager extends GenericManager<ProductComponentSet, Long> {

	List<ProductComponentSet> search(String searchTerm);
	//public List<Template> search(String query);

	ProductComponent getProductComponentById(String productComponentId);

	ProductComponent getProductComponentWithSetsById(String productComponentId);

	List<ProductComponentSet> getAllDistinct();
}
