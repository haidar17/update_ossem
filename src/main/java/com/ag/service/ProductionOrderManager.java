package com.ag.service;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import com.ag.model.LabelValue;
import com.ag.model.Product;
import com.ag.model.ProductionOrder;
/**
 * @author hamid
 *
 */
@WebService
@Path("/productionOrders")
public interface ProductionOrderManager extends GenericManager<ProductionOrder, Long> {

	List<ProductionOrder> search(String searchTerm);

	ProductionOrder getWithOrderItems(Long id);

	ProductionOrder getWithOrderItemsAndAdjustments(Long id);

	Product getProductByCode(String productCode);

	@GET
	@Path("/adjustmentCodes")
	List<LabelValue> getAdjustmentCodes(@QueryParam("query") String query);
}
