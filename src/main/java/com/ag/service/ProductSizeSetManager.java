package com.ag.service;

import java.util.List;

import com.ag.model.ProductSize;
import com.ag.model.ProductSizeSet;
/**
 * @author hamid
 *
 */
public interface ProductSizeSetManager extends GenericManager<ProductSizeSet, Long> {

	List<ProductSizeSet> search(String searchTerm);
	//public List<Template> search(String query);

	ProductSize getProductSizeById(String productSizeId);

	ProductSize getProductSizeWithSetsById(String productSizeId);

	List<ProductSizeSet> getAllDistinct();
}
