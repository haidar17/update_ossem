package com.ag.service;

import java.util.List;

import com.ag.model.Customer;
import com.ag.model.Product;
import com.ag.model.SalesOrder;

public interface SalesOrderManager extends GenericManager<SalesOrder, Long> {

	public Customer getCustomerById(Long customerId);

	public SalesOrder getWithSalesOrderItems(Long id);

	public Product getProductByCode(String productCode);

	public List<SalesOrder> getByCustomerId(Long customerId);

	public  List<SalesOrder> getConfirmedSalesOrderByCustomerId(Long customerId);

}
