package com.ag.service;

import com.ag.model.ProductionOrder;
import com.ag.model.ProductionOrderNumberBin;

public interface ProductionOrderNumberBinManager extends GenericManager<ProductionOrderNumberBin, Long> {

	public String getNextOrderNumber(boolean isLeadingZeros, Integer numberOfLeadinZero);

	public boolean updateOrderNumberBin(ProductionOrder productionOrder);

}
