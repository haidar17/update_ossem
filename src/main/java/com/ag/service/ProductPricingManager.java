package com.ag.service;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ag.model.Customer;
import com.ag.model.CustomerProductPricing;
import com.ag.model.Product;
import com.ag.model.ProductPricing;
import com.ag.model.SimpleProductPricingForm;
/**
 * @author hamid
 *
 */
@WebService
@Path("/productPricings")
public interface ProductPricingManager extends GenericManager<ProductPricing, Long> {

	List<ProductPricing> search(String searchTerm);
	//public List<ProductPricing> search(String query);

	Customer getCustomerById(Long customerId);

	Product getProductById(Long productId);

	List<ProductPricing> getByCustomer(Long id);

	List<CustomerProductPricing> getAllDistinctByCustomer();
	
	@GET
	@Path("/{customerId}/price/{productCode}")
	public Double getCustomerProductUnitPrice(@PathParam("customerId") Long customerId,
														@PathParam("productCode") String productCode);
	
	@GET
	@Path("/{customerId}/{productCode}/setPrice/{productPrice}")
	public boolean setCustomerProductUnitPrice(@PathParam("customerId") Long customerId,
														@PathParam("productCode") String productCode,
														@PathParam("productPrice") Double productPrice);	
	
	@POST
	@Path("/create")
	@ResponseBody public SimpleProductPricingForm createProductPricing(@RequestBody final SimpleProductPricingForm simpleProductPricingForm);
}
