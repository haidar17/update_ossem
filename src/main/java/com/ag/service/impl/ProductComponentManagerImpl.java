package com.ag.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.ag.dao.ProductComponentDao;
import com.ag.model.LabelValue;
import com.ag.model.ProductComponent;
import com.ag.model.SimpleProductComponentForm;
import com.ag.service.ProductComponentManager;
/**
 * @author hamid
 *
 */
@Service("productComponentManager")
public class ProductComponentManagerImpl extends GenericManagerImpl<ProductComponent, String> implements ProductComponentManager {
	private ProductComponentDao productComponentDao;

	@Autowired
	public ProductComponentManagerImpl(ProductComponentDao productComponentDao){
		super(productComponentDao);
		this.productComponentDao = productComponentDao;
	}

	@Override
	public List<ProductComponent> search(String searchTerm) {
		return super.search(searchTerm, ProductComponent.class);
	}

	@Override
	public List<LabelValue> suggestProductComponentById(String query) {
		List<LabelValue> result = new ArrayList<LabelValue>();
		List<ProductComponent> productComponents = productComponentDao.suggestById(query);
		
		if (productComponents != null && !productComponents.isEmpty()){
			for (ProductComponent ps: productComponents){
				result.add(new LabelValue(ps.getDescription(), ps.getId()));
			}
		}
		
		return result;
	}

	@Override
	public SimpleProductComponentForm createProductComponent(SimpleProductComponentForm simpleProductComponentForm) {
		// 1. find existing product
		ProductComponent productComponent = null;
		
		try {
			productComponent = productComponentDao.get(simpleProductComponentForm.getId());
		} catch (Exception e){
			//ignore
		}
		
		if (productComponent == null){			
			productComponent = new ProductComponent(simpleProductComponentForm.getId(),
										simpleProductComponentForm.getDescription());		
		
			try {
				productComponent = productComponentDao.save(productComponent);
				
			} catch (DataIntegrityViolationException e){
				e.printStackTrace();
				simpleProductComponentForm.setMessage("Existing product component record with the same code exists!");
				return simpleProductComponentForm;
				
			} catch (Exception e){
				e.printStackTrace();
				simpleProductComponentForm.setMessage("Unable to create product component record due to " + e.getMessage());
				return simpleProductComponentForm;
				
			}
			
			simpleProductComponentForm.setId(productComponent.getId());
			simpleProductComponentForm.setMessage("Product component record created successfully!");			
		} else {
			simpleProductComponentForm.setMessage("There is an existing product component record with the specified code. Aborted!");			
		}
		
		return simpleProductComponentForm;
	}

	@Override
	public SimpleProductComponentForm getProductComponent(String productComponentId) {
		ProductComponent productComponent = null;
		
		try {
			productComponent = productComponentDao.get(productComponentId);
		} catch (Exception e){
			//ignore
		}
		
		return new SimpleProductComponentForm(productComponent);
	}

}
