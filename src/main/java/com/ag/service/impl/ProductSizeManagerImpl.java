package com.ag.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.ag.dao.ProductSizeDao;
import com.ag.model.LabelValue;
import com.ag.model.Product;
import com.ag.model.ProductSize;
import com.ag.model.SimpleProductSizeForm;
import com.ag.service.ProductSizeManager;
/**
 * @author hamid
 *
 */
@Service("productSizeManager")
public class ProductSizeManagerImpl extends GenericManagerImpl<ProductSize, String> implements ProductSizeManager {
	private ProductSizeDao productSizeDao;

	@Autowired
	public ProductSizeManagerImpl(ProductSizeDao productSizeDao){
		super(productSizeDao);
		this.productSizeDao = productSizeDao;
	}

	@Override
	public List<ProductSize> search(String searchTerm) {
		return super.search(searchTerm, ProductSize.class);
	}

	@Override
	public List<LabelValue> suggestProductSizeById(String query) {
		List<LabelValue> result = new ArrayList<LabelValue>();
		List<ProductSize> productSizes = productSizeDao.suggestById(query);
		
		if (productSizes != null && !productSizes.isEmpty()){
			for (ProductSize ps: productSizes){
				result.add(new LabelValue(ps.getDescription(), ps.getId()));
			}
		}
		
		return result;
	}

	@Override
	public SimpleProductSizeForm createProductSize(SimpleProductSizeForm simpleProductSizeForm) {
		// 1. find existing product
		ProductSize productSize = null;
		
		try {
			productSize = productSizeDao.get(simpleProductSizeForm.getId());
		} catch (Exception e){
			//ignore
		}
		
		if (productSize == null){			
			productSize = new ProductSize(simpleProductSizeForm.getId(),
										simpleProductSizeForm.getDescription());		
		
			try {
				productSize = productSizeDao.save(productSize);
				
			} catch (DataIntegrityViolationException e){
				e.printStackTrace();
				simpleProductSizeForm.setMessage("Existing product size record with the same code exists!");
				return simpleProductSizeForm;
				
			} catch (Exception e){
				e.printStackTrace();
				simpleProductSizeForm.setMessage("Unable to create product size record due to " + e.getMessage());
				return simpleProductSizeForm;
				
			}
			
			simpleProductSizeForm.setId(productSize.getId());
			simpleProductSizeForm.setMessage("Product size record created successfully!");			
		} else {
			simpleProductSizeForm.setMessage("There is an existing product size record with the specified code. Aborted!");			
		}
		
		return simpleProductSizeForm;
	}

	@Override
	public SimpleProductSizeForm getProductSize(String productSizeId) {
		ProductSize productSize = null;
		
		try {
			productSize = productSizeDao.get(productSizeId);
		} catch (Exception e){
			//ignore
		}
		
		return new SimpleProductSizeForm(productSize);
	}

}
