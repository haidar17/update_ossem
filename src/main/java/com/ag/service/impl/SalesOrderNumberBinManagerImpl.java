package com.ag.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.SalesOrderNumberBinDao;
import com.ag.model.Customer;
import com.ag.model.SalesOrderNumberBin;
import com.ag.model.SalesOrder;
import com.ag.service.SalesOrderNumberBinManager;

@Service("salesOrderNumberBinManager")
public class SalesOrderNumberBinManagerImpl extends GenericManagerImpl<SalesOrderNumberBin, Long> implements SalesOrderNumberBinManager {
	private SalesOrderNumberBinDao salesOrderNumberBinDao;
	
	@Autowired
	public SalesOrderNumberBinManagerImpl(SalesOrderNumberBinDao salesOrderNumberBinDao){
		super(salesOrderNumberBinDao);
		this.salesOrderNumberBinDao = salesOrderNumberBinDao;
	}
	
	@Override
	public String getNextOrderNumber(boolean isLeadingZeros, Integer numberOfLeadinZero){
		
		SalesOrderNumberBin orderNumberBin = new SalesOrderNumberBin();
		orderNumberBin.setLeadingZeros(isLeadingZeros);
		orderNumberBin = salesOrderNumberBinDao.save(orderNumberBin);
		
		String format = "%0" + numberOfLeadinZero + "d";
		
		return String.format(format, orderNumberBin.getId());
	}

	@Override
	public boolean updateOrderNumberBin(SalesOrder salesOrder) {		
		Long orderNumber = Long.parseLong(salesOrder.getOrderNo());
		SalesOrderNumberBin orderNumberBin = salesOrderNumberBinDao.get(orderNumber);
		
		if (orderNumberBin != null){
			orderNumberBin.setUsed(true);
			orderNumberBin.setLeadingZeros(true);
			try {
				orderNumberBin = salesOrderNumberBinDao.save(orderNumberBin);
				return true;
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		
		return false;
	}
}
