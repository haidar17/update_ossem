package com.ag.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.CustomerDao;
import com.ag.dao.ProductDao;
import com.ag.dao.SalesOrderDao;
import com.ag.model.Customer;
import com.ag.model.Product;
import com.ag.model.SalesOrder;
import com.ag.service.SalesOrderManager;

@Service("salesOrderManager")
public class SalesOrderManagerImpl extends GenericManagerImpl<SalesOrder, Long> implements SalesOrderManager{
	private SalesOrderDao salesOrderDao;
	private CustomerDao customerDao;
	private ProductDao productDao;
	
	@Autowired
	public SalesOrderManagerImpl(SalesOrderDao salesOrderDao){
		super(salesOrderDao);
		this.salesOrderDao = salesOrderDao;
	}

	@Autowired
	public void setCustomerDao(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}

	@Autowired
	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}

	@Override
	public Customer getCustomerById(Long customerId) {
		return customerDao.get(customerId);
	}

	@Override
	public SalesOrder getWithSalesOrderItems(Long id) {
		SalesOrder so = salesOrderDao.getWithSalesOrderItems(id);
		log.debug("so=" + so.getSalesOrderItems());
		
		return so;
	}

	@Override
	public Product getProductByCode(String productCode) {
		return productDao.getByCode(productCode);
	}

	@Override
	public List<SalesOrder> getByCustomerId(Long customerId) {
		return salesOrderDao.getByCustomerId(customerId);
	}

	@Override
	public List<SalesOrder> getConfirmedSalesOrderByCustomerId(Long customerId) {
		return salesOrderDao.getConfirmedSalesOrderByCustomerId(customerId);
	}
}
