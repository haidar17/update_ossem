package com.ag.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.ProductInventoryDao;
import com.ag.model.ProductInventory;
import com.ag.service.ProductInventoryManager;

@Service("productInventoryManager")
public class ProductInventoryManagerImpl extends GenericManagerImpl<ProductInventory, Long> implements ProductInventoryManager {
	private ProductInventoryDao productInventoryDao;
	
	@Autowired
	public ProductInventoryManagerImpl(ProductInventoryDao productInventoryDao){
		super(productInventoryDao);
		this.productInventoryDao = productInventoryDao;
	}
}
