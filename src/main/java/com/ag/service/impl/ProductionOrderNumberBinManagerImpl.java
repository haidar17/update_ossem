package com.ag.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.ProductionOrderNumberBinDao;
import com.ag.model.ProductionOrder;
import com.ag.model.ProductionOrderNumberBin;
import com.ag.service.ProductionOrderNumberBinManager;

@Service("productionOrderNumberBinManager")
public class ProductionOrderNumberBinManagerImpl extends GenericManagerImpl<ProductionOrderNumberBin, Long> implements ProductionOrderNumberBinManager {
	private ProductionOrderNumberBinDao productionOrderNumberBinDao;
	
	@Autowired
	public ProductionOrderNumberBinManagerImpl(ProductionOrderNumberBinDao productionOrderNumberBinDao){
		super(productionOrderNumberBinDao);
		this.productionOrderNumberBinDao = productionOrderNumberBinDao;
	}
	
	@Override
	public String getNextOrderNumber(boolean isLeadingZeros, Integer numberOfLeadinZero){
		
		ProductionOrderNumberBin orderNumberBin = new ProductionOrderNumberBin();
		orderNumberBin.setLeadingZeros(isLeadingZeros);
		orderNumberBin = productionOrderNumberBinDao.save(orderNumberBin);
		
		String format = "%0" + numberOfLeadinZero + "d";
		
		return String.format(format, orderNumberBin.getId());
	}

	@Override
	public boolean updateOrderNumberBin(ProductionOrder productionOrder) {		
		Long orderNumber = Long.parseLong(productionOrder.getOrderNo());
		ProductionOrderNumberBin orderNumberBin = productionOrderNumberBinDao.get(orderNumber);
		
		if (orderNumberBin != null){
			orderNumberBin.setUsed(true);
			orderNumberBin.setLeadingZeros(true);
			try {
				orderNumberBin = productionOrderNumberBinDao.save(orderNumberBin);
				return true;
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		
		return false;
	}
}
