package com.ag.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.ag.dao.CustomerDao;
import com.ag.dao.ProductDao;
import com.ag.dao.ProductPricingDao;
import com.ag.model.Customer;
import com.ag.model.CustomerProductPricing;
import com.ag.model.Product;
import com.ag.model.ProductPricing;
import com.ag.model.SimpleProductPricingForm;
import com.ag.service.ProductPricingManager;
/**
 * @author hamid
 *
 */
@Service("productPricingManager")
public class ProductPricingManagerImpl extends GenericManagerImpl<ProductPricing, Long> implements ProductPricingManager {
	private ProductPricingDao productPricingDao;
	private CustomerDao customerDao;
	private ProductDao productDao;
	
	@Autowired
	public ProductPricingManagerImpl(ProductPricingDao templateDao){
		super(templateDao);
		this.productPricingDao = templateDao;
	}

	@Autowired
	public void setCustomerDao(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}

	@Autowired
	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}

	@Override
	public List<ProductPricing> search(String searchTerm) {
		return super.search(searchTerm, ProductPricing.class);
	}

	@Override
	public Customer getCustomerById(Long customerId) {
		return customerDao.get(customerId);
	}

	@Override
	public Product getProductById(Long productId) {
		return productDao.get(productId);
	}

	@Override
	public List<ProductPricing> getByCustomer(Long id) {
		return productPricingDao.getAllByCustomer(id);
	}

	@Override
	public List<CustomerProductPricing> getAllDistinctByCustomer() {
		return productPricingDao.AllDistinctByCustomer();
	}

	@Override
	public Double getCustomerProductUnitPrice(Long customerId,
			String productCode) {
		ProductPricing pp = productPricingDao.findCustomerProductUnitPrice(customerId, productCode);
		if (pp != null) return pp.getPrice();
		
		return null;
	}

	@Override
	public boolean setCustomerProductUnitPrice(Long customerId,
			String productCode, Double productPrice) {
		Customer customer = customerDao.get(customerId);
		Product product = productDao.getByCode(productCode);
		return productPricingDao.setCustomerProductUnitPrice(customer, product, productPrice);
	}

	@Override
	public SimpleProductPricingForm createProductPricing(
			SimpleProductPricingForm simpleProductPricingForm){
		
		log.debug("find existing product");
		// 1. find existing product
		Product product =  productDao.getByCode(simpleProductPricingForm.getProductCode());
		
		if (product == null) {
			product = new Product(simpleProductPricingForm.getProductName(),
									simpleProductPricingForm.getProductCode(), null);
			try {
				product = productDao.save(product);
				
			} catch (Exception e){
				//e.printStackTrace();
				simpleProductPricingForm.setMessage("Unable to save product record due to " + e.getMessage());
				return simpleProductPricingForm;
			}
		}
		
		log.debug("find specified customer");
		// 2. find specified customer
		Customer customer = customerDao.get(simpleProductPricingForm.getCustomerId());
		
		if (customer == null){
			simpleProductPricingForm.setMessage("Specified customer could not be found!");
			return simpleProductPricingForm;
		}
		
		log.debug("create product pricing");
		// 3. create product pricing
		ProductPricing productPricing = productPricingDao.findCustomerProductUnitPrice(simpleProductPricingForm.getCustomerId(), 
																							simpleProductPricingForm.getProductCode());
		if (productPricing != null){
			simpleProductPricingForm.setMessage("Product pricing for the specified customer and product exists!");
			return simpleProductPricingForm;
			
		}
		
		try {
			productPricing = new ProductPricing(customer, product, simpleProductPricingForm.getPrice());
			productPricing = productPricingDao.save(productPricing);
		 
		} catch (DataIntegrityViolationException e){
			e.printStackTrace();
			simpleProductPricingForm.setMessage("Product pricing for existing customer and product exists!");
			return simpleProductPricingForm;
			
		} catch (Exception e){
			e.printStackTrace();
			simpleProductPricingForm.setMessage("Unable to save product pricing record due to " + e.getMessage());
			return simpleProductPricingForm;
		}
		
		log.debug("product pricing created");
		simpleProductPricingForm.setId(productPricing.getId());
		simpleProductPricingForm.setMessage("Product pricing created successfully!");
		
		return simpleProductPricingForm;
	}

}
