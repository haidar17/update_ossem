package com.ag.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.ProductComponentDao;
import com.ag.dao.ProductComponentSetDao;
import com.ag.model.ProductComponent;
import com.ag.model.ProductComponentSet;
import com.ag.service.ProductComponentSetManager;
/**
 * @author hamid
 *
 */
@Service("productComponentSetManager")
public class ProductComponentSetManagerImpl extends GenericManagerImpl<ProductComponentSet, Long> implements ProductComponentSetManager {
	private ProductComponentSetDao productComponentSetDao;
	private ProductComponentDao productComponentDao;
	

	@Autowired
	public ProductComponentSetManagerImpl(ProductComponentSetDao productComponentSetDao){
		super(productComponentSetDao);
		this.productComponentSetDao = productComponentSetDao;
	}

	@Autowired
	public void setProductComponentDao(ProductComponentDao productComponentDao) {
		this.productComponentDao = productComponentDao;
	}

	@Override
	public List<ProductComponentSet> search(String searchTerm) {
		return super.search(searchTerm, ProductComponentSet.class);
	}

	@Override
	public ProductComponent getProductComponentById(String productComponentId) {
		return productComponentDao.get(productComponentId);
	}

	@Override
	public ProductComponent getProductComponentWithSetsById(String productComponentId) {
		return productComponentDao.getProductComponentWithSetsById(productComponentId);
	}

	@Override
	public List<ProductComponentSet> getAllDistinct() {
		return productComponentSetDao.getAllDistinct();
	}

}
