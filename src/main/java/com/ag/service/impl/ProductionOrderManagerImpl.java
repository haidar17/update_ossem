package com.ag.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ag.dao.ProductDao;
import com.ag.dao.ProductionOrderDao;
import com.ag.model.LabelValue;
import com.ag.model.Product;
import com.ag.model.ProductionOrder;
import com.ag.service.ProductionOrderManager;
/**
 * @author hamid
 *
 */
@Service("productionOrderManager")
public class ProductionOrderManagerImpl extends GenericManagerImpl<ProductionOrder, Long> implements ProductionOrderManager {
	private ProductionOrderDao productionOrderDao;
	private ProductDao productDao;
	private List<LabelValue> adjustmentCodes;
	private static Map<String, List<LabelValue>> adjustmentCodeMap = new HashMap<String, List<LabelValue>>();
	
	@Value("#{productionOrderProps['adjustment.code.keyvalue']}") 
	private String adjustmentCodeKeyValue;
	
	@Autowired
	public ProductionOrderManagerImpl(ProductionOrderDao productionOrderDao){
		super(productionOrderDao);
		this.productionOrderDao = productionOrderDao;
	}
		
	@Autowired
	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}

	@Override
	public List<ProductionOrder> search(String searchTerm) {
		return super.search(searchTerm, ProductionOrder.class);
	}

	@Override
	public ProductionOrder getWithOrderItems(Long id) {
		return productionOrderDao.getWithOrderItems(id);
	}

	@Override
	public ProductionOrder getWithOrderItemsAndAdjustments(Long id) {
		return productionOrderDao.getWithOrderItemsAndAdjustments(id);
	}

	@Override
	public Product getProductByCode(String productCode) {
		return productDao.getByCode(productCode);
	}

	@Override
	public List<LabelValue> getAdjustmentCodes(String query) {
		log.debug("query=" + query);
		log.debug("adjustmentCodeKeyValue=" + adjustmentCodeKeyValue);
		if (query == null || StringUtils.isEmpty(query)) return null;
		if (adjustmentCodeMap.containsKey(query)) return adjustmentCodeMap.get(query);
		
		adjustmentCodes = new ArrayList<LabelValue>();
		String[] keyValues = adjustmentCodeKeyValue.split(",");

		for (String keyValue:keyValues){
			String[] keyAndValue = keyValue.split(":");
			if (keyAndValue[0].contains(query)) adjustmentCodes.add( new LabelValue(String.format("%s|%s", keyAndValue[0], keyAndValue[1]), keyAndValue[0]));
		}			
		adjustmentCodeMap.put(query, adjustmentCodes);
		
		return adjustmentCodes;
	}
}
