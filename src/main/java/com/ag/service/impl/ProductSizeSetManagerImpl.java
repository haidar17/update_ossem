package com.ag.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.ProductSizeDao;
import com.ag.dao.ProductSizeSetDao;
import com.ag.model.ProductSize;
import com.ag.model.ProductSizeSet;
import com.ag.service.ProductSizeSetManager;
/**
 * @author hamid
 *
 */
@Service("productSizeSetManager")
public class ProductSizeSetManagerImpl extends GenericManagerImpl<ProductSizeSet, Long> implements ProductSizeSetManager {
	private ProductSizeSetDao productSizeSetDao;
	private ProductSizeDao productSizeDao;
	

	@Autowired
	public ProductSizeSetManagerImpl(ProductSizeSetDao productSizeSetDao){
		super(productSizeSetDao);
		this.productSizeSetDao = productSizeSetDao;
	}

	@Autowired
	public void setProductSizeDao(ProductSizeDao productSizeDao) {
		this.productSizeDao = productSizeDao;
	}

	@Override
	public List<ProductSizeSet> search(String searchTerm) {
		return super.search(searchTerm, ProductSizeSet.class);
	}

	@Override
	public ProductSize getProductSizeById(String productSizeId) {
		return productSizeDao.get(productSizeId);
	}

	@Override
	public ProductSize getProductSizeWithSetsById(String productSizeId) {
		return productSizeDao.getProductSizeWithSetsById(productSizeId);
	}

	@Override
	public List<ProductSizeSet> getAllDistinct() {
		return productSizeSetDao.getAllDistinct();
	}

}
