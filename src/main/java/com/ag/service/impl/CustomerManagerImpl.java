package com.ag.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.ag.dao.AddresssDao;
import com.ag.dao.CustomerDao;
import com.ag.model.BillingAddress;
import com.ag.model.BusinessCustomer;
import com.ag.model.ContactPerson;
import com.ag.model.Customer;
import com.ag.model.DeliveryAddress;
import com.ag.model.LabelValue;
import com.ag.model.SimpleCustomerForm;
import com.ag.service.CustomerManager;

@Service("customerManager")
public class CustomerManagerImpl extends GenericManagerImpl<Customer, Long> implements CustomerManager {
	private CustomerDao customerDao;
	private AddresssDao addresssDao;
	
	@Autowired
	public CustomerManagerImpl(CustomerDao customerDao){
		super(customerDao);
		this.customerDao = customerDao;
	}

	@Autowired
	public void setAddresssDao(AddresssDao addresssDao) {
		this.addresssDao = addresssDao;
	}

	@Override
	public List<LabelValue> suggestByCustomerName(String query) {
		List<LabelValue> result = new ArrayList<LabelValue>();
		List<Customer> customers = customerDao.findByCustomerName(query);
		for (Customer customer:customers){
			result.add(new LabelValue(String.format("%s | %s", customer.getLegalIdentity(), customer.getFullName()), ""+customer.getId()));
		}
		
		return result;
	}

	@Override
	public DeliveryAddress getDeliveryAddress(Long customerId) {
		Customer customer = customerDao.get(customerId);
		if (customer != null && customer.getDeliveryAddress() != null) return customer.getDeliveryAddress();
		
		return null;
	}

	@Override
	public BillingAddress getBillingAddress(Long customerId) {
		Customer customer = customerDao.get(customerId);
		if (customer != null && customer.getBillingAddress() != null) return customer.getBillingAddress();
		
		return null;
	}

	@Override
	public DeliveryAddress getDeliveryAddressById(Long deliveryAddressId) {
		Object object =  addresssDao.get(deliveryAddressId);		
		if (object != null) return (DeliveryAddress)object;
		
		return null;
	}

	@Override
	public BillingAddress getBillingAddressById(Long billingAddressId) {
		Object object =  addresssDao.get(billingAddressId);		
		if (object != null) return (BillingAddress)object;
		
		return null;
	}


	@Override
	public SimpleCustomerForm createCustomer(SimpleCustomerForm simpleCustomerForm) {
		
		ContactPerson contactPerson = new ContactPerson(simpleCustomerForm.getFirstName(),
															simpleCustomerForm.getLastName(),
															null);
		Customer customer = new BusinessCustomer(simpleCustomerForm.getFullName(),
													simpleCustomerForm.getLegalIdentity(),
													contactPerson);
		try {
			customer = customerDao.save(customer);
		 
		} catch (DataIntegrityViolationException e){
			e.printStackTrace();
			simpleCustomerForm.setMessage("Unable to create record due to " + e.getMessage());
			return simpleCustomerForm;
			
		} catch (Exception e){
			e.printStackTrace();
			simpleCustomerForm.setMessage("Unable to create record due to " + e.getMessage());
			return simpleCustomerForm;
			
		}
		
		simpleCustomerForm.setId(customer.getId());
		simpleCustomerForm.setMessage("Customer record created successfully");
		
		return simpleCustomerForm;
	}
}
