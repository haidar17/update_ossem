package com.ag.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.AddresssDao;
import com.ag.dao.CustomerDao;
import com.ag.dao.DeliveryOrderDao;
import com.ag.dao.SalesOrderDao;
import com.ag.model.BillingAddress;
import com.ag.model.Customer;
import com.ag.model.DeliveryAddress;
import com.ag.model.DeliveryOrder;
import com.ag.model.SalesOrder;
import com.ag.service.DeliveryOrderManager;

@Service("deliveryOrderManager")
public class DeliveryOrderManagerImpl extends GenericManagerImpl<DeliveryOrder, Long> implements DeliveryOrderManager {
	private DeliveryOrderDao deliveryOrderDao;
	private SalesOrderDao salesOrderDao;
	private CustomerDao customerDao;
	private AddresssDao addresssDao;
	
	@Autowired
	public DeliveryOrderManagerImpl(DeliveryOrderDao deliveryOrderDao){
		super(deliveryOrderDao);
		this.deliveryOrderDao = deliveryOrderDao;
	}

	@Autowired
	public void setSalesOrderDao(SalesOrderDao salesOrderDao) {
		this.salesOrderDao = salesOrderDao;
	}

	@Autowired
	public void setCustomerDao(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}

	@Autowired
	public void setAddresssDao(AddresssDao addresssDao) {
		this.addresssDao = addresssDao;
	}

	@Override
	public List<SalesOrder> getByCustomerId(Long customerId) {
		return salesOrderDao.getByCustomerId(customerId);
	}

	@Override
	public Customer getCustomerById(Long customerId) {
		return customerDao.get(customerId);
	}

	@Override
	public BillingAddress getBillingAddressById(Long billingAddressId) {
		Object object =  addresssDao.get(billingAddressId);		
		if (object != null) return (BillingAddress)object;
		
		return null;		
	}

	@Override
	public DeliveryAddress getDeliveryAddressById(Long deliveryAddressId) {
		Object object =  addresssDao.get(deliveryAddressId);		
		if (object != null) return (DeliveryAddress)object;
		
		return null;
	}

	@Override
	public DeliveryOrder getWithConfirmedSalesOrders(Long id) {
		return deliveryOrderDao.getWithConfirmedSalesOrders(id);
	}

	@Override
	public List<SalesOrder> getConfirmedSalesOrdersByCustomerId(Long customerId) {
		return salesOrderDao.getConfirmedSalesOrderByCustomerId(customerId);
	}

	@Override
	public SalesOrder getSalesOrderById(Long id) {
		log.debug("salesOrderDao ==  null :" + (salesOrderDao == null));
		log.debug("id ==  null :" + (id == null));
		SalesOrder salesOrder = salesOrderDao.get(id);
		log.debug("salesOrder == null:"+ (salesOrder == null));
		
		return salesOrder;
	}
}
