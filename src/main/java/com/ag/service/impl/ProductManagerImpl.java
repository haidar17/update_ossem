package com.ag.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.ag.dao.ProductDao;
import com.ag.dao.ProductSizeSetDao;
import com.ag.dao.ProductComponentSetDao;
import com.ag.model.LabelValue;
import com.ag.model.Product;
import com.ag.model.ProductSizeSet;
import com.ag.model.ProductComponentSet;
import com.ag.model.SimpleProductForm;
import com.ag.service.ProductManager;

@Service("productManager")
public class ProductManagerImpl extends GenericManagerImpl<Product, Long> implements ProductManager {
	private ProductDao productDao;
	private ProductSizeSetDao productSizeSetDao;
	private ProductComponentSetDao productComponentSetDao;
	
	@Autowired
	public ProductManagerImpl(ProductDao productDao){
		super(productDao);
		this.productDao = productDao;
	}

	@Autowired
	public void setProductSizeSetDao(ProductSizeSetDao productSizeSetDao) {
		this.productSizeSetDao = productSizeSetDao;
	}
	
	//START PRODUCT COMPONENT ---------------------------------
	@Autowired
	public void setProductComponentSetDao(ProductComponentSetDao productComponentSetDao) {
		this.productComponentSetDao = productComponentSetDao;
	}
	//END PRODUCT COMPONENT ---------------------------------


	@Override
	public List<LabelValue> suggestProductByCode( String query) {
		log.debug("query=" + query);
		List<LabelValue> result = new ArrayList<LabelValue>();
		List<Product> products = productDao.findProductByCode(query);
		for (Product product:products){
			result.add(new LabelValue(String.format("%s | %s", product.getCode(), product.getName()), ""+product.getId()));
		}
		
		return result;
	}


	@Override
	public SimpleProductForm createProduct(SimpleProductForm simpleProductForm) {
		
		
		// 1. find existing product
		Product product = productDao.getByCode(simpleProductForm.getProductCode());
		
		if (product == null){			
			product = new Product(simpleProductForm.getProductName(),
										simpleProductForm.getProductCode(), null);		
		
			try {
				product = productDao.save(product);
				
			} catch (DataIntegrityViolationException e){
				e.printStackTrace();
				simpleProductForm.setMessage("Existing product record with the same code exists!");
				return simpleProductForm;
				
			} catch (Exception e){
				e.printStackTrace();
				simpleProductForm.setMessage("Unable to create product record due to " + e.getMessage());
				return simpleProductForm;
				
			}
			
			simpleProductForm.setId(product.getId());
			simpleProductForm.setMessage("Product record created successfully!");			
		} else {
			simpleProductForm.setMessage("There is an existing product record with the specified code. Aborted!");			
		}
		
		return simpleProductForm;
	}


	@Override
	public List<LabelValue> getAllDistinctProductSizeSets() {
		log.debug("Entering getAllDistinctProductSizeSets...");
		List<LabelValue> result = new ArrayList<LabelValue>();
		List<ProductSizeSet> productSizeSets = productSizeSetDao.getAllDistinct();
		
		if (productSizeSets != null && !productSizeSets.isEmpty()){
			for (ProductSizeSet productSizeSet:productSizeSets){
				log.debug("productSizeSet.getId()=" + productSizeSet.getId());
				log.debug("productSizeSet.getName()=" + productSizeSet.getName());
				result.add(new LabelValue(productSizeSet.getName(), "" + productSizeSet.getId()));
			}
		}
		
		return result;
	}

	@Override
	public ProductSizeSet getProductSizeSetById(Long productSizeSetId) {
		return productSizeSetDao.get(productSizeSetId);
	}
	
	//START PRODUCT COMPONENT ---------------------------------
	@Override
	public List<LabelValue> getAllDistinctProductComponentSets() {
		log.debug("Entering getAllDistinctProductComponentSets...");
		List<LabelValue> result = new ArrayList<LabelValue>();
		List<ProductComponentSet> productComponentSets = productComponentSetDao.getAllDistinct();
		
		if (productComponentSets != null && !productComponentSets.isEmpty()){
			for (ProductComponentSet productComponentSet:productComponentSets){
				log.debug("productComponentSet.getId()=" + productComponentSet.getId());
				log.debug("productComponentSet.getName()=" + productComponentSet.getName());
				result.add(new LabelValue(productComponentSet.getName(), "" + productComponentSet.getId()));
			}
		}
		
		return result;
	}

	@Override
	public ProductComponentSet getProductComponentSetById(Long productComponentSetId) {
		return productComponentSetDao.get(productComponentSetId);
	}
	
	//END PRODUCT COMPONENT ---------------------------------
	
	@Override
	public List<Product> getAllDistinct() {
		return productDao.getAllDistinct();
	}

	@Override
	public ProductSizeSet getProductSizeSetByProductCode(String productCode) {
		Product product = productDao.getByCode(productCode);
		
		if (product != null){
			return (product.getProductSizeSet() != null? product.getProductSizeSet(): new ProductSizeSet());	
		}
		
		return new ProductSizeSet();
	}
}
