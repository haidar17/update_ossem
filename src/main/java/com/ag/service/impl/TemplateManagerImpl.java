package com.ag.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.TemplateDao;
import com.ag.model.Template;
import com.ag.service.TemplateManager;
/**
 * This is manager implementation template for other domain
 * You may copy and build your own however you see it fit your intended domain. 
 * Important: Please do not edit or update any part of this file.
 * @author hamid
 *
 */
@Service("templateManager")
public class TemplateManagerImpl extends GenericManagerImpl<Template, Long> implements TemplateManager {
	private TemplateDao templateDao;

	@Autowired
	public TemplateManagerImpl(TemplateDao templateDao){
		super(templateDao);
		this.templateDao = templateDao;
	}

	@Override
	public List<Template> search(String searchTerm) {
		return super.search(searchTerm, Template.class);
	}

}
