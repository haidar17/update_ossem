package com.ag.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag.dao.DeliveryOrderNumberBinDao;
import com.ag.model.DeliveryOrder;
import com.ag.model.DeliveryOrderNumberBin;
import com.ag.service.DeliveryOrderNumberBinManager;

@Service("deliveryOrderNumberBinManager")
public class DeliveryOrderNumberBinManagerImpl extends GenericManagerImpl<DeliveryOrderNumberBin, Long> implements DeliveryOrderNumberBinManager {
	private DeliveryOrderNumberBinDao deliveryOrderNumberBinDao;
	
	@Autowired
	public DeliveryOrderNumberBinManagerImpl(DeliveryOrderNumberBinDao deliveryOrderNumberBinDao){
		super(deliveryOrderNumberBinDao);
		this.deliveryOrderNumberBinDao = deliveryOrderNumberBinDao;
	}
	
	@Override
	public String getNextOrderNumber(boolean isLeadingZeros, Integer numberOfLeadinZero){
		
		DeliveryOrderNumberBin orderNumberBin = new DeliveryOrderNumberBin();
		orderNumberBin.setLeadingZeros(isLeadingZeros);
		orderNumberBin = deliveryOrderNumberBinDao.save(orderNumberBin);
		
		String format = "%0" + numberOfLeadinZero + "d";
		
		return String.format(format, orderNumberBin.getId());
	}

	@Override
	public boolean updateOrderNumberBin(DeliveryOrder deliveryOrder) {		
		Long orderNumber = Long.parseLong(deliveryOrder.getOrderNo());
		DeliveryOrderNumberBin orderNumberBin = deliveryOrderNumberBinDao.get(orderNumber);
		
		if (orderNumberBin != null){
			orderNumberBin.setUsed(true);
			orderNumberBin.setLeadingZeros(true);
			try {
				orderNumberBin = deliveryOrderNumberBinDao.save(orderNumberBin);
				return true;
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		
		return false;
	}
}
