package com.ag.service;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ag.model.BillingAddress;
import com.ag.model.Customer;
import com.ag.model.DeliveryAddress;
import com.ag.model.LabelValue;
import com.ag.model.SimpleCustomerForm;

@WebService
@Path("/customers")
public interface CustomerManager extends GenericManager<Customer, Long>{
	
	@GET
	@Path("/suggest")
    List<LabelValue> suggestByCustomerName(@QueryParam("query") String query);
	
	@GET
	@Path("/{customerId}/deliveryAddress")
    DeliveryAddress getDeliveryAddress(@PathParam("customerId") Long customerId);
	
	@GET
	@Path("/{customerId}/billingAddress")
    BillingAddress getBillingAddress(@PathParam("customerId") Long customerId);

	DeliveryAddress getDeliveryAddressById(Long deliveryAddressId);

	BillingAddress getBillingAddressById(Long billingAddressId);
	
	@POST
	@Path("/create")
	@ResponseBody SimpleCustomerForm createCustomer(@RequestBody final SimpleCustomerForm simpleCustomerForm);
}
