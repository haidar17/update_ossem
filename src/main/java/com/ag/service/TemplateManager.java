package com.ag.service;

import java.util.List;

import com.ag.model.Template;
import com.ag.model.User;
/**
 * This is manager template for other domain
 * You may copy and build your own however you see it fit your intended domain. 
 * Important: Please do not edit or update any part of this file.
 * @author hamid
 *
 */
public interface TemplateManager extends GenericManager<Template, Long> {

	List<Template> search(String searchTerm);
	//public List<Template> search(String query);
}
