package com.ag.service;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ag.model.LabelValue;
import com.ag.model.ProductSize;
import com.ag.model.SimpleProductSizeForm;
/**
 * @author hamid
 *
 */

@WebService
@Path("/productSizes")
public interface ProductSizeManager extends GenericManager<ProductSize, String> {

	List<ProductSize> search(String searchTerm);
	//public List<ProductSize> search(String query);
	
	@GET
	@Path("/suggest")
	List<LabelValue> suggestProductSizeById(@QueryParam("query") String query);
	
	@POST
	@Path("/create")
	@ResponseBody SimpleProductSizeForm createProductSize(@RequestBody final SimpleProductSizeForm simpleProductSize);	
	
	@GET
	@Path("/{productSizeId}")
	@ResponseBody SimpleProductSizeForm getProductSize(@PathParam("productSizeId") String productSizeId);
}
