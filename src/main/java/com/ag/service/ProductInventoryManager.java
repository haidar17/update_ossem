package com.ag.service;

import com.ag.model.ProductInventory;

public interface ProductInventoryManager extends GenericManager<ProductInventory, Long>{
	
}
