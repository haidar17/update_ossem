package com.ag.service;

import com.ag.model.DeliveryOrder;
import com.ag.model.DeliveryOrderNumberBin;

public interface DeliveryOrderNumberBinManager extends GenericManager<DeliveryOrderNumberBin, Long> {

	public String getNextOrderNumber(boolean isLeadingZeros, Integer numberOfLeadinZero);

	public boolean updateOrderNumberBin(DeliveryOrder deliveryOrder);

}
