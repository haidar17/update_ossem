package com.ag.service;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ag.model.LabelValue;
import com.ag.model.Product;
import com.ag.model.ProductComponentSet;
import com.ag.model.ProductSizeSet;
import com.ag.model.SimpleProductForm;

@WebService
@Path("/products")
public interface ProductManager extends GenericManager<Product, Long> {

	@GET
	@Path("/suggest")
    List<LabelValue> suggestProductByCode(@QueryParam("query") String query);
	
	@POST
	@Path("/create")
	@ResponseBody SimpleProductForm createProduct(@RequestBody final SimpleProductForm simpleProductForm);
	
	@GET
	@Path("/{productCode}/productSizeSet")
	@ResponseBody
	ProductSizeSet getProductSizeSetByProductCode(@PathParam("productCode") String productCode);

	List<LabelValue> getAllDistinctProductSizeSets();

	ProductSizeSet getProductSizeSetById(Long productSizeSetId);
	
	//START PRODUCT COMPONENT ---------------------------------
	List<LabelValue> getAllDistinctProductComponentSets();

	ProductComponentSet getProductComponentSetById(Long productComponentSetId);
	//END PRODUCT COMPONENT ---------------------------------

	List<Product> getAllDistinct();
}
