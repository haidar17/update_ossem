package com.ag.service;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ag.model.LabelValue;
import com.ag.model.ProductComponent;
import com.ag.model.SimpleProductComponentForm;
/**
 * @author hamid
 *
 */

@WebService
@Path("/productComponents")
public interface ProductComponentManager extends GenericManager<ProductComponent, String> {

	List<ProductComponent> search(String searchTerm);
	//public List<ProductSize> search(String query);
	
	@GET
	@Path("/suggest")
	List<LabelValue> suggestProductComponentById(@QueryParam("query") String query);
	
	@POST
	@Path("/create")
	@ResponseBody SimpleProductComponentForm createProductComponent(@RequestBody final SimpleProductComponentForm simpleProductComponent);	
	
	@GET
	@Path("/{productComponentId}")
	@ResponseBody SimpleProductComponentForm getProductComponent(@PathParam("productComponentId") String productComponentId);
}
