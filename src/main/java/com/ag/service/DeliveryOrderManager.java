package com.ag.service;

import java.util.List;
import java.util.Set;

import com.ag.model.BillingAddress;
import com.ag.model.Customer;
import com.ag.model.DeliveryAddress;
import com.ag.model.DeliveryOrder;
import com.ag.model.SalesOrder;

public interface DeliveryOrderManager extends GenericManager<DeliveryOrder, Long> {

	List<SalesOrder> getByCustomerId(Long customerId);

	Customer getCustomerById(Long customerId);

	BillingAddress getBillingAddressById(Long billingAddressId);

	DeliveryAddress getDeliveryAddressById(Long deliveryAddressId);

	DeliveryOrder getWithConfirmedSalesOrders(Long id);

	List<SalesOrder> getConfirmedSalesOrdersByCustomerId(Long customerId);

	SalesOrder getSalesOrderById(Long id);

}
