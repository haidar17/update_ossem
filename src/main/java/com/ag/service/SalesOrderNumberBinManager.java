package com.ag.service;

import com.ag.model.SalesOrderNumberBin;
import com.ag.model.SalesOrder;

public interface SalesOrderNumberBinManager extends GenericManager<SalesOrderNumberBin, Long> {

	public String getNextOrderNumber(boolean isLeadingZeros, Integer numberOfLeadinZero);

	public boolean updateOrderNumberBin(SalesOrder salesOrder);

}
