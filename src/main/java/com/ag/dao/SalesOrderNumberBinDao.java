package com.ag.dao;

import com.ag.model.SalesOrderNumberBin;

public interface SalesOrderNumberBinDao extends GenericDao<SalesOrderNumberBin, Long>{

}
