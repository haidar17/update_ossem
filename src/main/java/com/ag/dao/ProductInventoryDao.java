package com.ag.dao;

import com.ag.model.ProductInventory;

public interface ProductInventoryDao extends GenericDao<ProductInventory, Long> {

}
