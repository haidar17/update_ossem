package com.ag.dao;

import java.util.List;

import com.ag.model.Customer;

public interface CustomerDao extends GenericDao<Customer, Long> {

	List<Customer> findByCustomerName(String query);

}
