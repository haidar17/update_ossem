package com.ag.dao;

import com.ag.model.ProductSizeSet;
/**
 * @author hamid
 *
 */
public interface ProductSizeSetDao extends GenericDao<ProductSizeSet, Long>{

}
