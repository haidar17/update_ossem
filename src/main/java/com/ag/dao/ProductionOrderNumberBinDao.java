package com.ag.dao;

import com.ag.model.ProductionOrderNumberBin;
/**
 * @author hamid
 *
 */
public interface ProductionOrderNumberBinDao extends GenericDao<ProductionOrderNumberBin, Long>{

}
