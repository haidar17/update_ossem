package com.ag.dao;

import java.util.List;

import com.ag.model.ProductComponent;
/**
 * @author hamid
 *
 */
public interface ProductComponentDao extends GenericDao<ProductComponent, String>{

	List<ProductComponent> suggestById(String query);

	ProductComponent getProductComponentWithSetsById(String productComponentId);

}
