package com.ag.dao;

import java.util.List;

import com.ag.model.Product;

public interface ProductDao extends GenericDao<Product, Long>{

	List<Product> findProductByCode(String keyword);

	Product getByCode(String productCode);

}
