package com.ag.dao;

import com.ag.model.ProductComponentSet;
/**
 * @author hamid
 *
 */
public interface ProductComponentSetDao extends GenericDao<ProductComponentSet, Long>{

}
