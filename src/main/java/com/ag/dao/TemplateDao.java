package com.ag.dao;

import com.ag.model.Template;
/**
 * This is dao template for other domain
 * You may copy and build your own however you see it fit your intended domain. 
 * Important: Please do not edit or update any part of this file.
 * @author hamid
 *
 */
public interface TemplateDao extends GenericDao<Template, Long>{

}
