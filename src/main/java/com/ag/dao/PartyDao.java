package com.ag.dao;

import com.ag.model.Party;

public interface PartyDao extends GenericDao<Party, Long> {

}
