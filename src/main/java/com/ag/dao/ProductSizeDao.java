package com.ag.dao;

import java.util.List;

import com.ag.model.ProductSize;
/**
 * @author hamid
 *
 */
public interface ProductSizeDao extends GenericDao<ProductSize, String>{

	List<ProductSize> suggestById(String query);

	ProductSize getProductSizeWithSetsById(String productSizeId);

}
