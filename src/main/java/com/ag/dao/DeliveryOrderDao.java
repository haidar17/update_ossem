package com.ag.dao;

import com.ag.model.DeliveryOrder;

public interface DeliveryOrderDao extends GenericDao<DeliveryOrder, Long> {

	DeliveryOrder getWithConfirmedSalesOrders(Long id);
	
}
