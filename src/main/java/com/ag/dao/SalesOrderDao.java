package com.ag.dao;

import java.util.List;

import com.ag.model.SalesOrder;

public interface SalesOrderDao extends GenericDao<SalesOrder, Long>{

	SalesOrder getWithSalesOrderItems(Long id);

	List<SalesOrder> getByCustomerId(Long customerId);

	List<SalesOrder> getConfirmedSalesOrderByCustomerId(Long customerId);

}
