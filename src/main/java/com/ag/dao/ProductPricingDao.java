package com.ag.dao;

import java.util.List;

import com.ag.model.Customer;
import com.ag.model.CustomerProductPricing;
import com.ag.model.Product;
import com.ag.model.ProductPricing;

/**
 * This is dao template for other domain
 * You may copy and build your own however you see it fit your intended domain. 
 * Important: Please do not edit or update any part of this file.
 * @author hamid
 *
 */
public interface ProductPricingDao extends GenericDao<ProductPricing, Long>{

	List<ProductPricing> getAllByCustomer(Long id);

	List<CustomerProductPricing> AllDistinctByCustomer();

	ProductPricing findCustomerProductUnitPrice(Long customerId,
			String productCode);

	boolean setCustomerProductUnitPrice(Customer customer, Product product,
			Double productPrice);

}
