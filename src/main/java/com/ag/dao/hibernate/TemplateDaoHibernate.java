package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.TemplateDao;
import com.ag.model.Template;
/**
 * This is dao hibernate template for other domain
 * You may copy and build your own however you see it fit your intended domain. 
 * Important: Please do not edit or update any part of this file.
 * @author hamid
 *
 */
@Repository("templateDao")
public class TemplateDaoHibernate extends GenericDaoHibernate<Template, Long> implements TemplateDao {

	public TemplateDaoHibernate(){
		super(Template.class);
	}
}
