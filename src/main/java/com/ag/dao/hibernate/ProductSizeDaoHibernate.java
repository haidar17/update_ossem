package com.ag.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ag.dao.ProductSizeDao;
import com.ag.model.ProductSize;
/**
 * @author hamid
 *
 */
@Repository("productSizeDao")
public class ProductSizeDaoHibernate extends GenericDaoHibernate<ProductSize, String> implements ProductSizeDao {

	public ProductSizeDaoHibernate(){
		super(ProductSize.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductSize> suggestById(String query) {
		Criteria crit = getSession().createCriteria(ProductSize.class);
		crit.add(Restrictions.like("id", "%" + query + "%"));
		
		return crit.list();
	}

	@Override
	public ProductSize getProductSizeWithSetsById(String productSizeId) {
		Criteria crit = getSession().createCriteria(ProductSize.class);
		crit.add(Restrictions.eq("id", productSizeId));
		crit.setFetchMode("productSizeSets", FetchMode.JOIN);
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		Object object = crit.uniqueResult();
		if (object != null) return (ProductSize)object;
		
		return null;
		
	}
}
