package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.AddresssDao;
import com.ag.model.Addresss;

@Repository("addresssDao")
public class AddresssDaoHibernate extends GenericDaoHibernate<Addresss, Long> implements AddresssDao {

	public AddresssDaoHibernate() {
		super(Addresss.class);
	}

}
