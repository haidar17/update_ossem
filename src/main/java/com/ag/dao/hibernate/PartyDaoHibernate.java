package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.PartyDao;
import com.ag.model.Party;

@Repository("partyDao")
public class PartyDaoHibernate extends GenericDaoHibernate<Party, Long> implements PartyDao {

	public PartyDaoHibernate(){
		super(Party.class);
	}
}
