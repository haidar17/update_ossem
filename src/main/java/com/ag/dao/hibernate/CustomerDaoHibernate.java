package com.ag.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ag.dao.CustomerDao;
import com.ag.model.Customer;

@Repository("customerDao")
public class CustomerDaoHibernate extends GenericDaoHibernate<Customer, Long> implements CustomerDao {
	
	public CustomerDaoHibernate(){
		super(Customer.class);
	}

	@Override
	public List<Customer> findByCustomerName(String keyword) {
		Criteria crit = getSession().createCriteria(Customer.class);
		crit.add(Restrictions.like("fullName", "%" + keyword + "%"));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return crit.list();		
	}
}
