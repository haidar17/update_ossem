package com.ag.dao.hibernate;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ag.dao.DeliveryOrderDao;
import com.ag.model.DeliveryOrder;

@Repository("deliveryOrderDao")
public class DeliveryOrderDaoHibernate extends GenericDaoHibernate<DeliveryOrder, Long> implements DeliveryOrderDao {

	public DeliveryOrderDaoHibernate() {
		super(DeliveryOrder.class);
	}

	@Override
	public DeliveryOrder getWithConfirmedSalesOrders(Long id) {
		Criteria crit = getSession().createCriteria(DeliveryOrder.class);
		crit.add(Restrictions.eq("id", id));
		crit.setFetchMode("confirmedSalesOrders", FetchMode.JOIN);
		crit.setFetchMode("confirmedSalesOrders.salesOrderItems", FetchMode.JOIN);		
		

		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				
		Object object = crit.uniqueResult();
		if (object != null) return (DeliveryOrder)object;
		
		return null;
	}

}
