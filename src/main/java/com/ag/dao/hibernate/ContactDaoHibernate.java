package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.ContactDao;
import com.ag.model.Contact;

@Repository("contactDao")
public class ContactDaoHibernate extends GenericDaoHibernate<Contact, Long> implements ContactDao {

	public ContactDaoHibernate() {
		super(Contact.class);
	}

}
