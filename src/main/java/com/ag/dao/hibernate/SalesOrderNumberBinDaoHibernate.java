package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.SalesOrderNumberBinDao;
import com.ag.model.SalesOrderNumberBin;

@Repository("salesOrderNumberBinDao")
public class SalesOrderNumberBinDaoHibernate extends GenericDaoHibernate<SalesOrderNumberBin, Long> implements SalesOrderNumberBinDao {

	public SalesOrderNumberBinDaoHibernate(){
		super(SalesOrderNumberBin.class);
	}
}
