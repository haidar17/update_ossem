package com.ag.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ag.dao.SalesOrderDao;
import com.ag.model.SalesOrder;

@Repository("salesOrderDao")
public class SalesOrderDaoHibernate extends GenericDaoHibernate<SalesOrder, Long> implements SalesOrderDao {

	public SalesOrderDaoHibernate() {
		super(SalesOrder.class);
	}

	@Override
	public SalesOrder getWithSalesOrderItems(Long id) {
		Criteria crit = getSession().createCriteria(SalesOrder.class);
		crit.add(Restrictions.eq("id", id));
		crit.setFetchMode("salesOrderItems", FetchMode.JOIN);
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		Object object = crit.uniqueResult();
		if (object != null) return (SalesOrder)object;
		
		return null;
	}

	@Override
	public List<SalesOrder> getByCustomerId(Long customerId) {
		Criteria crit = getSession().createCriteria(SalesOrder.class);
		crit.add(Restrictions.eq("customer.id", customerId));
		crit.setFetchMode("salesOrderItems", FetchMode.JOIN);
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				
		return crit.list();
	}

	@Override
	public List<SalesOrder> getConfirmedSalesOrderByCustomerId(Long customerId) {
		Criteria crit = getSession().createCriteria(SalesOrder.class);
		crit.add(Restrictions.eq("customer.id", customerId));
		crit.add(Restrictions.eq ("status", 'C'));
		crit.add(Restrictions.isNull("deliveryOrder"));
		crit.setFetchMode("salesOrderItems", FetchMode.JOIN);
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				
		return crit.list();
	}

}
