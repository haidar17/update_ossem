package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.ProductSizeSetDao;
import com.ag.model.ProductSizeSet;
/**
 * @author hamid
 *
 */
@Repository("productSizeSetDao")
public class ProductSizeSetDaoHibernate extends GenericDaoHibernate<ProductSizeSet, Long> implements ProductSizeSetDao {

	public ProductSizeSetDaoHibernate(){
		super(ProductSizeSet.class);
	}
}
