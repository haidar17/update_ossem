package com.ag.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ag.dao.ProductPricingDao;
import com.ag.model.Customer;
import com.ag.model.CustomerProductPricing;
import com.ag.model.Product;
import com.ag.model.ProductPricing;
/**
 * This is dao hibernate template for other domain
 * You may copy and build your own however you see it fit your intended domain. 
 * Important: Please do not edit or update any part of this file.
 * @author hamid
 *
 */
@Repository("productPricingDao")
public class ProductPricingDaoHibernate extends GenericDaoHibernate<ProductPricing, Long> implements ProductPricingDao {

	public ProductPricingDaoHibernate(){
		super(ProductPricing.class);
	}

	@Override
	public List<ProductPricing> getAllByCustomer(Long id) {
		Criteria crit = getSession().createCriteria(ProductPricing.class);	
		crit.add(Restrictions.eq("customer.id", id));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return crit.list();	
	}

	@Override
	public List<CustomerProductPricing> AllDistinctByCustomer() {
		List<CustomerProductPricing> result = new ArrayList<CustomerProductPricing>();
		Criteria crit = getSession().createCriteria(ProductPricing.class);	
		ProjectionList projectionList = Projections.projectionList();
		projectionList.add(Projections.groupProperty("customer"));
		projectionList.add(Projections.rowCount());
		crit.setProjection(projectionList);
		List<Object[]> list = crit.list();
		for (Object[] obj : list) {
			CustomerProductPricing cpp = new CustomerProductPricing((Customer) obj[0], (Long) obj[1]);
			result.add(cpp);
	    }	
		
		return result;	
	}

	@Override
	public ProductPricing findCustomerProductUnitPrice(Long customerId,
			String productCode) {
		Criteria crit = getSession().createCriteria(ProductPricing.class);
		crit.createAlias("product", "p");
		crit.add(Restrictions.eq("customer.id", customerId));
		crit.add(Restrictions.eq("p.code", productCode));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		Object object = crit.uniqueResult();
		if (object != null) return (ProductPricing)object;
		
		return null;
		
	}

	@Override
	public boolean setCustomerProductUnitPrice(Customer customer, Product product,
			Double productPrice) {
		ProductPricing productPricing = findCustomerProductUnitPrice(customer.getId(), product.getCode());
		try {
			if (productPricing == null) productPricing = new ProductPricing();
			productPricing.setCustomer(customer);
			productPricing.setProduct(product);
			productPricing.setPrice(productPrice);
			
			productPricing = save(productPricing);
			return true;
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return false;
	}
}
