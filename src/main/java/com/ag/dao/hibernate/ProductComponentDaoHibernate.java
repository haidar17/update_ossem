package com.ag.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ag.dao.ProductComponentDao;
import com.ag.model.ProductComponent;
/**
 * @author hamid
 *
 */
@Repository("productComponentDao")
public class ProductComponentDaoHibernate extends GenericDaoHibernate<ProductComponent, String> implements ProductComponentDao {

	public ProductComponentDaoHibernate(){
		super(ProductComponent.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductComponent> suggestById(String query) {
		Criteria crit = getSession().createCriteria(ProductComponent.class);
		crit.add(Restrictions.like("id", "%" + query + "%"));
		
		return crit.list();
	}

	@Override
	public ProductComponent getProductComponentWithSetsById(String productComponentId) {
		Criteria crit = getSession().createCriteria(ProductComponent.class);
		crit.add(Restrictions.eq("id", productComponentId));
		crit.setFetchMode("productComponentSets", FetchMode.JOIN);
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		Object object = crit.uniqueResult();
		if (object != null) return (ProductComponent)object;
		
		return null;
		
	}
}
