package com.ag.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ag.dao.ProductDao;
import com.ag.model.Product;

@Repository("productDao")
public class ProductDaoHibernate  extends GenericDaoHibernate<Product, Long> implements ProductDao {

	public ProductDaoHibernate() {
		super(Product.class);
	}

	@Override
	public List<Product> findProductByCode(String query) {
		log.debug("query=" + query);
		Criteria crit = getSession().createCriteria(Product.class);
		crit.add(Restrictions.like("code", "%" + query + "%"));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		List<Product> result = crit.list();
		log.debug("result=" + result);
		
		return result;
	}

	@Override
	public Product getByCode(String productCode) {
		Criteria crit = getSession().createCriteria(Product.class);
		crit.add(Restrictions.eq("code", productCode));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		Object object =  crit.uniqueResult();
		if (object != null) return (Product)object;
		
		return null;
	}

}
