package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.ProductionOrderNumberBinDao;
import com.ag.model.ProductionOrderNumberBin;
/**
 * @author hamid
 *
 */
@Repository("productionOrderNumberBinDao")
public class ProductionOrderNumberBinDaoHibernate extends GenericDaoHibernate<ProductionOrderNumberBin, Long> implements ProductionOrderNumberBinDao {

	public ProductionOrderNumberBinDaoHibernate(){
		super(ProductionOrderNumberBin.class);
	}
}
