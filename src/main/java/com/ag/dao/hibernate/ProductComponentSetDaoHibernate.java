package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.ProductComponentSetDao;
import com.ag.model.ProductComponentSet;
/**
 * @author hamid
 *
 */
@Repository("productComponentSetDao")
public class ProductComponentSetDaoHibernate extends GenericDaoHibernate<ProductComponentSet, Long> implements ProductComponentSetDao {

	public ProductComponentSetDaoHibernate(){
		super(ProductComponentSet.class);
	}
}
