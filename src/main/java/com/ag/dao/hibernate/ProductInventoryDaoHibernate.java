package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.ProductInventoryDao;
import com.ag.model.ProductInventory;

@Repository("productInventoryDao")
public class ProductInventoryDaoHibernate extends GenericDaoHibernate<ProductInventory, Long> implements ProductInventoryDao {

	public ProductInventoryDaoHibernate(){
		super(ProductInventory.class);
	}
}
