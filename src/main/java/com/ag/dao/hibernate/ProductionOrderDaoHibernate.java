package com.ag.dao.hibernate;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ag.dao.ProductionOrderDao;
import com.ag.model.ProductionOrder;
import com.ag.model.ProductionOrderItem;
/**
 * @author hamid
 *
 */
@Repository("productionOrderDao")
public class ProductionOrderDaoHibernate extends GenericDaoHibernate<ProductionOrder, Long> implements ProductionOrderDao {

	public ProductionOrderDaoHibernate(){
		super(ProductionOrder.class);
	}

	@Override
	public ProductionOrder getWithOrderItems(Long id) {
		Criteria crit = getSession().createCriteria(ProductionOrder.class);
		crit.add(Restrictions.eq("id", id));
		crit.setFetchMode("productionOrderItems", FetchMode.JOIN);
		//crit.setMaxResults(1);
		
		Object object = crit.uniqueResult();
		if (object != null) return (ProductionOrder)object;
		
		return null;
	}

	@Override
	public ProductionOrder getWithOrderItemsAndAdjustments(Long id) {
		Criteria crit = getSession().createCriteria(ProductionOrder.class);
		crit.add(Restrictions.eq("id", id));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	
		Object object = crit.uniqueResult();
		
		if (object != null){
			ProductionOrder po = (ProductionOrder)object;
			Hibernate.initialize(po.getAdjustments());
			Hibernate.initialize(po.getProductionOrderItems());
			if (po.getProductionOrderItems() != null){
				for (ProductionOrderItem poi:po.getProductionOrderItems()){
					Hibernate.initialize(poi.getHashProductionOrderQuantityBySizes());
				}
			}
			log.debug("productionOrderItem.size=" + (po.getProductionOrderItems() != null ? po.getProductionOrderItems():"[null]"));
			
			return po;
		} else {
			log.debug("object is null!");
		}
		
		return null;
	}
}
