package com.ag.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.ag.dao.DeliveryOrderNumberBinDao;
import com.ag.model.DeliveryOrderNumberBin;

@Repository("deliveryOrderNumberBinDao")
public class DeliveryOrderNumberBinDaoHibernate extends GenericDaoHibernate<DeliveryOrderNumberBin, Long> implements DeliveryOrderNumberBinDao {

	public DeliveryOrderNumberBinDaoHibernate(){
		super(DeliveryOrderNumberBin.class);
	}
}
