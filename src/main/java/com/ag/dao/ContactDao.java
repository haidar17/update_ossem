package com.ag.dao;

import com.ag.model.Contact;

public interface ContactDao extends GenericDao<Contact, Long>{

}
