package com.ag.dao;

import com.ag.model.DeliveryOrderNumberBin;

public interface DeliveryOrderNumberBinDao extends GenericDao<DeliveryOrderNumberBin, Long>{

}
