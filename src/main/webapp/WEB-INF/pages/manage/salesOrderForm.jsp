<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="salesOrderForm.title"/></title>
    <meta name="menu" content="ManageMenu"/>
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>    
</head>
<c:set value="${param.method == 'Add'}" var="isAdd"/>
<c:set var="delObject" scope="request">sales order</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<c:if test="${salesOrderForm.hasSalesOrderItems}">
<div class="hidden" id="salesOrderItemsJSON">
    <json:object name="list">
        <json:array name="salesOrderItems" var="salesOrderItem" items="${salesOrderForm.salesOrderItems}">
            <json:object>
                <json:property name="id" value="${salesOrderItem.id}"/>
                <json:property name="productId" value="${salesOrderItem.product.id}"/>
                <json:property name="productName" value="${salesOrderItem.product.name}"/>
                <json:property name="productCode" value="${salesOrderItem.product.code}"/>
                <json:property name="orderQuantity" value="${salesOrderItem.orderQuantity}"/>
                <json:property name="deliveryQuantity" value="${salesOrderItem.deliveryQuantity}"/>
                <json:property name="price" value="${salesOrderItem.unitPrice}"/>
            </json:object>
        </json:array>
    </json:object>
</div>
</c:if>
<div class="col-sm-2">
    <h2><fmt:message key="salesOrderForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="salesOrderForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="salesOrderForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">
    <spring:bind path="salesOrderForm.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
    <form:form commandName="salesOrderForm" method="post" action="salesOrdersForm" id="salesOrderForm" autocomplete="off"
               cssClass="well">
        <form:hidden path="id"/>
        <form:hidden path="orderNo"/>
        <form:hidden path="customerId"/>
        <form:hidden path="status" id="status"/>
        <form:hidden path="deliveryStatus" id="deliveryStatus"/>
         <div class="form-group">
			<div class="alert alert-dismissable alert-info">
			  <button type="button" class="close" data-dismiss="alert">x</button>
			  <c:if test="${salesOrderForm.newOrder}">
			  <p>This is a new order.</p>
			  </c:if>			  
			  <c:if test="${salesOrderForm.draftOrder}">
			  <p>This is a draft order.</p>
			  </c:if>
			  <c:if test="${salesOrderForm.acceptedOrder}">
			  <p>NOTE: This order has been accepted.</p>
			  </c:if>
			  <c:if test="${salesOrderForm.confirmedOrder}">
			  <p>NOTE: This order has been confirmed.</p>
			  </c:if>			  
			  <c:if test="${salesOrderForm.completedOrder}">
			  <p>NOTE: This order has been completed.</p>
			  </c:if>			  
			  <c:if test="${salesOrderForm.rejectedOrder}">
			  <p>NOTE: This order has been rejected.</p>
			  </c:if>			  			  
			</div>
        </div>        
		<div class="form-group">
	            <appfuse:label styleClass="control-label" key="salesOrderForm.customerFullName"/>
	            <div class="input-group">
	            	<form:input cssClass="form-control" path="customerFullName" id="customerFullName"  maxlength="50" placeholder="Enter Customer Name..." readonly="${!salesOrderForm.newOrder && !salesOrderForm.draftOrder}"/>
					<span class="input-group-btn"><button class="btn btn-default" type="button" id="addCustomer-btn" ${!salesOrderForm.newOrder && !salesOrderForm.draftOrder ? "disabled='disabled'":""}>&emsp;<fmt:message key="button.add.sign"/>&emsp;</button></span>
   				 </div>
	            <form:errors path="customerFullName" cssClass="help-block" cssStyle="color:red"/>            
        </div> 
		<div class="row">
	        <div class="col-sm-6 form-group">
	            <appfuse:label styleClass="control-label" key="salesOrderForm.orderNo"/>
	            <input type="text" class="form-control" value="${salesOrderForm.orderNo}" maxlength="50" disabled="disabled"/>	            
	        </div> 
	        <div class="col-sm-6 form-group">
	            <appfuse:label styleClass="control-label" key="salesOrderForm.orderDate"/>
	            <form:input cssClass="form-control" path="orderDate" id="orderDate" maxlength="50" placeholder="mm/dd/yyyy"/>
	            <form:errors path="orderDate" cssClass="help-block" cssStyle="color:red"/>
	        </div>  
       </div>  
       
	   <ul class="nav nav-tabs">
		  <li class="active"><a href="#orderItems" data-toggle="tab"><appfuse:label styleClass="control-label" key="salesOrderForm.orderItems"/>&nbsp;<span class="badge">${salesOrderForm.salesOrderItemCount}</span></a></li>
		  <li class=""><a href="#productionOrders" data-toggle="tab"><appfuse:label styleClass="control-label" key="salesOrderForm.productionOrders"/>&nbsp;<span class="badge">0</span></a></li>  
	   </ul>
	   <div id="myTabContent" class="tab-content">	   
		  <div class="tab-pane fade active in" id="orderItems"><br/>
		 		<div class="control-group well" style="height:200px; overflow-y:scroll"> 
		            	<div class="row" id="orderProduct-row">
			            	<div class="col-sm-3 form-group">
				            	<appfuse:label styleClass="control-label" key="salesOrderForm.orderProductCode"/>
				            	<div class="input-group">
				            		<input type="text" class="form-control" id="orderProduct-code" name="orderProduct-code" placeholder="e.g.: 10001"/>
									<span class="input-group-btn"><button class="btn btn-default" type="button" id="addProductCode-btn" ${salesOrderForm.addOrderItemAllowed ? "":"disabled='disabled'"}><fmt:message key="button.add.sign"/></button></span>
				   				 </div>
				            </div>
			            	<div class="col-sm-2 form-group">
				            	<appfuse:label styleClass="control-label" key="salesOrderForm.pricePerUnit"/>
				            	<input type="text" class="form-control" style="text-align:right" value="0.00" readonly="readonly" id="orderProduct-unit-price" name="orderProduct-unit-price"/>
				            </div>		            
			            	<div class="col-sm-2 form-group">
				            	<appfuse:label styleClass="control-label" key="salesOrderForm.readyStockQty"/>
				            	<input type="text" class="form-control" style="text-align:right" value="1" readonly="readonly" id="orderProduct-in-stock" name="orderProduct-in-stock"/>
				            </div>
			            	<div class="col-sm-2 form-group">
				            	<appfuse:label styleClass="control-label" key="salesOrderForm.orderQty"/>
				            	<input type="text" class="form-control" style="text-align:right" value="1" id="orderProduct-orderQty" name="orderProduct-orderQty"/>
				            </div>	
			            	<div class="col-sm-2 form-group">
				            	<appfuse:label styleClass="control-label" key="salesOrderForm.productionQty"/>
				            	<input type="text" class="form-control" style="text-align:right" value="0" id="orderProduct-deliveryQty" name="orderProduct-deliveryQty"/>
				            </div>				            
			            	<div class="col-sm-1 form-group">
				            	<appfuse:label styleClass="control-label" key="salesOrderForm.blank"/><br/>
					            <button type="button" class="btn" name="orderProduct-btn" id="orderProduct-btn" onclick="bCancel=false" ${salesOrderForm.addOrderItemAllowed ? "":"disabled='disabled'"}>
					                <i class="icon-add icon-white"></i>&emsp;<fmt:message key="button.add.sign"/>&emsp;
					            </button>
					            <!-- button type="button" class="btn"  class="form-control" name="orderProduct-btn" id="orderProduct-btn" onclick="bCancel=false" ${salesOrderForm.addOrderItemAllowed ? "":"disabled='disabled'"}>
					                <i class="icon-add icon-white"></i><fmt:message key="button.add.sign"/>
					            </button -->
				            </div>
				            <div class="col-sm-2 form-group">
				            	<appfuse:label styleClass="control-label" key="salesOrderForm.blank"/>
				            	<input type="hidden" class="form-control" style="text-align:right" value="" id="orderProduct-qtyBySize" name="orderProduct-qtyBySize"/>
					            <br/>	
					            <button type="button" class="btn" name="orderQtyBySize-edit-btn" id="orderQtyBySize-edit-btn" onclick="bCancel=false" disabled="disabled" data-index="">
					                <i class="icon-add icon-white"></i>Detail
					            </button>		            	
				            </div>            
			            </div>            	            
		        </div>   
		  </div>
		  <div class="tab-pane fade" id="productionOrders"><br/>
		  		<div class="control-group well" style="height:200px; overflow-y:scroll"> 
		  		<h2>Production orders</h2>
		  		</div>
		  </div>
	   </div>                 
         <div class="row">
        	<br/>
        	<br/>
       	</div>             
        <div class="form-group">
        	<c:if test="${!salesOrderForm.rejectedOrder && !salesOrderForm.completedOrder}">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            </c:if>
            <c:if test="${salesOrderForm.draftOrder}">
	           <button type="button" class="btn btn-warning" name="accept" id="acceptOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.accept"/>
	            </button>   
	           <button type="button" class="btn btn-danger" name="reject"  id="rejectOrder-btn"  onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.reject"/>
	            </button>  	             
            </c:if>
             <c:if test="${salesOrderForm.acceptedOrder}">
	           <button type="button" class="btn btn-success" name="confirm" id="confirmOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.confirm"/>
	            </button>   	             
            </c:if> 
              <c:if test="${salesOrderForm.confirmedOrder}">
	           <button type="button" class="btn btn-info" name="complete" id="completeOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.complete"/>
	            </button>   	             
            </c:if>                          
            <c:if test="${!salesOrderForm.rejectedOrder && !salesOrderForm.completedOrder}">      
			<a class="btn btn-default" href="<c:url value='${ctx}/manage/salesOrdersForm?id=${salesOrderForm.id}&from=list&method=Edit'/>">
           			<i class="icon-reset"></i> <fmt:message key="button.reset"/></a>
            </c:if>        
            <c:if test="${param.from == 'list' and param.method != 'Add' and (salesOrderForm.draftOrder)}">
              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
              </button>
            </c:if>
            <button type="submit" class="btn btn-default" value="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>        
     </form:form>
</div>

<div class="modal" id="unitPriceModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="unitPriceModalForm_title">Quick Set Product Price Form</h4>
      </div>
      <div class="modal-body">
		<div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>      
        	<p>Customer's product price for product code '<span id="unitPriceModalForm_productCode_span"></span>' could not be found!</p>
        </div>
        <div class="form-group">
            <label class="control-label">Price</label>
			<input id="unitPriceModalForm_productPrice" type="text" class="form-control" style="text-align:right" value="0.00"/>
 			<input id="unitPriceModalForm_productCode" class="hidden" type="text"/>
 			<input id="unitPriceModalForm_customerId" class="hidden" type="text"/>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="unitPriceModalForm-setPrice-btn" >Set Price</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="addCustomerModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="unitPriceModalForm_title">Quick Create Customer Form</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Name *</label>
			<input id="addCustomerModalForm_customerName" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Business/Company Reg. No.  *</label>
			<input id="addCustomerModalForm_customerLegalId" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Contact First Name  *</label>
			<input id="addCustomerModalForm_contactFirstName" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Contact Last Name  *</label>
			<input id="addCustomerModalForm_contactLastName" type="text" class="form-control addCustomerModalFormInput" />
        </div>                         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addCustomerModalForm-create-btn" >Create Customer</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="addProductPricingModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="addProductPricingModalForm_title">Quick Create Product Pricing Form</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Product Name *</label>
			<input id="addProductPricingModalForm_productName" type="text" class="form-control addProductPricingModalFormInput"/>
        </div> 
        <div class="form-group">
            <label class="control-label">Product Code *</label>
			<input id="addProductPricingModalForm_productCode" type="text" class="form-control addProductPricingModalFormInput"/>
        </div> 
         <div class="form-group">
            <label class="control-label">Price *</label>
			<input id="addProductPricingModalForm_price" type="text" class="form-control addProductPricingModalFormInput"/>
        </div>    
        <input id="addProductPricingModalForm_customerId" class="hidden addProductPricingModalFormInput" type="text"/>            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addProductPricingModalForm-create-btn" >Create Product Pricing</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="addOrderItemBySizeModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="addOrderItemBySizeModalForm_title">Add Order Item Form</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
            <label class="control-label">Product Code</label>
			<input id="addOrderItemBySizeModalForm_productCode" type="text" readonly="readonly" class="form-control addOrderItemBySizeModalFormInput"/>
			<input id="addOrderItemBySizeModalForm_qtyBySize" type="hidden" value="" class="addOrderItemBySizeModalFormInput"/>
			<input id="addOrderItemBySizeModalForm_index" type="hidden" value="" class="addOrderItemBySizeModalFormInput"/>
        </div>          
		   <ul class="nav nav-tabs">
			  <li class="active"><a href="#myPopupTabContent #orderItemBySize" data-toggle="tab"><appfuse:label styleClass="control-label" key="productionOrderForm.orderItemsQtyBySize"/></a></li>
		   </ul>
		   <div id="myPopupTabContent" class="tab-content">	   
			  <div class="tab-pane fade active in" id="orderItemBySize"><br/>
				  <div class="control-group well" style="height:200px; overflow-y:scroll"> 
					  	<div class="row hidden" id="orderItemBySizeRow">
					        <div class="col-sm-2 form-group">
					            <label class="control-label">Item<br/>Size</label>
								<input id="addOrderItemBySizeModalForm_size" name="addOrderItemBySizeModalForm_size" type="text" readonly="readonly" class="form-control addOrderItemBySizeModalFormInput"/>
								<input id="addOrderItemBySizeModalForm_id" name="addOrderItemBySizeModalForm_size" type="hidden" class="addOrderItemBySizeModalFormInput"/>
					        </div> 
					        <div class="col-sm-2 form-group">
					            <label class="control-label">In Stock<br/>Qty</label>
								<input id="addOrderItemBySizeModalForm_inStock"  name="addOrderItemBySizeModalForm_inStock" type="text" readonly="readonly"  class="form-control addOrderItemBySizeModalFormInput"/>
					        </div>
					        <div class="col-sm-2 form-group">
					            <label class="control-label">In Prod.<br/>Qty</label>
								<input id="addOrderItemBySizeModalForm_inProduction"  name="addOrderItemBySizeModalForm_inProduction" type="text" readonly="readonly"  class="form-control addOrderItemBySizeModalFormInput"/>
					        </div>	
					        <div class="col-sm-2 form-group">
					            <label class="control-label">Order<br/>Qty</label>
								<input id="addOrderItemBySizeModalForm_qty" name="addOrderItemBySizeModalForm_qty" value="0" type="text" class="form-control addOrderItemBySizeModalFormInput"/>
					        </div> 	
					        <div class="col-sm-2 form-group">
					            &emsp;
					        </div> 				        		        		        			        			        
					  	 </div>
					  	 <div class="row">
					  	 	<br/><br/>
					  	 </div>
				  	</div>
			  </div>
		   </div> 		        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addOrderItemBySizeModalForm-create-btn">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<c:set var="scripts" scope="request">
<%@ include file="/scripts/salesOrderForm.js"%>
</c:set>       