<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="salesInvoices.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="salesInvoices.heading"/></h2>
</div>