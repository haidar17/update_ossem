<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="productPricingForm.title"/></title>
    <meta name="menu" content="ManageMenu"/>   
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>    
</head>
<c:set var="hasId" value="${productPricingForm.id != null}"/>
<c:set var="hasCustomerId" value="${productPricingForm.customerId != null}"/>
<c:set var="customerProductPricings_size" value="${fn:length(customerProductPricings)}" scope="request"/>
<c:set value="${param.method == 'Add'}" var="isAdd"/>
<c:set var="delObject" scope="request">product pricing</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<div class="col-sm-2">
    <h2><fmt:message key="productPricingForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="productPricingForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="productPricingForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">    
		<div class="panel panel-primary">
  			<div class="panel-heading">
    			<h3 class="panel-title" style="color:white"><c:out value="${productPricingForm.customerId != null ? productPricingForm.customerName.concat('-'):''}"/> Product Pricings</span></h3>
  			</div>
  			<div class="panel-body">
			  <div id="actions" class="btn-group">
     				<a class="btn btn-primary" href="<c:url value='${ctx}/manage/productPricingsForm?customerId=${productPricingForm.customerId}&method=New'/>">
           			<i class="icon-plus icon-white"></i> <fmt:message key="button.new"/></a>				        
       				<a class="btn btn-default" href="<c:url value='${ctx}/manage/productPricings'/>">
           			<i class="icon-plus"></i> <fmt:message key="button.back"/></a>
			    </div>     			 	
 		        <div class="form-group">
		        	<c:if test="${customerProductPricings_size == 0}">
		        		No record found.
		        	</c:if>
		        	<c:if test="${customerProductPricings_size > 0}">
		        		<c:out value="${customerProductPricings_size} records found, displaying all records."/>
		        	</c:if>
		        	<br/>
		       	</div> 				  
   				<div class="control-group span12" style="height:400px; overflow-y:scroll"> 
				   	 <display:table name="customerProductPricings" cellspacing="0" cellpadding="0" requestURI=""
				                   defaultsort="1" id="customerProductPricing" pagesize="0" class="table table-condensed table-striped table-hover">                      
				        <display:column escapeXml="false" sortable="true" titleKey="productPricing.productCode" style="width: 60%">
				        	<a href="${ctx}/manage/productPricingsForm?id=${customerProductPricing.id}&from=list&method=Edit"><c:out value="${ customerProductPricing.product.code} - ${ customerProductPricing.product.name}"/></a>
				        </display:column>  
				        <display:column escapeXml="false" sortable="true" titleKey="productPricing.price" style="width: 20%; text-align:right">
				        		<fmt:formatNumber value="${customerProductPricing.price}" pattern="###,###.00"/>
				        </display:column>   
				        <display:column escapeXml="true" sortable="true" titleKey="column.blank" style="width: 40%; text-align:right"/> 						        
				        <display:setProperty name="paging.banner.item_name"><fmt:message key="productPricings.entry"/></display:setProperty>
				        <display:setProperty name="paging.banner.items_name"><fmt:message key="productPricings.entries"/></display:setProperty>
				    </display:table>
  				</div> 		
  				<div class="row">
  					<br/><br/>
  				</div>  					  				
			</div>  
     	</div>          
	</div>
	<div id="confirm_modal_form" class="modal">
        <div class="modal-dialog modal-md"  >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="confirm_modal_form_title">Product Pricing Form (${param.method})</h4>
            </div>
            <div class="modal-body">
			    <spring:bind path="productPricingForm.*">
			        <c:if test="${not empty status.errorMessages}">
			            <div class="alert alert-danger alert-dismissable">
			                <a href="#" data-dismiss="alert" class="close">&times;</a>
			                <c:forEach var="error" items="${status.errorMessages}">
			                    <c:out value="${error}" escapeXml="false"/><br/>
			                </c:forEach>
			            </div>
			        </c:if>
			    </spring:bind>            
			    <form:form commandName="productPricingForm" method="post" action="productPricingsForm" id="productPricingForm" autocomplete="off"
			               cssClass="well">
			        <form:hidden path="id"/>
			        <form:hidden path="customerId"/>
			        <form:hidden path="productId"/>  
			        <div class="form-group">
			            <appfuse:label styleClass="control-label" key="productPricingForm.customerName"/>
			            <div class="input-group">
			            	<form:input cssClass="form-control" path="customerName" id="customerName" maxlength="50" readonly="${hasCustomerId}" placeholder="Enter Customer Name..."/>
							<span class="input-group-btn"><button class="btn btn-default" type="button" id="addCustomer-btn" ${hasCustomerId ? "disabled='disabled'":""}>&emsp;<fmt:message key="button.add.sign"/>&emsp;</button></span>
   				 		</div>	            
			            <form:errors path="customerName" cssClass="help-block" cssStyle="color:red"/>
			        </div>  
			        <div class="form-group">
			            <appfuse:label styleClass="control-label" key="productPricingForm.productCode"/>
			             <div class="input-group">
			            	<form:input cssClass="form-control" path="productCode" id="productCode" maxlength="50" placeholder="e.g.: 10001"/>
							<span class="input-group-btn"><button class="btn btn-default" type="button" id="addProductCode-btn">&emsp;<fmt:message key="button.add.sign"/>&emsp;</button></span>
		   				 </div>				            
			            <form:errors path="productCode" cssClass="help-block" cssStyle="color:red"/>
			        </div> 
			       <div class="form-group">
			            <appfuse:label styleClass="control-label" key="productPricingForm.productName"/>
			            <form:input cssClass="form-control" path="productName" id="productName" readonly="true"/>
			            <form:errors path="productName" cssClass="help-block" cssStyle="color:red"/>
			        </div>               
			        <div class="form-group">
			            <appfuse:label styleClass="control-label" key="productPricingForm.price"/>
			            <form:input cssClass="form-control" path="price" id="price" maxlength="50" placeholder="e.g.: 1.00"/>
			            <form:errors path="price" cssClass="help-block" cssStyle="color:red"/>
			        </div>                        
			         <div class="row">
			        	<br/>
			        	<br/>
			       	</div>             
			        <div class="form-group">
			            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
			                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
			            </button>
			            <button type="reset" class="btn" name="reset" onclick="bCancel=false">
			                <i class="icon-reset"></i> <fmt:message key="button.reset"/>
			            </button>
			            <c:if test="${hasId}">
			              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
			                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
			              </button>
			            </c:if>
			            <button type="submit" class="btn btn-default" value="form-cancel" name="form-cancel" onclick="bCancel=true">
			                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
			            </button>
			        </div>        
			     </form:form>             	
            </div>         
          </div>
        </div>
      </div> 

<div class="modal" id="addProductModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="addProductModalForm_title">Quick Create Product Form</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Product Name *</label>
			<input id="addProductModalForm_productName" type="text" class="form-control addProductModalFormInput"/>
        </div> 
        <div class="form-group">
            <label class="control-label">Product Code *</label>
			<input id="addProductModalForm_productCode" type="text" class="form-control addProductModalFormInput"/>
        </div>         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addProductModalForm-create-btn" >Create Product</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
      
<div class="modal" id="addCustomerModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="unitPriceModalForm_title">Quick Create Customer Form</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Name *</label>
			<input id="addCustomerModalForm_customerName" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Business/Company Reg. No.  *</label>
			<input id="addCustomerModalForm_customerLegalId" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Contact First Name  *</label>
			<input id="addCustomerModalForm_contactFirstName" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Contact Last Name  *</label>
			<input id="addCustomerModalForm_contactLastName" type="text" class="form-control addCustomerModalFormInput" />
        </div>                         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addCustomerModalForm-create-btn" >Create Customer</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>      
<c:set var="scripts" scope="request">
<%@ include file="/scripts/productPricingForm.js"%>
</c:set>       