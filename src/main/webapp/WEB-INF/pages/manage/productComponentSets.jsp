<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="productComponentSets.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="productComponentSets.heading"/></h2>
     <form method="get" action="${ctx}/manage/productComponentSets" id="searchForm" class="form-inline">
    <div id="search" class="text-right">
        <span class="col-sm-9">
            <input type="text" size="20" name="q" id="query" value="${param.q}"
                   placeholder="<fmt:message key="search.enterTerms"/>" class="form-control input-sm">
        </span>
        <button id="button.search" class="btn btn-default btn-sm" type="submit">
            <i class="icon-search"></i> <fmt:message key="button.search"/>
        </button>
    </div>
    </form>
    
    <div id="actions" class="btn-group">
        <a class="btn btn-primary" href="<c:url value='${ctx}/manage/productComponentSetsForm'/>">
            <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/></a>

        <a class="btn btn-default" href="<c:url value='/home'/>">
            <i class="icon-ok"></i> <fmt:message key="button.done"/></a>
    </div>   
   <display:table name="productComponentSets" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="productComponentSet" pagesize="25" class="table table-condensed table-striped table-hover" export="true">                      
         <display:column escapeXml="false" sortable="true" titleKey="productComponentSet.name"
                        style="width: 30%">
             <a href="${ctx}/manage/productComponentSetsForm?id=${productComponentSet.id}&from=list&method=Edit"><c:out value="${productComponentSet.name}"/></a>
        </display:column>
        <display:column property="description" escapeXml="true" sortable="true" titleKey="productComponentSet.description"
                        style="width: 70%"/>                              
        <display:setProperty name="paging.banner.item_name"><fmt:message key="productComponentSets.entry"/></display:setProperty>
        <display:setProperty name="paging.banner.items_name"><fmt:message key="productComponentSets.entries"/></display:setProperty>

        <display:setProperty name="export.excel.filename" value="Product Component Set List.xls"/>
        <display:setProperty name="export.csv.filename" value="Product Component Set List.csv"/>
        <display:setProperty name="export.pdf.filename" value="Product Component Set List.pdf"/>
    </display:table>     
</div>    