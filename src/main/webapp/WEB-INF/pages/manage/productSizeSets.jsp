<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="productSizeSets.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="productSizeSets.heading"/></h2>
     <form method="get" action="${ctx}/manage/productSizeSets" id="searchForm" class="form-inline">
    <div id="search" class="text-right">
        <span class="col-sm-9">
            <input type="text" size="20" name="q" id="query" value="${param.q}"
                   placeholder="<fmt:message key="search.enterTerms"/>" class="form-control input-sm">
        </span>
        <button id="button.search" class="btn btn-default btn-sm" type="submit">
            <i class="icon-search"></i> <fmt:message key="button.search"/>
        </button>
    </div>
    </form>
    
    <div id="actions" class="btn-group">
        <a class="btn btn-primary" href="<c:url value='${ctx}/manage/productSizeSetsForm'/>">
            <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/></a>

        <a class="btn btn-default" href="<c:url value='/home'/>">
            <i class="icon-ok"></i> <fmt:message key="button.done"/></a>
    </div>   
   <display:table name="productSizeSets" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="productSizeSet" pagesize="25" class="table table-condensed table-striped table-hover" export="true">                      
         <display:column escapeXml="false" sortable="true" titleKey="productSizeSet.name"
                        style="width: 30%">
             <a href="${ctx}/manage/productSizeSetsForm?id=${productSizeSet.id}&from=list&method=Edit"><c:out value="${productSizeSet.name}"/></a>
        </display:column>
        <display:column property="description" escapeXml="true" sortable="true" titleKey="productSizeSet.description"
                        style="width: 70%"/>                              
        <display:setProperty name="paging.banner.item_name"><fmt:message key="productSizeSets.entry"/></display:setProperty>
        <display:setProperty name="paging.banner.items_name"><fmt:message key="productSizeSets.entries"/></display:setProperty>

        <display:setProperty name="export.excel.filename" value="Product Size Set List.xls"/>
        <display:setProperty name="export.csv.filename" value="Product Size Set List.csv"/>
        <display:setProperty name="export.pdf.filename" value="Product Size Set List.pdf"/>
    </display:table>     
</div>    