<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="salesReceipts.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="salesReceipts.heading"/></h2>
</div>