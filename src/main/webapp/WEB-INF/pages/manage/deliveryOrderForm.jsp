<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="deliveryOrderForm.title"/></title>
    <meta name="menu" content="ManageMenu"/>
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>     
</head>
<c:set value="${param.method == 'Add'}" var="isAdd"/>
<c:set var="delObject" scope="request">delivery order</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<div class="col-sm-2">
    <h2><fmt:message key="deliveryOrderForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="deliveryOrderForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="deliveryOrderForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">
    <spring:bind path="deliveryOrderForm.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
    <form:form commandName="deliveryOrderForm" method="post" action="deliveryOrdersForm" id="deliveryOrderForm" autocomplete="off"
               cssClass="well">
        <form:hidden path="id"/> 
        <form:hidden path="customerId"/> 
        <form:hidden path="billingAddressId"/>
        <form:hidden path="deliveryAddressId"/>
        <form:hidden path="status" id="status"/> 
        <div class="form-group">
			<div class="alert alert-dismissable alert-info">
			  <button type="button" class="close" data-dismiss="alert">�</button>
			  <c:if test="${deliveryOrderForm.newOrder}">
			  <p>This is a new order.</p>
			  </c:if>			  
			  <c:if test="${deliveryOrderForm.draftOrder}">
			  <p>This is a draft order.</p>
			  </c:if>
			  <c:if test="${deliveryOrderForm.acceptedOrder}">
			  <p>NOTE: This order has been accepted.</p>
			  </c:if>
			  <c:if test="${deliveryOrderForm.confirmedOrder}">
			  <p>NOTE: This order has been confirmed.</p>
			  </c:if>			  
			  <c:if test="${deliveryOrderForm.completedOrder}">
			  <p>NOTE: This order has been completed.</p>
			  </c:if>			  
			  <c:if test="${deliveryOrderForm.rejectedOrder}">
			  <p>NOTE: This order has been rejected.</p>
			  </c:if>			  			  
			</div>
        </div>               
 		<div class="row">
	        <div class="col-sm-6 form-group">
	            <appfuse:label styleClass="control-label" key="deliveryOrderForm.orderNo"/>
	            <form:input cssClass="form-control" path="orderNo" id="orderNo" readonly="true"/>
	            <form:errors path="orderNo" cssClass="help-block" cssStyle="color:red"/>
        	</div>  
        	<div class="col-sm-6 form-group">
	            <appfuse:label styleClass="control-label" key="deliveryOrderForm.orderDate"/>
	            <form:input cssClass="form-control" path="orderDate" id="orderDate" maxlength="50" placeholder="mm/dd/yyyy"/>
	            <form:errors path="orderDate" cssClass="help-block" cssStyle="color:red"/>
        	</div>    
        </div>    
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="deliveryOrderForm.reference"/>
            <form:input cssClass="form-control" path="reference" id="reference" maxlength="50" placeholder="e.g.: REF/000001"/>
            <form:errors path="reference" cssClass="help-block" cssStyle="color:red"/>
        </div> 
 
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="deliveryOrderForm.customerFullName"/>
            <div class="input-group">
            	<form:input cssClass="form-control" path="customerFullName" id="customerFullName" maxlength="50" placeholder="Enter Customer Name..." disabled="${!deliveryOrderForm.changeCustomerAllowed}"/>
				<span class="input-group-btn"><button class="btn btn-default" type="button" id="addCustomer-btn" ${!deliveryOrderForm.changeCustomerAllowed ? "disabled='disabled'" :""}>&emsp;<fmt:message key="button.add.sign"/>&emsp;</button></span>
   			</div>
            <form:errors path="customerFullName" cssClass="help-block" cssStyle="color:red"/>
        </div>  
	   <ul class="nav nav-tabs">
		  <li ${deliveryOrderForm.newOrder ? "class='disabled'":"class='active'" }><a ${!deliveryOrderForm.newOrder ? "href='#salesOrder' data-toggle='tab'":"" }><appfuse:label styleClass="control-label" key="deliveryOrderForm.salesOrder"/></a></li>
		  <li class=""><a href="#billingAddress" data-toggle="tab"><appfuse:label styleClass="control-label" key="deliveryOrderForm.billingAddress"/></a></li>  
		  <li class=""><a href="#deliveryAddress" data-toggle="tab"><appfuse:label styleClass="control-label" key="deliveryOrderForm.deliveryAddress"/></a></li>  
	   </ul>
	   <div id="myTabContent" class="tab-content">	   
		  <div ${!deliveryOrderForm.newOrder ? "class='tab-pane fade active in'":"class='hidden'" } id="salesOrder"><br/>		   
		 		<div id="confirmedSalesOrdersPane" class="control-group well" style="height:200px; overflow-y:scroll"> 
				   	 <display:table name="confirmedSalesOrders" cellspacing="0" cellpadding="0" requestURI=""
				                   defaultsort="1" id="confirmedSalesOrder" pagesize="0" class="table table-condensed table-striped table-hover">
		                <display:column escapeXml="false" sortable="false" title="&nbsp;" style="width: 5px">
		                	<input type="checkbox" title="Check to include" class="selectedRow" id="selectedConfirmedSalesOrderId" name="selectedConfirmedSalesOrderId" value="${confirmedSalesOrder.id}" ${confirmedSalesOrder.selectedSalesOrder ? 'checked=checked':''}" ${!deliveryOrderForm.changeConfirmedSalesOrderAllowed ? 'disabled=disabled':''}/>
		                </display:column>                      
			        	<display:column escapeXml="false" sortable="true" title="Order Detail" style="width: 20%">
			        		<span title="Order No."><c:out value="${confirmedSalesOrder.orderNo }"/></span>
			      		 	<span title="Order Date"><fmt:formatDate type="date" value="${confirmedSalesOrder.orderDate}" pattern="MMM d, yyyy"/></span>
			        		<span title="Order Status" class="label ${confirmedSalesOrder.statusCss}">${confirmedSalesOrder.statusName}</span>				      		 	                  
			        	</display:column>  
						<c:set var="salesOrderItems" value="${confirmedSalesOrder.salesOrderItems}" />								
						<display:column title="Order Items" style="width: 80%" sortable="true">								
						  	<display:table name="${salesOrderItems}" id="salesOrderItem" pagesize="0">
								<display:column escapeXml="false" title="Product Code" style="width: 20%">
									<div title="${salesOrderItem.product.name }">${salesOrderItem.product.code }</div>
								</display:column>
								<display:column property="orderQuantity" title="Order Qty" style="text-align:center;width: 20%"/>
								<display:column titleKey="product.unitPrice" style="text-align:right;width: 20%">
									<fmt:formatNumber value="${salesOrderItem.unitPrice}" pattern="$###,###.00"/>
								</display:column>
								<display:column title="InStock Qty" style="text-align:center;width: 10%">
									<c:out value="${salesOrderItem.orderQuantity}"/>
								</display:column>										
								<display:column title="Delivery Qty" style="text-align:center;width: 10%">
									<input type="text" size="2" maxlength="3" value="${salesOrderItem.orderQuantity}" class="form-control input-sm"/>
								</display:column>										
								<display:column escapeXml="false" titleKey="product.total" style="text-align:right;width: 20%">
									<fmt:formatNumber value="${ salesOrderItem.unitPrice * salesOrderItem.orderQuantity}" pattern="$###,###.00"/>
								</display:column>
							</display:table>
						</display:column>													         		
				    </display:table>
				</div> 					
		  </div>
		  <div class="tab-pane fade" id="billingAddress"><br/>
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="deliveryOrderForm.address"/>
		            <form:input cssClass="form-control" path="billingAddress" id="billingAddress" maxlength="50"/>
		            <form:errors path="billingAddress" cssClass="help-block" cssStyle="color:red"/>
		        </div> 	
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="deliveryOrderForm.street"/>
		            <form:input cssClass="form-control" path="billingStreet" id="billingStreet" maxlength="50"/>
		            <form:errors path="billingStreet" cssClass="help-block" cssStyle="color:red"/>
		        </div> 				        
				<div class="row">
			        <div class="col-sm-6 form-group">
			            <appfuse:label styleClass="control-label" key="deliveryOrderForm.city"/>
			            <form:input cssClass="form-control" path="billingCity" id="billingCity" maxlength="50"/>
			            <form:errors path="billingCity" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>
			        <div class="col-sm-6 form-group">
		            <appfuse:label styleClass="control-label" key="deliveryOrderForm.postalCode"/>
			            <form:input cssClass="form-control" path="billingPostalCode" id="billingPostalCode" maxlength="50"/>
			            <form:errors path="billingPostalCode" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>					        
			    </div>	
				<div class="row">
			        <div class="col-sm-6 form-group">
			            <appfuse:label styleClass="control-label" key="deliveryOrderForm.stateId"/>
			            <form:input cssClass="form-control" path="billingStateId" id="billingStateId" maxlength="50"/>
			            <form:errors path="billingStateId" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>
			        <div class="col-sm-6 form-group">
		            <appfuse:label styleClass="control-label" key="deliveryOrderForm.countryId"/>
			            <form:input cssClass="form-control" path="billingCountryId" id="billingCountryId" maxlength="50"/>
			            <form:errors path="billingCountryId" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>					        
			    </div>	  
		  </div>
		  <div class="tab-pane fade" id="deliveryAddress"><br/>
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="deliveryOrderForm.address"/>
		            <form:input cssClass="form-control" path="deliveryAddress" id="deliveryAddress" maxlength="50"/>
		            <form:errors path="deliveryAddress" cssClass="help-block" cssStyle="color:red"/>
		        </div> 	
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="deliveryOrderForm.street"/>
		            <form:input cssClass="form-control" path="deliveryStreet" id="deliveryStreet" maxlength="50"/>
		            <form:errors path="deliveryStreet" cssClass="help-block" cssStyle="color:red"/>
		        </div> 					        
				<div class="row">
			        <div class="col-sm-6 form-group">
			            <appfuse:label styleClass="control-label" key="deliveryOrderForm.city"/>
			            <form:input cssClass="form-control" path="deliveryCity" id="deliveryCity" maxlength="50"/>
			            <form:errors path="deliveryCity" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>
			        <div class="col-sm-6 form-group">
		            <appfuse:label styleClass="control-label" key="deliveryOrderForm.postalCode"/>
			            <form:input cssClass="form-control" path="deliveryPostalCode" id="deliveryPostalCode" maxlength="50"/>
			            <form:errors path="deliveryPostalCode" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>					        
			    </div>	
				<div class="row">
			        <div class="col-sm-6 form-group">
			            <appfuse:label styleClass="control-label" key="deliveryOrderForm.stateId"/>
			            <form:input cssClass="form-control" path="deliveryStateId" id="deliveryStateId" maxlength="50"/>
			            <form:errors path="deliveryStateId" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>
			        <div class="col-sm-6 form-group">
		            <appfuse:label styleClass="control-label" key="deliveryOrderForm.countryId"/>
			            <form:input cssClass="form-control" path="deliveryCountryId" id="deliveryCountryId" maxlength="50"/>
			            <form:errors path="deliveryCountryId" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>					        
			    </div>		  
		</div>		  
		</div>        		           				     
         <div class="row">
        	<br/>
        	<br/>
       	</div>             
        <div class="form-group">
        	<c:if test="${!deliveryOrderForm.rejectedOrder && !deliveryOrderForm.completedOrder}">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            </c:if>
            <c:if test="${deliveryOrderForm.draftOrder}">
	           <button type="button" class="btn btn-warning" name="accept" id="acceptOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.accept"/>
	            </button>   
	           <button type="button" class="btn btn-danger" name="reject"  id="rejectOrder-btn"  onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.reject"/>
	            </button>  	             
            </c:if>
             <c:if test="${deliveryOrderForm.acceptedOrder}">
	           <button type="button" class="btn btn-success" name="confirm" id="confirmOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.confirm"/>
	            </button>   	             
            </c:if> 
              <c:if test="${deliveryOrderForm.confirmedOrder}">
	           <button type="button" class="btn btn-info" name="complete" id="completeOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.complete"/>
	            </button>   	             
            </c:if>                          
            <c:if test="${!deliveryOrderForm.rejectedOrder && !deliveryOrderForm.completedOrder}">      
			<a class="btn btn-default" href="<c:url value='${ctx}/manage/deliveryOrdersForm?id=${deliveryOrderForm.id}&from=list&method=Edit'/>">
           			<i class="icon-reset"></i> <fmt:message key="button.reset"/></a>
            </c:if>        
            <c:if test="${param.from == 'list' and param.method != 'Add' and (deliveryOrderForm.draftOrder)}">
              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
              </button>
            </c:if>
            <button type="submit" class="btn btn-default" value="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>        
     </form:form>
</div>

<div class="modal" id="addCustomerModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="unitPriceModalForm_title">Quick Create Customer Form</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Name *</label>
			<input id="addCustomerModalForm_customerName" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Business/Company Reg. No.  *</label>
			<input id="addCustomerModalForm_customerLegalId" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Contact First Name  *</label>
			<input id="addCustomerModalForm_contactFirstName" type="text" class="form-control addCustomerModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Contact Last Name  *</label>
			<input id="addCustomerModalForm_contactLastName" type="text" class="form-control addCustomerModalFormInput" />
        </div>                         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addCustomerModalForm-create-btn" >Create Customer</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<c:set var="scripts" scope="request">
<%@ include file="/scripts/deliveryOrderForm.js"%>
</c:set>       