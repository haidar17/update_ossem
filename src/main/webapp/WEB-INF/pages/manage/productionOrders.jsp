<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="productionOrders.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="productionOrders.heading"/></h2>
     <form method="get" action="${ctx}/manage/productionOrders" id="searchForm" class="form-inline">
    <div id="search" class="text-right">
        <span class="col-sm-9">
            <input type="text" size="20" name="q" id="query" value="${param.q}"
                   placeholder="<fmt:message key="search.enterTerms"/>" class="form-control input-sm">
        </span>
        <button id="button.search" class="btn btn-default btn-sm" type="submit">
            <i class="icon-search"></i> <fmt:message key="button.search"/>
        </button>
    </div>
    </form>
    
    <div id="actions" class="btn-group">
        <a class="btn btn-primary" href="<c:url value='${ctx}/manage/productionOrdersForm'/>">
            <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/></a>

        <a class="btn btn-default" href="<c:url value='/home'/>">
            <i class="icon-ok"></i> <fmt:message key="button.done"/></a>
    </div>   
   <display:table name="productionOrders" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="productionOrder" pagesize="25" class="table table-condensed table-striped table-hover" export="true">                      
         <display:column escapeXml="false" sortable="true" titleKey="productionOrder.orderNo"
                        style="width: 20%">
             <a href="${ctx}/manage/productionOrdersForm?id=${productionOrder.id}&from=list&method=Edit"><c:out value="${productionOrder.orderNo}"/></a>
        </display:column>
        <display:column escapeXml="false" sortable="true" titleKey="productionOrder.orderDate"
                        style="width: 20%">
      		 <fmt:formatDate type="date" value="${productionOrder.orderDate}" pattern="MMM d, yyyy"/>                  
        </display:column>           
        <display:column property="reference" escapeXml="true" sortable="true" titleKey="productionOrder.reference"
                        style="width: 40%"/>        
        <display:column escapeXml="false" sortable="true" titleKey="productionOrder.status"
                        style="width: 20%">
              <span class="label ${productionOrder.statusCss}">${productionOrder.statusName}</span>
        </display:column>                                                
        <display:setProperty name="paging.banner.item_name"><fmt:message key="productionOrders.entry"/></display:setProperty>
        <display:setProperty name="paging.banner.items_name"><fmt:message key="productionOrders.entries"/></display:setProperty>

        <display:setProperty name="export.excel.filename" value="Production Order List.xls"/>
        <display:setProperty name="export.csv.filename" value="Production Order List.csv"/>
        <display:setProperty name="export.pdf.filename" value="Production Order List.pdf"/>
    </display:table>     
</div>    