<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="deliveryOrders.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="deliveryOrders.heading"/></h2>
     <form method="get" action="${ctx}/manage/deliveryOrders" id="searchForm" class="form-inline">
    <div id="search" class="text-right">
        <span class="col-sm-9">
            <input type="text" size="20" name="q" id="query" value="${param.q}"
                   placeholder="<fmt:message key="search.enterTerms"/>" class="form-control input-sm">
        </span>
        <button id="button.search" class="btn btn-default btn-sm" type="button">
            <i class="icon-search"></i> <fmt:message key="button.search"/>
        </button>
    </div>
    </form>
    
    <div id="actions" class="btn-group">
        <a class="btn btn-primary" href="<c:url value='${ctx}/manage/deliveryOrdersForm'/>">
            <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/></a>

        <a class="btn btn-default" href="<c:url value='/home'/>">
            <i class="icon-ok"></i> <fmt:message key="button.done"/></a>
    </div>     
   <display:table name="deliveryOrders" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="deliveryOrder" pagesize="25" class="table table-condensed table-striped table-hover" export="true">                      
         <display:column escapeXml="false" sortable="true" titleKey="deliveryOrder.orderNo"
                        style="width: 15%">
             <a href="${ctx}/manage/deliveryOrdersForm?id=${deliveryOrder.id}&from=list&method=Edit"><c:out value="${deliveryOrder.orderNo}"/></a>
        </display:column>
        <display:column escapeXml="false" sortable="true" titleKey="deliveryOrder.orderDate"
                        style="width: 10%">
      		 <fmt:formatDate type="date" value="${deliveryOrder.orderDate}" pattern="MMM d, yyyy"/>                  
        </display:column>   
        <display:column property="customer.fullName" escapeXml="true" sortable="true" titleKey="deliveryOrder.customerFullName"
                        style="width: 30%"/>                 
        <display:column property="reference" escapeXml="true" sortable="true" titleKey="deliveryOrder.reference"
                        style="width: 25%"/> 
        <display:column escapeXml="false" sortable="true" titleKey="deliveryOrder.status"
                        style="width: 10%">
              <span class="label ${deliveryOrder.statusCss}">${deliveryOrder.statusName}</span>
        </display:column>                         
        <display:setProperty name="paging.banner.item_name"><fmt:message key="deliveryOrders.entry"/></display:setProperty>
        <display:setProperty name="paging.banner.items_name"><fmt:message key="deliveryOrders.entries"/></display:setProperty>

        <display:setProperty name="export.excel.filename" value="Delivery Order List.xls"/>
        <display:setProperty name="export.csv.filename" value="Delivery Order List.csv"/>
        <display:setProperty name="export.pdf.filename" value="Delivery Order List.pdf"/>
    </display:table>     
</div>    