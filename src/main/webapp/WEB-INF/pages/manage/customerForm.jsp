<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="customerForm.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>
<c:set value="${param.method == 'Add'}" var="isAdd"/>
<c:set var="delObject" scope="request">customer</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<div class="col-sm-2">
    <h2><fmt:message key="customerForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="customerForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="customerForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">
    <spring:bind path="customerForm.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
    <form:form commandName="customerForm" method="post" action="customersForm" id="customerForm" autocomplete="off"
               cssClass="well">
        <form:hidden path="id"/>
        <form:hidden path="contactId"/>
        <form:hidden path="billingAddressId"/>
        <form:hidden path="deliveryAddressId"/> 
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="customerForm.fullName"/>
            <form:input cssClass="form-control" path="fullName" id="fullName" maxlength="50"/>
            <form:errors path="fullName" cssClass="help-block" cssStyle="color:red"/>
        </div>  
         <div class="form-group">
            <appfuse:label styleClass="control-label" key="customerForm.legalIdentity"/>
            <form:input cssClass="form-control" path="legalIdentity" id="legalIdentity" maxlength="50"/>
            <form:errors path="legalIdentity" cssClass="help-block" cssStyle="color:red"/>
        </div> 
	   <ul class="nav nav-tabs">
		  <li class="active"><a href="#contactPerson" data-toggle="tab"><appfuse:label styleClass="control-label" key="customerForm.contactPerson"/></a></li>
		  <li class=""><a href="#billingAddress" data-toggle="tab"><appfuse:label styleClass="control-label" key="customerForm.billingAddress"/></a></li>  
		  <li class=""><a href="#deliveryAddress" data-toggle="tab"><appfuse:label styleClass="control-label" key="customerForm.deliveryAddress"/></a></li>
	   </ul>
	   <div id="myTabContent" class="tab-content">	   
		  <div class="tab-pane fade active in" id="contactPerson"><br/>        
					<div id="contactPerson">          
						<div class="row">
					        <div class="col-sm-6 form-group">
					            <appfuse:label styleClass="control-label" key="customerForm.contactFirstName"/>
					            <form:input cssClass="form-control" path="contactFirstName" id="contactFirstName" maxlength="50"/>
					            <form:errors path="contactFirstName" cssClass="help-block" cssStyle="color:red"/>
					        </div> 
					        <div class="col-sm-6 form-group">
					            <appfuse:label styleClass="control-label" key="customerForm.contactLastName"/>
					            <form:input cssClass="form-control" path="contactLastName" id="contactLastName" maxlength="50"/>
					            <form:errors path="contactLastName" cssClass="help-block" cssStyle="color:red"/>
					        </div>  
				        </div>  
						<div class="row">
					        <div class="col-sm-6 form-group">
					            <appfuse:label styleClass="control-label" key="customerForm.contactEmail"/>
					            <form:input cssClass="form-control" path="contactEmail" id="contactEmail" maxlength="50"/>
					            <form:errors path="contactEmail" cssClass="help-block" cssStyle="color:red"/>
					        </div> 
					        <div class="col-sm-6 form-group">
					            <appfuse:label styleClass="control-label" key="customerForm.contactMobilePhone"/>
					            <form:input cssClass="form-control" path="contactMobilePhone" id="contactMobilePhone" maxlength="50"/>
					            <form:errors path="contactMobilePhone" cssClass="help-block" cssStyle="color:red"/>
					        </div>  
				       </div> 		            
				</div>		 
			</div>
			<div class="tab-pane fade" id="billingAddress"><br/>
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="customerForm.address"/>
		            <form:input cssClass="form-control" path="billingAddress" id="billingAddress" maxlength="50"/>
		            <form:errors path="billingAddress" cssClass="help-block" cssStyle="color:red"/>
		        </div> 	
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="customerForm.street"/>
		            <form:input cssClass="form-control" path="billingStreet" id="billingStreet" maxlength="50"/>
		            <form:errors path="billingStreet" cssClass="help-block" cssStyle="color:red"/>
		        </div> 				        
				<div class="row">
			        <div class="col-sm-6 form-group">
			            <appfuse:label styleClass="control-label" key="customerForm.city"/>
			            <form:input cssClass="form-control" path="billingCity" id="billingCity" maxlength="50"/>
			            <form:errors path="billingCity" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>
			        <div class="col-sm-6 form-group">
		            <appfuse:label styleClass="control-label" key="customerForm.postalCode"/>
			            <form:input cssClass="form-control" path="billingPostalCode" id="billingPostalCode" maxlength="50"/>
			            <form:errors path="billingPostalCode" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>					        
			    </div>	
				<div class="row">
			        <div class="col-sm-6 form-group">
			            <appfuse:label styleClass="control-label" key="customerForm.stateId"/>
			            <form:input cssClass="form-control" path="billingStateId" id="billingStateId" maxlength="50"/>
			            <form:errors path="billingStateId" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>
			        <div class="col-sm-6 form-group">
		            <appfuse:label styleClass="control-label" key="customerForm.countryId"/>
			            <form:input cssClass="form-control" path="billingCountryId" id="billingCountryId" maxlength="50"/>
			            <form:errors path="billingCountryId" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>					        
			    </div>				
			</div>
			<div class="tab-pane fade" id="deliveryAddress"><br/>
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="customerForm.address"/>
		            <form:input cssClass="form-control" path="deliveryAddress" id="deliveryAddress" maxlength="50"/>
		            <form:errors path="deliveryAddress" cssClass="help-block" cssStyle="color:red"/>
		        </div> 	
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="customerForm.street"/>
		            <form:input cssClass="form-control" path="deliveryStreet" id="deliveryStreet" maxlength="50"/>
		            <form:errors path="deliveryStreet" cssClass="help-block" cssStyle="color:red"/>
		        </div> 					        
				<div class="row">
			        <div class="col-sm-6 form-group">
			            <appfuse:label styleClass="control-label" key="customerForm.city"/>
			            <form:input cssClass="form-control" path="deliveryCity" id="deliveryCity" maxlength="50"/>
			            <form:errors path="deliveryCity" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>
			        <div class="col-sm-6 form-group">
		            <appfuse:label styleClass="control-label" key="customerForm.postalCode"/>
			            <form:input cssClass="form-control" path="deliveryPostalCode" id="deliveryPostalCode" maxlength="50"/>
			            <form:errors path="deliveryPostalCode" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>					        
			    </div>	
				<div class="row">
			        <div class="col-sm-6 form-group">
			            <appfuse:label styleClass="control-label" key="customerForm.stateId"/>
			            <form:input cssClass="form-control" path="deliveryStateId" id="deliveryStateId" maxlength="50"/>
			            <form:errors path="deliveryStateId" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>
			        <div class="col-sm-6 form-group">
		            <appfuse:label styleClass="control-label" key="customerForm.countryId"/>
			            <form:input cssClass="form-control" path="deliveryCountryId" id="deliveryCountryId" maxlength="50"/>
			            <form:errors path="deliveryCountryId" cssClass="help-block" cssStyle="color:red"/>					        
			        </div>					        
			    </div>	
			</div>			
		</div>				        			          
         <div class="row">
        	<br/>
        	<br/>
       	</div>             
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            <button type="reset" class="btn" name="reset" onclick="bCancel=false">
                <i class="icon-reset"></i> <fmt:message key="button.reset"/>
            </button>
            <c:if test="${param.from == 'list' and param.method != 'Add'}">
              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
              </button>
            </c:if>

            <button type="submit" class="btn btn-default" value="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>        
     </form:form>
</div>
<c:set var="scripts" scope="request">
<%@ include file="/scripts/customerForm.js"%>
</c:set>       