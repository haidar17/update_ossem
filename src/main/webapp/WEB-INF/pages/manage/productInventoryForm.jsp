<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="productInventoryForm.title"/></title>
    <meta name="menu" content="ManageMenu"/> 
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>        
</head>
<c:set value="${param.method == 'Add'}" var="isAdd"/>
<c:set var="delObject" scope="request">template</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<div class="col-sm-2">
    <h2><fmt:message key="productInventoryForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="productInventoryForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="productInventoryForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">
    <spring:bind path="productInventoryForm.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
    <form:form commandName="productInventoryForm" method="post" action="productInventoryForm" id="productInventoryForm" autocomplete="off"
               cssClass="well">
        <form:hidden path="id"/> 
         <div class="form-group">
            <appfuse:label styleClass="control-label" key="productInventoryForm.productCode"/>
            <form:input cssClass="form-control" path="productCode" id="productCode" maxlength="50"/>
            <form:errors path="productCode" cssClass="help-block" cssStyle="color:red"/>
        </div> 
          <div class="form-group">
            <appfuse:label styleClass="control-label" key="productInventoryForm.productName"/>
            <form:input cssClass="form-control" path="productName" id="productName" readonly="true"/>
            <form:errors path="productName" cssClass="help-block" cssStyle="color:red"/>
        </div>          
		<div class="row">
	        <div class="col-sm-4 form-group">
	            <appfuse:label styleClass="control-label" key="productInventoryForm.currentCount"/>
	            <form:input cssClass="form-control" path="currentCount" id="currentCount" readonly="true" cssStyle="text-align:right"/>
	            <form:errors path="currentCount" cssClass="help-block" cssStyle="color:red"/>
	        </div>
	        <div class="col-sm-4 form-group">
	            <appfuse:label styleClass="control-label" key="productInventoryForm.lowerLimitCount"/>
	            <form:input cssClass="form-control" path="lowerLimitCount" id="lowerLimitCount" readonly="true" cssStyle="text-align:right"/>
	            <form:errors path="lowerLimitCount" cssClass="help-block" cssStyle="color:red"/>
	        </div> 
          	<div class="col-sm-4 form-group">
            	<appfuse:label styleClass="control-label" key="productInventoryForm.upperLimitCount"/>
            	<form:input cssClass="form-control" path="upperLimitCount" id="upperLimitCount" readonly="true" cssStyle="text-align:right"/>
            	<form:errors path="upperLimitCount" cssClass="help-block" cssStyle="color:red"/>
        	</div>
        </div>   
 	   <ul class="nav nav-tabs">
		  <li class="active"><a href="#adjustment" data-toggle="tab"><appfuse:label styleClass="control-label" key="salesOrderForm.orderItems"/></a></li>
	   </ul>
	   <div id="myTabContent" class="tab-content">	   
		  <div class="tab-pane fade active in" id="adjustment"><br/>
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="productInventoryForm.entryCount"/>
		            <form:input cssClass="form-control" path="entryCount" id="entryCount" maxlength="50"/>
		            <form:errors path="entryCount" cssClass="help-block" cssStyle="color:red"/>
		        </div>
		         <div class="form-group">
		            <appfuse:label styleClass="control-label" key="productInventoryForm.entryNote"/>
		            <form:input cssClass="form-control" path="entryNote" id="entryNote" maxlength="50"/>
		            <form:errors path="entryNote" cssClass="help-block" cssStyle="color:red"/>
		        </div>  		  
		  </div>
		</div>                               
         <div class="row">
        	<br/>
        	<br/>
       	</div>             
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            <button type="reset" class="btn" name="reset" onclick="bCancel=false">
                <i class="icon-reset"></i> <fmt:message key="button.reset"/>
            </button>
            <c:if test="${param.from == 'list' and param.method != 'Add'}">
              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
              </button>
            </c:if>
            <button type="submit" class="btn btn-default" value="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>        
     </form:form>
</div>
<c:set var="scripts" scope="request">
<%@ include file="/scripts/productInventoryForm.js"%>
</c:set>       