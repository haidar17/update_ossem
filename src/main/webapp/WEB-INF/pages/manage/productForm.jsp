<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="productForm.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>
<c:set value="${param.method == 'Add'}" var="isAdd"></c:set>
<c:set var="delObject" scope="request">product</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<div class="col-sm-2">
    <h2><fmt:message key="productForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="productForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="productForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">
    <spring:bind path="productForm.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
    <form:form commandName="productForm" method="post" action="productsForm" id="productForm" autocomplete="off"
               cssClass="well">
        <form:hidden path="id"/>
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="productForm.code"/>
            <form:input cssClass="form-control" path="code" id="code" maxlength="50"/>
            <form:errors path="code" cssClass="help-block" cssStyle="color:red"/>
        </div>  
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="productForm.description"/>
            <form:input cssClass="form-control" path="description" id="description" maxlength="50"/>
            <form:errors path="description" cssClass="help-block" cssStyle="color:red"/>
        </div> 

 	   <ul class="nav nav-tabs">
		  <li class="active"><a href="#productSizeSet" data-toggle="tab"><appfuse:label styleClass="control-label" key="productForm.productSizeSetId"/></a></li>
		  <li class=""><a href="#productComponentSet" data-toggle="tab"><appfuse:label styleClass="control-label" key="productForm.productComponentSetId"/></a></li>  
	   </ul>
	   <div id="myTabContent" class="tab-content">	   
		  	<div class="tab-pane fade active in" id="productSizeSet"><br/>       
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="productForm.productSizeSetId"/>
					<form:select path="productSizeSetId" cssClass="form-control">
						<option value=""></option>
						<c:forEach items="${productSizeSets}" var="productSize">
							<option  value="${productSize.value}" ${productSize.value ==productForm.productSizeSetId ? "SELECTED":""}>${productSize.label}</option>
						</c:forEach>
					</form:select>
		        </div>  
		  	</div> 
		  	<div class="tab-pane fade active in" id="productComponentSet"><br/>
		        <div class="form-group">
		            <appfuse:label styleClass="control-label" key="productForm.productComponentSetId"/>
					<form:select path="productComponentSetId" cssClass="form-control">
						<option value=""></option>
						<c:forEach items="${productComponentSets}" var="productComponent">
							<option  value="${productComponent.value}" ${productComponent.value ==productForm.productComponentSetId ? "SELECTED":""}>${productComponent.label}</option>
						</c:forEach>
					</form:select>
		        </div>		  	
		  	</div>
		 </div>	
		 	  	                                                         
        
         <div class="row">
        	<br/>
        	<br/>
       	</div>             
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            <button type="reset" class="btn" name="reset" onclick="bCancel=false">
                <i class="icon-reset"></i> <fmt:message key="button.reset"/>
            </button>
            <c:if test="${param.from == 'list' and param.method != 'Add'}">
              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
              </button>
            </c:if>

            <button type="submit" class="btn btn-default" value="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>        
     </form:form>
</div>
<c:set var="scripts" scope="request">
<%@ include file="/scripts/productForm.js"%>
</c:set>       