<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="productInventory.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="productInventory.heading"/></h2>
     <form method="get" action="${ctx}/manage/productInventory" id="searchForm" class="form-inline">
    <div id="search" class="text-right">
        <span class="col-sm-9">
            <input type="text" size="20" name="q" id="query" value="${param.q}"
                   placeholder="<fmt:message key="search.enterTerms"/>" class="form-control input-sm">
        </span>
        <button id="button.search" class="btn btn-default btn-sm" type="button">
            <i class="icon-search"></i> <fmt:message key="button.search"/>
        </button>
    </div>
    </form>
    
    <div id="actions" class="btn-group">
        <a class="btn btn-primary" href="<c:url value='${ctx}/manage/productInventoryForm'/>">
            <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/></a>

        <a class="btn btn-default" href="<c:url value='/home'/>">
            <i class="icon-ok"></i> <fmt:message key="button.done"/></a>
    </div>  
    <display:table name="productInventory" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="productInventory" pagesize="25" class="table table-condensed table-striped table-hover" export="true">                      
        <display:column escapeXml="false" sortable="true" titleKey="productInventory.productCode"
                        style="width: 20%">
             <a href="${ctx}/manage/productInventoryForm?id=${productInventory.id}&from=list&method=Edit"><c:out value="${productInventory.product.code}"/></a>
        </display:column>
        <display:column property="product.name" escapeXml="true" sortable="true" titleKey="productInventory.productName" style="width: 20%"/>
        <display:column property="currentCount" escapeXml="true" sortable="true" titleKey="productInventory.currentCount" style="width: 20%"/>
        <display:column property="lowerLimitCount" escapeXml="true" sortable="true" titleKey="productInventory.lowerLimitCount" style="width: 20%"/>
        <display:column property="upperLimitCount" escapeXml="true" sortable="true" titleKey="productInventory.upperLimitCount" style="width: 20%"/>                     
        <display:column property="entryCount" escapeXml="true" sortable="true" titleKey="productInventory.entryCount" style="width: 20%"/>
        <display:column property="entryDate" escapeXml="true" sortable="true" titleKey="productInventory.entryDate" style="width: 20%"/>                     

        <display:setProperty name="paging.banner.item_name"><fmt:message key="productInventory.entry"/></display:setProperty>
        <display:setProperty name="paging.banner.items_name"><fmt:message key="productInventory.entries"/></display:setProperty>

        <display:setProperty name="export.excel.filename" value="Product Inventory List.xls"/>
        <display:setProperty name="export.csv.filename" value="Product Inventory List.csv"/>
        <display:setProperty name="export.pdf.filename" value="Product Inventory List.pdf"/>
    </display:table>       
</div>