<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="productComponentSetForm.title"/></title>
    <meta name="menu" content="ManageMenu"/>   
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>     
</head>
<c:set value="${param.method == 'Add'}" var="isAdd"/>
<c:set var="delObject" scope="request">productComponentSet</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<c:if test="${productComponentSetForm.hasProductComponents}">
<div class="hidden" id="productComponentsJSON">
    <json:object name="list">
        <json:array name="productComponents" var="productComponent" items="${productComponentSetForm.productComponents}">
            <json:object>
                <json:property name="id" value="${productComponent.id}"/>
                <json:property name="description" value="${productComponent.description}"/>
            </json:object>
        </json:array>
    </json:object>
</div>
</c:if>
<div class="col-sm-2">
    <h2><fmt:message key="productComponentSetForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="productComponentSetForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="productComponentSetForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">
    <spring:bind path="productComponentSetForm.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
    <form:form commandName="productComponentSetForm" method="post" action="productComponentSetsForm" id="productComponentSetForm" autocomplete="off"
               cssClass="well">
        <form:hidden path="id"/> 
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="productComponentSetForm.name"/>
            <form:input cssClass="form-control" path="name" id="name" maxlength="50"/>
            <form:errors path="name" cssClass="help-block" cssStyle="color:red"/>
        </div>  
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="productComponentSetForm.description"/>
            <form:input cssClass="form-control" path="description" id="description" maxlength="50"/>
            <form:errors path="description" cssClass="help-block" cssStyle="color:red"/>
        </div>   
 		<div id="productComponents">
            <legend class="accordion-heading">
                <a data-toggle="collapse" href="#collapse-product-components"><fmt:message key="productComponentSetForm.productComponents"/></a>&nbsp;<span class="badge">${productComponentSetForm.productComponentCount}</span>
            </legend>    
            <div id="collapse-product-components" class="accordion-body collapse"> 
            	<div class="row" id="productComponent-row">
	            	<div class="col-sm-4 form-group">
		            	<appfuse:label styleClass="control-label" key="productComponentSetForm.productComponentId"/>
		            	<div class="input-group">
		            		<input type="text" class="form-control addProductComponentFormInput" id="productComponent-id" name="productComponent-id" placeholder="e.g.: S"/>
							<span class="input-group-btn"><button class="btn btn-default" type="button" id="addProductComponent-btn">&emsp;<fmt:message key="button.add.sign"/>&emsp;</button></span>
		   				 </div>
		            </div>
	            	<div class="col-sm-2 form-group">
		            	<appfuse:label styleClass="control-label" key="productComponentSetForm.blank"/><br/>
			            <button type="button" class="btn" name="productComponent-btn" id="productComponent-btn" onclick="bCancel=false">
			                <i class="icon-add icon-white"></i>&emsp;<fmt:message key="button.add.sign"/>&emsp;
			            </button>
		            </div>		            
	            </div>            	            
            </div>
        </div>                               
         <div class="row">
        	<br/>
        	<br/>
       	</div>             
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            <button type="reset" class="btn" name="reset" onclick="bCancel=false">
                <i class="icon-reset"></i> <fmt:message key="button.reset"/>
            </button>
            <c:if test="${param.from == 'list' and param.method != 'Add'}">
              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
              </button>
            </c:if>
            <button type="submit" class="btn btn-default" value="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>        
     </form:form>
</div>

<div class="modal" id="addProductComponentModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="addProductComponentModalForm_title">Quick Create Product Component Form</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Component Code *</label>
			<input id="addProductComponentModalForm_id" type="text" class="form-control addProductComponentModalFormInput" />
        </div> 
        <div class="form-group">
            <label class="control-label">Component Colour</label>
			<input id="addProductComponentModalForm_description" type="text" class="form-control" />
        </div>                         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addProductComponentModalForm-create-btn" >Create Product Component</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<c:set var="scripts" scope="request">
<%@ include file="/scripts/productComponentSetForm.js"%>
</c:set>  