<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="productPricings.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="productPricings.heading"/></h2>
     <form method="get" action="${ctx}/manage/productPricings" id="searchForm" class="form-inline">
    <div id="search" class="text-right">
        <span class="col-sm-9">
            <input type="text" size="20" name="q" id="query" value="${param.q}"
                   placeholder="<fmt:message key="search.enterTerms"/>" class="form-control input-sm">
        </span>
        <button id="button.search" class="btn btn-default btn-sm" type="submit">
            <i class="icon-search"></i> <fmt:message key="button.search"/>
        </button>
    </div>
    </form>
    
    <div id="actions" class="btn-group">
        <a class="btn btn-primary" href="<c:url value='${ctx}/manage/productPricingsForm?method=New&from=list'/>">
            <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/></a>

        <a class="btn btn-default" href="<c:url value='/home'/>">
            <i class="icon-ok"></i> <fmt:message key="button.done"/></a>
    </div>   
   <display:table name="customerProductPricings" cellspacing="0" cellpadding="0" requestURI=""
                  defaultsort="1" id="customerProductPricing" pagesize="25" class="table table-condensed table-striped table-hover" export="true">                      
         <display:column  property="customer.fullName" escapeXml="false" sortable="true" titleKey="productPricing.customerName"
                        style="width: 30%" paramId="customerId" paramProperty="customer.id" href="${ctx}/manage/productPricingsForm?from=list&method=View"/>
         <display:column property="rowCount" escapeXml="true" sortable="true" titleKey="productPricing.rowCount"
                        style="width: 20%; text-align:center"/>    
         <display:column escapeXml="true" sortable="true" titleKey="column.blank"
                        style="width: 60%; text-align:right"/>                                                
        <display:setProperty name="paging.banner.item_name"><fmt:message key="productPricings.entry"/></display:setProperty>
        <display:setProperty name="paging.banner.items_name"><fmt:message key="productPricings.entries"/></display:setProperty>

        <display:setProperty name="export.excel.filename" value="Template List.xls"/>
        <display:setProperty name="export.csv.filename" value="Template List.csv"/>
        <display:setProperty name="export.pdf.filename" value="Template List.pdf"/>
    </display:table>     
</div>    

