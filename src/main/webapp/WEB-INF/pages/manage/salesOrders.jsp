<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="salesOrders.title"/></title>
    <meta name="menu" content="ManageMenu"/>
</head>

<div class="col-sm-10">
    <h2><fmt:message key="salesOrders.heading"/></h2>
     <form method="get" action="${ctx}/manage/salesOrders" id="searchForm" class="form-inline">
    <div id="search" class="text-right">
        <span class="col-sm-9">
            <input type="text" size="20" name="q" id="query" value="${param.q}"
                   placeholder="<fmt:message key="search.enterTerms"/>" class="form-control input-sm">
        </span>
        <button id="button.search" class="btn btn-default btn-sm" type="button">
            <i class="icon-search"></i> <fmt:message key="button.search"/>
        </button>
    </div>
    </form>
    
    <div id="actions" class="btn-group">
        <a class="btn btn-primary" href="<c:url value='${ctx}/manage/salesOrdersForm'/>">
            <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/></a>

        <a class="btn btn-default" href="<c:url value='/home'/>">
            <i class="icon-ok"></i> <fmt:message key="button.done"/></a>
    </div>   
   <display:table name="salesOrders" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="salesOrder" pagesize="25" class="table table-condensed table-striped table-hover" export="true">                      
        <display:column escapeXml="false" sortable="true" titleKey="salesOrder.orderNo"
                        style="width: 20%">
             <a href="${ctx}/manage/salesOrdersForm?id=${salesOrder.id}&from=list&method=Edit"><c:out value="${salesOrder.orderNo}"/></a>
        </display:column>
        <display:column escapeXml="false" sortable="true" titleKey="salesOrder.orderDate"
                        style="width: 20%">
      		 <fmt:formatDate type="date" value="${salesOrder.orderDate}" pattern="MMM d, yyyy"/>                  
        </display:column>   
        <display:column property="customer.fullName" escapeXml="true" sortable="true" titleKey="salesOrder.customer"
                        style="width: 40%"/>
        <display:column escapeXml="false" sortable="true" titleKey="salesOrder.status"
                        style="width: 20%">
              <span class="label ${salesOrder.statusCss}">${salesOrder.statusName}</span>
        </display:column>                                                       
        <display:setProperty name="paging.banner.item_name"><fmt:message key="salesOrders.entry"/></display:setProperty>
        <display:setProperty name="paging.banner.items_name"><fmt:message key="salesOrders.entries"/></display:setProperty>

        <display:setProperty name="export.excel.filename" value="Operation Order List.xls"/>
        <display:setProperty name="export.csv.filename" value="Operation Order List.csv"/>
        <display:setProperty name="export.pdf.filename" value="Operation Order List.pdf"/>
    </display:table>     
</div>    