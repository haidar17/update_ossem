<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="templateForm.title"/></title>
    <meta name="menu" content="ManageMenu"/>   
</head>
<c:set value="${param.method == 'Add'}" var="isAdd"/>
<c:set var="delObject" scope="request">template</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<div class="col-sm-2">
    <h2><fmt:message key="templateForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="templateForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="templateForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">
    <spring:bind path="templateForm.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
    <form:form commandName="templateForm" method="post" action="templatesForm" id="templateForm" autocomplete="off"
               cssClass="well">
        <form:hidden path="id"/> 
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="templateForm.name"/>
            <form:input cssClass="form-control" path="name" id="name" maxlength="50"/>
            <form:errors path="name" cssClass="help-block" cssStyle="color:red"/>
        </div>  
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="templateForm.description"/>
            <form:input cssClass="form-control" path="description" id="description" maxlength="50"/>
            <form:errors path="description" cssClass="help-block" cssStyle="color:red"/>
        </div>                         
         <div class="row">
        	<br/>
        	<br/>
       	</div>             
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            <button type="reset" class="btn" name="reset" onclick="bCancel=false">
                <i class="icon-reset"></i> <fmt:message key="button.reset"/>
            </button>
            <c:if test="${param.from == 'list' and param.method != 'Add'}">
              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
              </button>
            </c:if>
            <button type="submit" class="btn btn-default" value="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>        
     </form:form>
</div>
<c:set var="scripts" scope="request">
<%@ include file="/scripts/templateForm.js"%>
</c:set>       