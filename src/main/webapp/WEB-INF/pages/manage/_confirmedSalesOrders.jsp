<%@ include file="/common/taglibs.jsp"%>
		         <legend class="accordion-heading">
		             <a data-toggle="collapse" href="#collapse-sales-order"><fmt:message key="deliveryOrderForm.salesOrder"/></a>
		         </legend>    
		         <c:set var="confirmedSalesOrders_size" value="${fn:length(confirmedSalesOrders)}"/> 
		         <div id="collapse-sales-order" class="accordion-body collapse">  
					<div class="panel panel-primary">
					  <div class="panel-heading">
					    <h3 class="panel-title" style="color:white">
				        	<c:if test="${confirmedSalesOrders_size == 0}">
				        		No record found.
				        	</c:if>
				        	<c:if test="${confirmedSalesOrders_size > 0}">
				        		<c:out value="${confirmedSalesOrders_size} records found, displaying all records."/>
				        	</c:if>					    
					    </h3>
					  </div>
					  <div class="panel-body">				   
				 		<div id="confirmedSalesOrdersPane" class="control-group span12" style="height:200px; overflow-y:scroll"> 
						   	 <display:table name="confirmedSalesOrders" cellspacing="0" cellpadding="0" requestURI=""
						                   defaultsort="1" id="confirmedSalesOrder" pagesize="0" class="table table-condensed table-striped table-hover">
				                <display:column escapeXml="false" sortable="true" titleKey="salesOrder.included" style="width: 5px">
				                	<input type="checkbox" class="selectedRow" id="selectedConfirmedSalesOrderId" name="selectedConfirmedSalesOrderId" value="${confirmedSalesOrder.id}" ${confirmedSalesOrder.selectedSalesOrder ? 'checked=checked':''}" ${!deliveryOrderForm.changeConfirmedSalesOrderAllowed ? 'disabled=disabled':''}/>
				                </display:column>                      
					        	<display:column escapeXml="false" sortable="true" titleKey="salesOrder.orderNoDate" style="width: 20%">
					        		<c:out value="${confirmedSalesOrder.orderNo }"/><br/>
					      		 	<fmt:formatDate type="date" value="${confirmedSalesOrder.orderDate}" pattern="MMM d, yyyy"/>                  
					        	</display:column> 
					        	<display:column escapeXml="false" sortable="true" titleKey="salesOrder.status" style="width: 10%">
					        		<span class="label ${confirmedSalesOrder.statusCss}">${confirmedSalesOrder.statusName}</span>
					        	</display:column> 
								<c:set var="salesOrderItems" value="${confirmedSalesOrder.salesOrderItems}" />								
								<display:column title="Order Items" style="width: 60%" sortable="true">								
								  	<display:table name="${salesOrderItems}" id="salesOrderItem" pagesize="0">
										<display:column escapeXml="false" titleKey="product.code" style="width: 40%">
											<div title="${salesOrderItem.product.name }">${salesOrderItem.product.code }</div>
										</display:column>
										<display:column property="quantity" titleKey="product.quantity" style="text-align:center;width: 20%"/>
										<display:column titleKey="product.unitPrice" style="text-align:right;width: 20%">
											<fmt:formatNumber value="${salesOrderItem.unitPrice}" pattern="$###,###.00"/>
										</display:column>
										<display:column escapeXml="false" titleKey="product.total" style="text-align:right;width: 20%">
											<fmt:formatNumber value="${ salesOrderItem.unitPrice * salesOrderItem.quantity}" pattern="$###,###.00"/>
										</display:column>
									</display:table>
								</display:column>													         		
						    </display:table>
						</div> 					
					</div>
				</div>						
			</div>	