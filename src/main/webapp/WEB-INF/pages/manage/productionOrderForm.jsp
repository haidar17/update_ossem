<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="productionOrderForm.title"/></title>
    <meta name="menu" content="ManageMenu"/>   
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>
</head>
<c:set value="${param.method == 'Add'}" var="isAdd"/>
<c:set var="delObject" scope="request">production order</c:set>
<script type="text/javascript">var msgDelConfirm =
   "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<c:if test="${productionOrderForm.hasProductionOrderItems}">
<div class="hidden" id="productionOrderItemsJSON">
    <json:object name="list">
        <json:array name="productionOrderItems" var="productionOrderItem" items="${productionOrderForm.productionOrderItems}">
            <json:object>
                <json:property name="id" value="${productionOrderItem.id}"/>
                <json:property name="productId" value="${productionOrderItem.product.id}"/>
                <json:property name="productName" value="${productionOrderItem.product.name}"/>
                <json:property name="productCode" value="${productionOrderItem.product.code}"/>
                <json:property name="quantity" value="${productionOrderItem.quantity}"/>
                <c:if test="${productionOrderItem.hashProductionOrderQuantityBySizes}">
			        <json:array name="productionOrderQuantityBySizes" var="productionOrderQuantityBySize" items="${productionOrderItem.productionOrderQuantityBySizes}">
			            <json:object>
			                <json:property name="id" value="${productionOrderQuantityBySize.id}"/>
			                <json:property name="size" value="${productionOrderQuantityBySize.size}"/>
			                <json:property name="orderQuantity" value="${productionOrderQuantityBySize.orderQuantity}"/>
			            </json:object>
			        </json:array>
		        </c:if>        
            </json:object>
        </json:array>
    </json:object>
</div>
</c:if>
<c:if test="${productionOrderForm.hasAdjustments}">
<div class="hidden" id="adjustmentsJSON">
    <json:object name="list">
        <json:array name="adjustments" var="adjustment" items="${productionOrderForm.adjustments}">
            <json:object>
                <json:property name="id" value="${adjustment.id}"/>
                <json:property name="adjustmentCode" value="${adjustment.code}"/>
                <json:property name="adjustmentDate" value="${adjustment.date}"/>
                <json:property name="adjustmentNote" value="${adjustment.note}"/>
                <json:property name="adjustmentReference" value="${adjustment.reference}"/>
                <json:property name="adjustmentQty" value="${adjustment.quantity}"/>
            </json:object>
        </json:array>
    </json:object>
</div>
</c:if>
<div class="col-sm-2">
    <h2><fmt:message key="productionOrderForm.heading"/></h2>
    <c:choose>
        <c:when test="${param.from == 'list'}">
            <p><fmt:message key="productionOrderForm.admin.message"/></p>
        </c:when>
        <c:otherwise>
            <p><fmt:message key="productionOrderForm.message"/></p>
        </c:otherwise>
    </c:choose>
</div>
<div class="col-sm-7">
    <spring:bind path="productionOrderForm.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
    <form:form commandName="productionOrderForm" method="post" action="productionOrdersForm" id="productionOrderForm" autocomplete="off"
               cssClass="well">
        <form:hidden path="id"/> 
        <form:hidden path="status" id="status"/>  
         <div class="form-group">
			<div class="alert alert-dismissable alert-info">
			  <button type="button" class="close" data-dismiss="alert">�</button>
			  <c:if test="${productionOrderForm.newOrder}">
			  <p>This is a new order.</p>
			  </c:if>			  
			  <c:if test="${productionOrderForm.draftOrder}">
			  <p>This is a draft order.</p>
			  </c:if>
			  <c:if test="${productionOrderForm.acceptedOrder}">
			  <p>NOTE: This order has been accepted.</p>
			  </c:if>
			  <c:if test="${productionOrderForm.confirmedOrder}">
			  <p>NOTE: This order has been confirmed.</p>
			  </c:if>
			  <c:if test="${productionOrderForm.completedOrder}">
			  <p>NOTE: This order has been completed.</p>
			  </c:if>			  			  
			  <c:if test="${productionOrderForm.rejectedOrder}">
			  <p>NOTE: This order has been rejected.</p>
			  </c:if>			  			  
			</div>
        </div>              
		<div class="row">
	        <div class="col-sm-6 form-group">
	            <appfuse:label styleClass="control-label" key="productionOrderForm.orderNo"/>
	            <form:input cssClass="form-control" path="orderNo" id="orderNo" readonly="true"/>
	            <form:errors path="orderNo" cssClass="help-block" cssStyle="color:red"/>
        	</div>  
        	<div class="col-sm-6 form-group">
	            <appfuse:label styleClass="control-label" key="productionOrderForm.orderDate"/>
	            <form:input cssClass="form-control" path="orderDate" id="orderDate" maxlength="50" placeholder="mm/dd/yyyy"/>
	            <form:errors path="orderDate" cssClass="help-block" cssStyle="color:red"/>
        	</div>   
        </div>     
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="productionOrderForm.reference"/>
            <form:input cssClass="form-control" path="reference" id="reference" maxlength="50" placeholder="e.g.: REF/000001"/>
            <form:errors path="reference" cssClass="help-block" cssStyle="color:red"/>
        </div> 
 	   <ul class="nav nav-tabs">
		  <li class="active"><a href="#productionOrderItems" data-toggle="tab"><appfuse:label styleClass="control-label" key="productionOrderForm.productionOrderItems"/>&nbsp;<span class="badge">${productionOrderForm.productionOrderItemCount}</span></a></li>
		  <li class=""><a href="#adjustments" data-toggle="tab"><appfuse:label styleClass="control-label" key="productionOrderForm.adjustments"/>&nbsp;<span class="badge">${productionOrderForm.adjustmentCount}</span></a></li>  
	   </ul>
	   <div id="myTabContent" class="tab-content">	   
		  <div class="tab-pane fade active in" id="productionOrderItems"><br/>
		  	<div class="control-group well" style="height:200px; overflow-y:scroll">
            	<div class="row" id="orderProduct-row">
	            	<div class="col-sm-6 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.orderProductCode"/>
		            	<div class="input-group">
		            		<input type="text" class="form-control" id="orderProduct-code" name="orderProduct-code" placeholder="e.g.: 10001" maxlength="50"/>
							<span class="input-group-btn"><button class="btn btn-default" type="button" id="addProductCode-btn" ${productionOrderForm.addOrderItemAllowed ? "":"disabled='disabled'"}>&emsp;<fmt:message key="button.add.sign"/>&emsp;</button></span>
		   				 </div>		            		
		            </div>			            
	            	<div class="col-sm-2 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.orderQty"/>
		            	<input type="text" class="form-control" style="text-align:right" value="1" id="orderProduct-qty" name="orderProduct-qty"/>
		            </div>		            
	            	<div class="col-sm-2 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.blank"/><br/>
			            <button type="button" class="btn" name="orderProduct-btn" id="orderProduct-btn" onclick="bCancel=false" ${productionOrderForm.addOrderItemAllowed ? "":"disabled='disabled'"}>
			                <i class="icon-add icon-white"></i>&emsp;<fmt:message key="button.add.sign"/>&emsp;
			            </button>
		            </div>	
	            	<div class="col-sm-2 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.blank"/>
		            	<input type="hidden" class="form-control" style="text-align:right" value="" id="orderProduct-qtyBySize" name="orderProduct-qtyBySize"/>
			            <br/>	
			            <button type="button" class="btn" name="orderQtyBySize-edit-btn" id="orderQtyBySize-edit-btn" onclick="bCancel=false" disabled="disabled" data-index="">
			                <i class="icon-add icon-white"></i>Detail
			            </button>		            	
		            </div>		            	            
	            </div> 	
	         </div>	  
		  </div>
		  <div class="tab-pane fade" id="adjustments"><br/>
		  	  <div class="control-group well" style="height:200px; overflow-y:scroll">
	           	<div class="row" id="adjustment-row">
	            	<div class="col-sm-2 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.adjustmentCode"/>
		            	<input type="text" class="form-control" id="adjustment-code" name="adjustment-code" placeholder="e.g.: 10001"/>
		            </div>	           	
	            	<div class="col-sm-3 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.adjustmentDate"/>
		            	<input type="text" class="form-control" id="adjustment-date" name="adjustment-date" placeholder="mm/dd/yyyy"/>
		            </div>
	            	<div class="col-sm-3 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.adjustmentReference"/>
		            	<input type="text" class="form-control" placeholder="e.g. auditing adjustment" id="adjustment-reference" name="adjustment-reference"/>
		            </div>	
	            	<div class="col-sm-2 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.adjustmentQty"/>
		            	<input type="text" class="form-control" style="text-align:right" value="1" id="adjustment-qty" name="adjustment-qty"/>
		            </div>		            
	            	<div class="col-sm-2 form-group">
		            	<appfuse:label styleClass="control-label" key="productionOrderForm.blank"/><br/>
			            <button type="button" class="btn" name="adjustment-btn" id="adjustment-btn" onclick="bCancel=false" ${productionOrderForm.addAdjustmentAllowed ? "":"disabled='disabled'"}>
			                <i class="icon-add icon-white"></i>&emsp;<fmt:message key="button.add.sign"/>&emsp;
			            </button>
		            </div>		            
		         </div> 		  
		     </div>
		  </div>		  
	   </div>                                       
         <div class="row">
        	<br/>
        	<br/>
       	</div>             
        <div class="form-group">
        	<c:if test="${!productionOrderForm.rejectedOrder &&
        					 !productionOrderForm.completedOrder}">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            </c:if>
            <c:if test="${productionOrderForm.draftOrder}">
	           <button type="button" class="btn btn-warning" name="accept" id="acceptOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.accept"/>
	            </button>   
	           <button type="button" class="btn btn-danger" name="reject"  id="rejectOrder-btn"  onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.reject"/>
	            </button>  	             
            </c:if> 
             <c:if test="${productionOrderForm.acceptedOrder}">
	           <button type="button" class="btn btn-success" name="confirm" id="confirmOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.confirm"/>
	            </button>   	             
            </c:if>   
              <c:if test="${productionOrderForm.confirmedOrder}">
	           <button type="button" class="btn btn-info" name="completed" id="completeOrder-btn" onclick="bCancel=false">
	                <i class="icon-ok icon-white"></i> <fmt:message key="button.complete"/>
	            </button>   	             
            </c:if>                       
        	<c:if test="${!productionOrderForm.rejectedOrder &&
        					 !productionOrderForm.completedOrder}">   
			<a class="btn btn-default" href="<c:url value='${ctx}/manage/productionOrdersForm?id=${productionOrderForm.id}&from=list&method=Edit'/>">
           			<i class="icon-reset"></i> <fmt:message key="button.reset"/></a>
            </c:if>
            <c:if test="${param.from == 'list' and param.method != 'Add' and (productionOrderForm.draftOrder)}">
              <button type="submit" class="btn btn-default" name="delete" onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                  <i class="icon-trash"></i> <fmt:message key="button.delete"/>
              </button>
            </c:if>
            <button type="submit" class="btn btn-default" value="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>        
     </form:form>
</div>

<div class="modal" id="addProductModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="addProductModalForm_title">Quick Create Product Form</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Product Name *</label>
			<input id="addProductModalForm_productName" type="text" class="form-control addProductModalFormInput"/>
        </div> 
        <div class="form-group">
            <label class="control-label">Product Code *</label>
			<input id="addProductModalForm_productCode" type="text" class="form-control addProductModalFormInput"/>
        </div>         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addProductModalForm-create-btn" >Create Product</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="addOrderItemBySizeModalForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="addOrderItemBySizeModalForm_title">Add Order Item Form</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
            <label class="control-label">Product Code</label>
			<input id="addOrderItemBySizeModalForm_productCode" type="text" readonly="readonly" class="form-control addOrderItemBySizeModalFormInput"/>
			<input id="addOrderItemBySizeModalForm_qtyBySize" type="hidden" value="" class="addOrderItemBySizeModalFormInput"/>
			<input id="addOrderItemBySizeModalForm_index" type="hidden" value="" class="addOrderItemBySizeModalFormInput"/>
        </div>          
		   <ul class="nav nav-tabs">
			  <li class="active"><a href="#myPopupTabContent #orderItemBySize" data-toggle="tab"><appfuse:label styleClass="control-label" key="productionOrderForm.orderItemsQtyBySize"/></a></li>
		   </ul>
		   <div id="myPopupTabContent" class="tab-content">	   
			  <div class="tab-pane fade active in" id="orderItemBySize"><br/>
				  <div class="control-group well" style="height:200px; overflow-y:scroll"> 
					  	<div class="row hidden" id="orderItemBySizeRow">
					        <div class="col-sm-2 form-group">
					            <label class="control-label">Item<br/>Size</label>
								<input id="addOrderItemBySizeModalForm_size" name="addOrderItemBySizeModalForm_size" type="text" readonly="readonly" class="form-control addOrderItemBySizeModalFormInput"/>
								<input id="addOrderItemBySizeModalForm_id" name="addOrderItemBySizeModalForm_size" type="hidden" class="addOrderItemBySizeModalFormInput"/>
					        </div> 
					        <div class="col-sm-2 form-group">
					            <label class="control-label">In Stock<br/>Qty</label>
								<input id="addOrderItemBySizeModalForm_inStock"  name="addOrderItemBySizeModalForm_inStock" type="text" readonly="readonly"  class="form-control addOrderItemBySizeModalFormInput"/>
					        </div>
					        <div class="col-sm-2 form-group">
					            <label class="control-label">In Prod.<br/>Qty</label>
								<input id="addOrderItemBySizeModalForm_inProduction"  name="addOrderItemBySizeModalForm_inProduction" type="text" readonly="readonly"  class="form-control addOrderItemBySizeModalFormInput"/>
					        </div>	
					        <div class="col-sm-2 form-group">
					            <label class="control-label">Order<br/>Qty</label>
								<input id="addOrderItemBySizeModalForm_qty" name="addOrderItemBySizeModalForm_qty" value="0" type="text" class="form-control addOrderItemBySizeModalFormInput"/>
					        </div> 	
					        <div class="col-sm-2 form-group">
					            &emsp;
					        </div> 				        		        		        			        			        
					  	 </div>
					  	 <div class="row">
					  	 	<br/><br/>
					  	 </div>
				  	</div>
			  </div>
		   </div> 		        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addOrderItemBySizeModalForm-create-btn">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<c:set var="scripts" scope="request">
<%@ include file="/scripts/productionOrderForm.js"%>
</c:set>       