<script type="text/javascript">
	var counter = 1;
	var addedProductCode = [];
	$(document).ready(function(){
		try {
			$('#orderDate').datepicker();
			
			$('#customerFullName').autocomplete({
			    //lookup: customers,
				serviceUrl: '/services/api/customers/suggest.json',
				transformResult: function(response) {
					var realArray = $.parseJSON($.makeArray(response));
			        return {
			            suggestions: $.map(realArray, function(dataItem) {
			                return { value: dataItem.label, data: dataItem.value };
			            })
			        };
			    },				
			    onSelect: function (suggestion) {
			        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
			    	$("#customerId").val(suggestion.data);
			    	var temp = suggestion.value.split("|");
			    	$("#customerFullName").val(temp[1].trim());			    	
			    },
			    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
			    	$.jGrowl(errorThrown);
			    }
			});	
			
			$('#orderProduct-code').autocomplete({
			    //lookup: customers,
				serviceUrl: '/services/api/products/suggest.json',
				transformResult: function(response) {
					var realArray = $.parseJSON($.makeArray(response));
			        return {
			            suggestions: $.map(realArray, function(dataItem) {
			                return { value: dataItem.label, data: dataItem.value };
			            })
			        };
			    },				
			    onSelect: function (suggestion) {
			        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
			    	var customerId = $("#customerId").val();
			    	var temp = suggestion.value.split("|");
			    	var productCode = temp[0].trim();
	
			    	// set product code
			    	$("#orderProduct-code").val(productCode);
			    	
			    	// get price per unit
			    	// /services/api/productPricings/{customerId}/price/{productCode}
			    	if (customerId != ''){
			    		var url = "${ctx}/services/api/productPricings/" + customerId + "/price/" + productCode + ".json";
			    		getCustomerProductUnitPrice_ajax(url, customerId, productCode, customerProductPriceCallback);
			    	} else {
			    		$('#customerFullName').focus();
			    		alert("Please choose a customer first.");
			    	}
			    },
			    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
			    	$.jGrowl(errorThrown);
			    }
			});		
			
			// this to remove all cloned row in addOrderItemBySizeModalForm
			$("#addOrderItemBySizeModalForm").on("hide.bs.modal", function(event){
		    	$(".clonedRow").each(function(){
		    		$(this).remove();
		    	});
		    	
		    	$(".addOrderItemBySizeModalFormInput").each(function(){
		    		$(this).val("");
		    	});
		    });
			
			$("#orderProduct-btn").click(function(){
				var selectedProductCode = $("#orderProduct-code").val();
				
				if ($.isNumeric(selectedProductCode)){
					var result = getProductSizeSet_ajax("${ctx}/services/api/products/" + selectedProductCode + "/productSizeSet.json");
					if ($.isNumeric(result["id"])){
						$(result["productSizes"]).each(function(index, element){
							var cloneRow = $("#orderItemBySizeRow").clone();
							$(cloneRow).removeClass("hidden");
							$(cloneRow).addClass("clonedRow");
							$(cloneRow).insertAfter($("#orderItemBySizeRow"));
							$(cloneRow).find("#addOrderItemBySizeModalForm_size").val(element["id"]);
							$(cloneRow).find("#addOrderItemBySizeModalForm_qty").val("0");
						});
						
						// set code
						$("#addOrderItemBySizeModalForm_productCode").val(selectedProductCode);
						
						// change button text
						$("#addOrderItemBySizeModalForm-create-btn").text("Add");
						
						// show modal form
						$("#addOrderItemBySizeModalForm").modal("show");
					} else {
						//alert("Product Size for product code '" + selectedProductCode + "' could not be found!");
						var message = "Product Size for product code '" + selectedProductCode + "' could not be found!";
						showPopoverAlert("#orderProduct-code", "Invalid!", message, "bottom", 2000);
						$("#orderProduct-code").focus();
					}
				} else {
					//alert("Please choose specify valid product code!");
					showPopoverAlert("#orderProduct-code", "Invalid!", "Please specify valid product code!", "bottom", 2000);
					$("#orderProduct-code").focus();
				}
				//$.jGrowl("addOrderItem");
//				var selectedProductCode = $("#orderProduct-code").val();
//				var unitPrice = $("#orderProduct-unit-price").val();
//				var selectedProductQty = $("#orderProduct-orderQty").val();
//				if (!$.isNumeric(selectedProductCode)) selectedProductCode = "";
//				if (!$.isNumeric(selectedProductQty)) selectedProductQty = 0;
//				if (selectedProductCode != '' && selectedProductQty > 0 && unitPrice > 0){
//					if ($.inArray(selectedProductCode, addedProductCode) == -1){
//						addedProductCode.push(selectedProductCode);
//						var cloneRow = $("#orderProduct-row").clone();
//						$(cloneRow).insertAfter($("#orderProduct-row"));
//						
//						// 1.
//						$(cloneRow).find("#orderProduct-code").attr("readonly", true);
//						$(cloneRow).find("#orderProduct-code").attr("name", "orderProduct-" + counter + "-code");
//						$(cloneRow).find("#orderProduct-code").attr("id", "orderProduct-" + counter + "-code");
//						
//						// 2.
//						$(cloneRow).find("#orderProduct-in-stock").val("1");
//						$(cloneRow).find("#orderProduct-in-stock").attr("name", "orderProduct-" + counter + "-stock");
//						$(cloneRow).find("#orderProduct-in-stock").attr("id", "orderProduct-" + counter + "-stock");
//						
//						// 3.
//						$(cloneRow).find("#orderProduct-orderQty").attr("readonly", true);
//						$(cloneRow).find("#orderProduct-orderQty").attr("name", "orderProduct-" + counter + "-orderQty");
//						$(cloneRow).find("#orderProduct-orderQty").attr("id", "orderProduct-" + counter + "-orderQty");
//
//						$(cloneRow).find("#orderProduct-deliveryQty").attr("readonly", true);
//						$(cloneRow).find("#orderProduct-deliveryQty").attr("name", "orderProduct-" + counter + "-deliveryQty");
//						$(cloneRow).find("#orderProduct-deliveryQty").attr("id", "orderProduct-" + counter + "-deliveryQty");
//						
//						// 3.
//						$(cloneRow).find("#orderProduct-unit-price").attr("readonly", true);
//						$(cloneRow).find("#orderProduct-unit-price").attr("name", "orderProduct-" + counter + "-unit-price");
//						$(cloneRow).find("#orderProduct-unit-price").attr("id", "orderProduct-" + counter + "-unit-price");
//						
//						// 4.
//						$(cloneRow).find("#orderProduct-btn").html("-");
//						$(cloneRow).find("#orderProduct-btn").click(function(){
//							addedProductCode.pop(selectedProductCode);
//							$(cloneRow).remove();
//						});
//						$(cloneRow).find("#orderProduct-btn").attr("name", "orderProduct-" + counter + "-btn");
//						$(cloneRow).find("#orderProduct-btn").attr("id", "orderProduct-" + counter + "-btn");
//						
//						// 5.
//						$(cloneRow).attr("name", "orderProduct-" + counter + "-row");
//						$(cloneRow).attr("id", "orderProduct-" + counter + "-row");
//						
//						// 6.
//						$(cloneRow).find("#addProductCode-btn").addClass("hidden");
//						
//						counter++;
//					} else {
//						//$.jGrowl("Already added!");
//						alert("Product with product code '" + selectedProductCode + "' has already been added. Please choose different code.");
//					}
//				} else {
//					if (selectedProductCode == '') {
//						$.jGrowl("Please enter valid product code!");
//						$("#orderProduct-code").focus();
//					}
//					if (selectedProductQty < 1) {
//						$.jGrowl("Please enter valid product quantity!");
//						$("#orderProduct-qty").focus();
//					}	
//					
//					if (unitPrice < 1) {
//						$.jGrowl("Invalid product unit price per customer!");
//						$("#orderProduct-code").focus();						
//					}						
//				}
			});
			
			if ($("#salesOrderItemsJSON").length > 0){
				var salesOrderItemsJSON = $.parseJSON($("#salesOrderItemsJSON").text());
				$(salesOrderItemsJSON["salesOrderItems"]).each(function(index, element){
					$("#orderProduct-code").val(element['productCode']);
					$("#orderProduct-orderQty").val(element['orderQuantity']);
					$("#orderProduct-deliveryQty").val(element['deliveryQuantity']);
					$("#orderProduct-unit-price").val(parseFloat(element['price']).toFixed(2));
					//$("#orderProduct-btn").click();
					displayExistingSalesOrderItem();
				});	
				
				resetSalesOrderItemForm();
			}	
			
			$("#acceptOrder-btn").click(function(){
				var yes = confirm("Are sure to accept this order?");
				if (yes){
					$("#status").val("A");
					$("#salesOrderForm").submit();
				}
			});
	
			$("#confirmOrder-btn").click(function(){
				var yes = confirm("Are sure to confirm this order?");
				if (yes){
					$("#status").val("C");
					$("#salesOrderForm").submit();
				}
			});
			
			$("#completeOrder-btn").click(function(){
				var yes = confirm("Are sure to set this order as completed?");
				if (yes){
					$("#status").val("E");
					$("#salesOrderForm").submit();
				}
			});
			
			$("#rejectOrder-btn").click(function(){
				var yes = confirm("Are sure to reject this order?");
				if (yes){
					$("#status").val("R");
					$("#salesOrderForm").submit();
				}
			});		
			
			$("#unitPriceModalForm-setPrice-btn").click(function(){				
				var productPrice = $("#unitPriceModalForm_productPrice").val();
				var productCode = $("#unitPriceModalForm_productCode").val();
				var customerId = $("#unitPriceModalForm_customerId").val();
				
				if ($.isNumeric(productPrice) && productPrice > 0){
					var yes = confirm("Are sure to set unit price to '" + productPrice + "'?");
					if (yes){
				    	var url = "${ctx}/services/api/productPricings/" + customerId + "/" + productCode + "/setPrice/" + productPrice +".json";
				    	setCustomerProductUnitPrice_ajax(url, customerId, productCode, productPrice);
					}
				} else {
					alert("Invalid product unit price!");
				}
			});		
			
			$("#addProductCode-btn").click(function(){
				var customerId = $("#customerId").val();
				
				if (customerId != ''){
					$("#addProductPricingModalForm_customerId").val(customerId);
					$("#addProductPricingModalForm").modal("show");
				} else {
		    		$('#customerFullName').focus();
		    		alert("Please choose a customer first.");
		    	}
			});

			$("#addCustomer-btn").click(function(){
				$("#addCustomerModalForm").modal("show");
			});			
			
			$("#addCustomerModalForm-create-btn").click(function(){
				var isAllFieldsEntered = true;
				$(".addCustomerModalFormInput").each(function(index){
					if ($(this).val() == ''){					
						isAllFieldsEntered = false;
						return false;
					}
				});	
				
				if (isAllFieldsEntered){
					var url = "${ctx}/services/api/customers/create";
					var customerJSON = {};
					var contactPersonJSON = {};
					customerJSON["fullName"] = $("#addCustomerModalForm_customerName").val();
					customerJSON["legalIdentity"] = $("#addCustomerModalForm_customerLegalId").val();
					customerJSON["firstName"] = $("#addCustomerModalForm_contactFirstName").val();
					customerJSON["lastName"] = $("#addCustomerModalForm_contactLastName").val();
			
					createCustomer_ajax(url, customerJSON, createCustomerCallback);
				} else {
					alert("Please fill up all the required fields!");
				}
			});
			
			$("#addProductPricingModalForm-create-btn").click(function(){
				var isAllFieldsEntered = true;
				$(".addProductPricingModalFormInput").each(function(index){
					if ($(this).val() == ''){					
						isAllFieldsEntered = false;
						return false;
					}
				});		
				
				if (isAllFieldsEntered){
					var url = "${ctx}/services/api/productPricings/create";
					var productPricingJSON = {};
					productPricingJSON["productName"] = $("#addProductPricingModalForm_productName").val();
					productPricingJSON["productCode"] = $("#addProductPricingModalForm_productCode").val();
					productPricingJSON["price"] = $("#addProductPricingModalForm_price").val();
					productPricingJSON["customerId"] = $("#addProductPricingModalForm_customerId").val();
			
					createProductPricing_ajax(url, productPricingJSON, createProductPricingCallback);
				} else {
					alert("Please fill up all the required fields!");
				}
			});
			
			$("#addOrderItemBySizeModalForm-create-btn").click(function(){
				var index = $("#addOrderItemBySizeModalForm_index").val();
				if ($.isNumeric(index)){
					//modify
					var productCode = $("#addOrderItemBySizeModalForm_productCode").val();
					var qtyBySize = $("#addOrderItemBySizeModalForm_qtyBySize").val();					
					var qtyBySizeJSON = $.parseJSON(qtyBySize);		
					var qty = 0;
					$(".clonedRow").each(function(){
						var clonedSize = $(this).find("#addOrderItemBySizeModalForm_size").val();
						var clonedOrderQty = $(this).find("#addOrderItemBySizeModalForm_qty").val();
						$(qtyBySizeJSON).each(function(){
							if (this["size"] == clonedSize){
								this["orderQuantity"] = ($.isNumeric(clonedOrderQty) ? parseInt(clonedOrderQty) : 0);
								qty += ($.isNumeric(clonedOrderQty) ? parseInt(clonedOrderQty) : 0);
							}
						});						
					});
					
					// update main page details
					//$("#orderProduct-" + index + "-code").val(productCode);// does not changed
					$("#orderProduct-" + index + "-orderQty").val(qty);
					$("#orderProduct-" + index + "-qtyBySize").val(JSON.stringify(qtyBySizeJSON));
					
					// hide modal
					$("#addOrderItemBySizeModalForm").modal("hide");
				} else {
					//new					
					var productCode = $("#addOrderItemBySizeModalForm_productCode").val();
					var totalQty = 0;
					var qtyBySizeJSON = [];
					$(".clonedRow").each(function(){
						var size = $(this).find("#addOrderItemBySizeModalForm_size").val();
						var qty = $(this).find("#addOrderItemBySizeModalForm_qty").val();
						totalQty += ($.isNumeric(qty) ? parseInt(qty) : 0);
						var temp = {
								"id":"null",
								"size": size,
								"orderQuantity": ($.isNumeric(qty) ? parseInt(qty) : 0)
						}
						qtyBySizeJSON.push(temp);
					});
					
					// set values for dynamic creation of row in main page
					$("#orderProduct-code").val(productCode);
					$("#orderProduct-orderQty").val(totalQty);
					$("#orderProduct-qtyBySize").val(JSON.stringify(qtyBySizeJSON));
					
					// dynamically create row
					displayExistingSalesOrderItem();	
					
					//hide modal
					$("#addOrderItemBySizeModalForm").modal("hide");
				}
			});
			
		} catch (err){
	       $.jGrowl(err);
		}
	});
	
	function resetSalesOrderItemForm(){
		$("#orderProduct-code").val("");
		$("#orderProduct-unit-price").val("0.00");
		$("#orderProduct-orderQty").val("1");		
	}
	
	var customerProductPriceCallback = function(result, customerId, productCode){	
        if (result != ''){
        	$("#orderProduct-unit-price").val(result);
        } else {
        	$("#unitPriceModalForm_productCode_span").text(productCode);
        	$("#unitPriceModalForm_productCode").val(productCode);
        	$("#unitPriceModalForm_customerId").val(customerId);
        	$("#unitPriceModalForm").modal("show");	
        }
	}
	
	function getCustomerProductUnitPrice_ajax(url, customerId, productCode, callback){
		var result = "";
		$.ajax({
			type: "get",
	        async: true,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			dataType: "text",
			success: function(response){
					callback(response, customerId, productCode);
			},
			error: function(data, status, err){
				$.jGrowl("getCustomerProductPrice_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;
	}	
	
	function setCustomerProductUnitPrice_ajax(url, customerId, productCode, productPrice){
		$("#unitPriceModalForm_title").text("Setting price, please wait...");
		var result = "";
		$.ajax({
			type: "get",
	        async: false,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			dataType: "text",
			success: function(response){
					result = response;
			},
			error: function(data, status, err){
				$.jGrowl("setCustomerProductPrice_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		if (result){
			$("#orderProduct-unit-price").val(productPrice);
		}
		
		$("#unitPriceModalForm_title").text("Setting price, done!");
		$("#unitPriceModalForm").modal("hide");
		return result;
	}	
	
	var createCustomerCallback = function(result){
		$(".addCustomerModalFormInput").each(function(index){
			$(this).val("");
		});		
		
		$("#addCustomerModalForm").modal("hide");
		var id = result["id"];
		
		if ($.isNumeric(id)){
			$("#customerId").val(result["id"]);
			$("#customerFullName").val(result["fullName"]);
		}
		
		alert(result["message"]);
	}
	
	function createCustomer_ajax(url, customerJSON, callback){
		var result = "";
		$.ajax({
			type: "post",
	        async: true,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			data: JSON.stringify(customerJSON),
			dataType: "json",
			contentType: 'application/json',
		    mimeType: 'application/json',			
			success: function(response){
					callback(response);
			},
			error: function(data, status, err){
				$.jGrowl("createCustomer_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}
	
	
	var createProductPricingCallback = function(result){
		//alert(JSON.stringify(result));

		$(".addProductPricingModalFormInput").each(function(index){
			$(this).val("");
		});
		
		$("#addProductPricingModalForm").modal("hide");
		var id = result["id"];
		
		if ($.isNumeric(id)){
			$("#orderProduct-code").val(result["productCode"]);
			$("#orderProduct-unit-price").val(result["price"]);
		}
		
		alert(result["message"]);
	}
	
	function createProductPricing_ajax(url, productPricingJSON, callback){
		var result = "";
		$.ajax({
			type: "post",
	        async: true,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			data: JSON.stringify(productPricingJSON),
			dataType: "json",
			contentType: 'application/json',
		    mimeType: 'application/json',			
			success: function(response){
					callback(response);
			},
			error: function(data, status, err){
				$.jGrowl("createProductPricing_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}	
	
	
	function displayExistingSalesOrderItem(){
		//$.jGrowl("addOrderItem");
		var selectedProductCode = $("#orderProduct-code").val();
		var selectedProductQty = $("#orderProduct-orderQty").val();
		//alert(selectedProductCode);
		if (!$.isNumeric(selectedProductCode)) selectedProductCode = "";
		if (!$.isNumeric(selectedProductQty)) selectedProductQty = 0;
		if (selectedProductCode != '' && selectedProductQty > 0){
			if ($.inArray(selectedProductCode, addedProductCode) == -1){
				addedProductCode.push(selectedProductCode);
				var cloneRow = $("#orderProduct-row").clone();
				$(cloneRow).insertAfter($("#orderProduct-row"));
				
				// 1. product code
				$(cloneRow).find("#orderProduct-code").attr("readonly", true);
				$(cloneRow).find("#orderProduct-code").attr("name", "orderProduct-" + counter + "-code");
				$(cloneRow).find("#orderProduct-code").attr("id", "orderProduct-" + counter + "-code");
				
				// 2. product overall quantity
				$(cloneRow).find("#orderProduct-orderQty").attr("readonly", true);
				$(cloneRow).find("#orderProduct-orderQty").attr("name", "orderProduct-" + counter + "-orderQty");
				$(cloneRow).find("#orderProduct-orderQty").attr("id", "orderProduct-" + counter + "-orderQty");
				
				// 3. product qty by size JSON string holder
				$(cloneRow).find("#orderProduct-qtyBySize").attr("name", "orderProduct-" + counter + "-qtyBySize");
				$(cloneRow).find("#orderProduct-qtyBySize").attr("id", "orderProduct-" + counter + "-qtyBySize");	
				
				// 4. prepare edit button for added productionOrderItems
				$(cloneRow).find("#orderQtyBySize-edit-btn").attr("disabled", false);
				$(cloneRow).find("#orderQtyBySize-edit-btn").attr("data-index", counter);
				$(cloneRow).find("#orderQtyBySize-edit-btn").click(function(){
					var index = $(this).attr("data-index");
					var data = $.parseJSON($("#orderProduct-" + index + "-qtyBySize").val());
										
					$(data).each(function(index, element){
						var cloneRow = $("#orderItemBySizeRow").clone();
						$(cloneRow).removeClass("hidden");
						$(cloneRow).addClass("clonedRow");
						$(cloneRow).insertAfter($("#orderItemBySizeRow"));
						$(cloneRow).find("#addOrderItemBySizeModalForm_size").val(element["size"]);
						$(cloneRow).find("#addOrderItemBySizeModalForm_qty").val(element["orderQuantity"]);
					});
					
					//pass data
					$("#addOrderItemBySizeModalForm_qtyBySize").val(JSON.stringify(data));
					
					//pass index
					$("#addOrderItemBySizeModalForm_index").val(index);
					
					// pass product code
					$("#addOrderItemBySizeModalForm_productCode").val(selectedProductCode);
					
					// modify button
					$("#addOrderItemBySizeModalForm-create-btn").text("Modify");
					
					// show modal form
					$("#addOrderItemBySizeModalForm").modal("show");					
				});
				
				// 5. prepare removal button
				$(cloneRow).find("#orderProduct-btn").html("&emsp;-&emsp;");
				$(cloneRow).find("#orderProduct-btn").click(function(){
					addedProductCode.pop(selectedProductCode);
					$(cloneRow).remove();
				});
				
				// 6.prepare add button
				//$(cloneRow).find("#orderProduct-btn").attr("disabled", false);
				$(cloneRow).find("#orderProduct-btn").attr("name", "orderProduct-" + counter + "-btn");
				$(cloneRow).find("#orderProduct-btn").attr("id", "orderProduct-" + counter + "-btn");

				
				// 7. 
				$(cloneRow).attr("name", "orderProduct-" + counter + "-row");
				$(cloneRow).attr("id", "orderProduct-" + counter + "-row");
				
				// 8.
				$(cloneRow).find("#addProductCode-btn").addClass("hidden");
				
				counter++;
			} else {
				$.jGrowl("Already added!");
			}
		} else {
			if (selectedProductCode == '') {
				//$.jGrowl("Please enter valid product code!");
				showPopoverAlert("#orderProduct-code", "Invalid", "Please enter valid product code!", "bottom", 1000);
				$("#orderProduct-code").focus();
			}
			if (selectedProductQty < 1) {
				//$.jGrowl("Please enter valid product quantity!");
				showPopoverAlert("#orderProduct-orderQty", "Invalid", "Please specify valid product quantity!", "bottom", 1000);
				$("#orderProduct-orderQty").focus();
			}					
		}
	}
	
	var getProductSizeSetCallback = function(result){
		var id = result["id"];
	}
	
	function getProductSizeSet_ajax(url){
		var result = {};
		$.ajax({
			type: "get",
	        async: false,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			dataType: "text",			
			success: function(response){
					result = $.parseJSON(response);
			},
			error: function(data, status, err){
				$.jGrowl("getProductSizeSet_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;			
	}
	
	function showPopoverAlert(elementId, title, message, placement, delay){
		$(elementId).on('shown.bs.popover', function(){
			setTimeout(function(){
				$(elementId).popover('destroy');
			}, delay);
		});	
		// data-toggle="popover" data-content="Help!" data-placement="bottom" title="Required" 		
		$(elementId).attr("data-toggle", "popover");
		$(elementId).attr("data-content", message);
		$(elementId).attr("data-title", title);
		$(elementId).attr("data-placement", placement);
		$(elementId).popover('show');
	}
</script>