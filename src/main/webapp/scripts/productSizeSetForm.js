<script type="text/javascript">
	var counter = 1;
	var addedProductSizeCode = [];
	$(document).ready(function(){
		$('#productSize-id').autocomplete({
		    //lookup: customers,
			serviceUrl: '/services/api/productSizes/suggest.json',
			transformResult: function(response) {
				var realArray = $.parseJSON($.makeArray(response));
		        return {
		            suggestions: $.map(realArray, function(dataItem) {
		                return { value: dataItem.label, data: dataItem.value };
		            })
		        };
		    },				
		    onSelect: function (suggestion) {
		        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
		    	$("#productSize-id").val(suggestion.data);
		    	//var temp = suggestion.value.split("|");
		    	//$("#customerFullName").val(temp[1].trim());			    	
		    },
		    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
		    	$.jGrowl(errorThrown);
		    }
		});	

		$("#productSize-btn").click(function(){
			//$.jGrowl("addOrderItem");			
			var selectedProductSizeCode = $("#productSize-id").val().trim();
			
			if (selectedProductSizeCode != ''){
				var productSizeCodeExists = isProductSizeExists(selectedProductSizeCode);
				if (productSizeCodeExists){
					if ($.inArray(selectedProductSizeCode, addedProductSizeCode) == -1){
						addedProductSizeCode.push(selectedProductSizeCode);
						var cloneRow = $("#productSize-row").clone();
						$(cloneRow).insertAfter($("#productSize-row"));
						
						// 1.
						$(cloneRow).find("#productSize-id").attr("readonly", true);
						$(cloneRow).find("#productSize-id").attr("name", "productSize-" + counter + "-id");
						$(cloneRow).find("#productSize-id").attr("id", "productSize-" + counter + "-id");
						
						
						// 4.
						$(cloneRow).find("#productSize-btn").html("&emsp;-&emsp;");
						$(cloneRow).find("#productSize-btn").click(function(){
							addedProductSizeCode.pop(selectedProductSizeCode);
							$(cloneRow).remove();
						});
						$(cloneRow).find("#productSize-btn").attr("name", "productSize-" + counter + "-btn");
						$(cloneRow).find("#productSize-btn").attr("id", "productSize-" + counter + "-btn");
						
						// 5.
						$(cloneRow).attr("name", "productSize-" + counter + "-row");
						$(cloneRow).attr("id", "productSize-" + counter + "-row");
						
						// 6.
						$(cloneRow).find("#addProductSize-btn").addClass("hidden");
						
						counter++;
					} else {
						//$.jGrowl("Already added!");
						alert("Product with product size code '" + selectedProductSizeCode + "' has already been added. Please choose different code.");
					}
				} else {
					var code = $("#productSize-id").val();
					$.jGrowl("'" + code + "' could not be found. Please choose from existing product size codes!");
					$("#productSize-id").focus();				
				}
			} else {
				if (selectedProductSizeCode == '') {
					$.jGrowl("Please enter valid product size code!");
					$("#productSize-id").focus();
				}	
			}
		});		
		
		if ($("#productSizesJSON").length > 0){
			var productSizesJSON = $.parseJSON($("#productSizesJSON").text());
			$(productSizesJSON["productSizes"]).each(function(index, element){
				$("#productSize-id").val(element['id']);
				$("#productSize-btn").click();
			});	
			
			resetProductSizeForm();
		}
		
		$("#addProductSize-btn").click(function(){
			$("#addProductSizeModalForm").modal("show");
		});		
		
		$("#addProductSizeModalForm-create-btn").click(function(){
			var isAllFieldsEntered = true;
			$(".addProductSizeModalFormInput").each(function(index){
				if ($(this).val() == ''){					
					isAllFieldsEntered = false;
					return false;
				}
			});	
			
			if (isAllFieldsEntered){
				var url = "${ctx}/services/api/productSizes/create";
				var productSizeJSON = {};
	
				productSizeJSON["id"] = $("#addProductSizeModalForm_id").val();
				productSizeJSON["description"] = $("#addProductSizeModalForm_description").val();
		
				createProductSize_ajax(url, productSizeJSON, createProductSizeCallback);
			} else {
				alert("Please fill up all the required fields!");
			}
		});			
	});
	
	function resetProductSizeForm(){
		$("#productSize-id").val("");
	}
	
	var createProductSizeCallback = function(result){
		$(".addProductSizeModalFormInput").each(function(index){
			$(this).val("");
		});		
		
		$("#addProductSizeModalForm").modal("hide");
		var id = result["id"];

		$("#addProductSizeModalForm_id").val(result["id"]);
		
		alert(result["message"]);
	}
	
	function createProductSize_ajax(url, productSizeJSON, callback){
		var result = "";
		$.ajax({
			type: "post",
	        async: true,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			data: JSON.stringify(productSizeJSON),
			dataType: "json",
			contentType: 'application/json',
		    mimeType: 'application/json',			
			success: function(response){
					callback(response);
			},
			error: function(data, status, err){
				$.jGrowl("createProductSize_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}		
	
	function isProductSizeExists(productSizeCode){
		var result = getProductSize_ajax(productSizeCode);

		return result["id"] != null;

	}
	
	function getProductSize_ajax(productSizeCode){
		var result = {};
		$.ajax({
			type: "get",
	        async: false,
	        cache: false,
	        timeout: 30000, 		
			url: "${ctx}/services/api/productSizes/" + productSizeCode + ".json",		
			dataType: "text",
			success: function(response){
					result = $.parseJSON(response);
			},
			error: function(data, status, err){
				$.jGrowl("getProductSize_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}	
</script>