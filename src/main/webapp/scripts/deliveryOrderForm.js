<script type="text/javascript">
$(document).ready(function(){
	try {
		$('#orderDate').datepicker();
		
		$('#customerFullName').autocomplete({
		    //lookup: customers,
			serviceUrl: '/services/api/customers/suggest.json',
			transformResult: function(response) {
				var realArray = $.parseJSON($.makeArray(response));
		        return {
		            suggestions: $.map(realArray, function(dataItem) {
		                return { value: dataItem.label, data: dataItem.value };
		            })
		        };
		    },				
		    onSelect: function (suggestion) {
		        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
		    	var customerId = suggestion.data;
		    	$("#customerId").val(customerId);
		    	//split
		    	var temp = suggestion.value.split("|");
		    	$("#customerFullName").val(temp[1].trim());	
		    	
		    	// fetch confirmed sales order by customer id
		    	//$("#salesOrder").empty();
		    	//$("#salesOrder").load("/manage/confirmedSalesOrders?customerId=" + customerId);
		    	
		    	// fetch and populate delivery address if any
		    	var url = "${ctx}/services/api/customers/" +  customerId + "/deliveryAddress.json"; 
		    	fetchAddress_ajax(url, populateDeliveryAddressCallback);
		    	
		    	// fetch and populate billing address if any
		    	var url = "${ctx}/services/api/customers/" +  customerId + "/billingAddress.json"; 
		    	fetchAddress_ajax(url, populateBillingAddressCallback);
		    },
		    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
		    	$.jGrowl(errorThrown);
		    }
		});		
		
		$("#acceptOrder-btn").click(function(){
			var yes = confirm("Are sure to accept this order?");
			if (yes){
				$("#status").val("A");
				$("#deliveryOrderForm").submit();
			}
		});

		$("#confirmOrder-btn").click(function(){
			var yes = confirm("Are sure to confirm this order?");
			if (yes){
				$("#status").val("C");
				$("#deliveryOrderForm").submit();
			}
		});
		
		$("#completeOrder-btn").click(function(){
			var yes = confirm("Are sure to set this order as completed?");
			if (yes){
				$("#status").val("E");
				$("#deliveryOrderForm").submit();
			}
		});
		
		$("#rejectOrder-btn").click(function(){
			var yes = confirm("Are sure to reject this order?");
			if (yes){
				$("#status").val("R");
				$("#deliveryOrderForm").submit();
			}
		});

		$("#addCustomer-btn").click(function(){
			$("#addCustomerModalForm").modal("show");
		});			
		
		$("#addCustomerModalForm-create-btn").click(function(){
			var isAllFieldsEntered = true;
			$(".addCustomerModalFormInput").each(function(index){
				if ($(this).val() == ''){					
					isAllFieldsEntered = false;
					return false;
				}
			});	
			
			if (isAllFieldsEntered){			
				var url = "${ctx}/services/api/customers/create";
				var customerJSON = {};

				customerJSON["fullName"] = $("#addCustomerModalForm_customerName").val();
				customerJSON["legalIdentity"] = $("#addCustomerModalForm_customerLegalId").val();
				customerJSON["firstName"] = $("#addCustomerModalForm_contactFirstName").val();
				customerJSON["lastName"] = $("#addCustomerModalForm_contactLastName").val();
		
				createCustomer_ajax(url, customerJSON, createCustomerCallback);
			} else {
				alert("Please fill up all the required fields!");
			}			
		});
		
	} catch (err){
       $.jGrowl(err);
	}
});	

var populateDeliveryAddressCallback  = function(result){
	$("#deliveryAddress").val(result["address"]);
	$("#deliveryStreet").val(result["street"]);
	$("#deliveryCity").val(result["city"]);
	$("#deliveryPostalCode").val(result["postalCode"]);
}

var populateBillingAddressCallback  = function(result){
	$("#billingAddress").val(result["address"]);
	$("#billingStreet").val(result["street"]);
	$("#billingCity").val(result["city"]);
	$("#billingPostalCode").val(result["postalCode"]);
}

function fetchAddress_ajax(url, callback){
	var result = "";
	$.ajax({
		type: "get",
        async: true,
        cache: false,
        timeout: 30000, 		
		url: url,
		dataType: "text",
		success: function(response){
				result = $.parseJSON(response);
				callback(result);
		},
		error: function(data, status, err){
			$.jGrowl("fetchAddress_ajax():error while request...");
			$.jGrowl("data:" + data);
			$.jGrowl("status:" + status);
			$.jGrowl("err:" + err);    
			result = status;
			return true;
		}
	});    
	
	return result;
}

var createCustomerCallback = function(result){
	$(".addCustomerModalFormInput").each(function(index){
		$(this).val("");
	});		
	
	$("#addCustomerModalForm").modal("hide");
	var id = result["id"];
	
	if ($.isNumeric(id)){
		$("#customerId").val(result["id"]);
		$("#customerFullName").val(result["fullName"]);
	}
	
	alert(result["message"]);
}

function createCustomer_ajax(url, customerJSON, callback){
	var result = "";
	$.ajax({
		type: "post",
        async: true,
        cache: false,
        timeout: 30000, 		
		url: url,
		data: JSON.stringify(customerJSON),
		dataType: "json",
		contentType: 'application/json',
	    mimeType: 'application/json',			
		success: function(response){
				callback(response);
		},
		error: function(data, status, err){
			$.jGrowl("createCustomer_ajax():error while request...");
			$.jGrowl("data:" + data);
			$.jGrowl("status:" + status);
			$.jGrowl("err:" + err);    
			result = status;
			return true;
		}
	});    
	
	return result;		
}
</script>