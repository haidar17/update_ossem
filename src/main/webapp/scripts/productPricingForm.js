<script type="text/javascript">
$(document).ready(function(){
	$("#price").val($.isNumeric($("#price").val()) ? parseFloat($("#price").val()).toFixed(2): parseFloat("0").toFixed(2));
	
	$('#customerName').autocomplete({
	    //lookup: customers,
		serviceUrl: '/services/api/customers/suggest.json',
		transformResult: function(response) {
			var realArray = $.parseJSON($.makeArray(response));
	        return {
	            suggestions: $.map(realArray, function(dataItem) {
	                return { value: dataItem.label, data: dataItem.value };
	            })
	        };
	    },				
	    onSelect: function (suggestion) {
	        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
	    	$("#customerId").val(suggestion.data);
	    	var temp = suggestion.value.split("|");
	    	$("#customerName").val(temp[1].trim());			    	
	    },
	    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
	    	$.jGrowl(errorThrown);
	    }
	});		
	
	$('#productCode').autocomplete({
	    //lookup: customers,
		serviceUrl: '/services/api/products/suggest.json',
		transformResult: function(response) {
			var realArray = $.parseJSON($.makeArray(response));
	        return {
	            suggestions: $.map(realArray, function(dataItem) {
	                return { value: dataItem.label, data: dataItem.value };
	            })
	        };
	    },				
	    onSelect: function (suggestion) {
	        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
	    	$("#productId").val(suggestion.data);
	    	var temp = suggestion.value.split("|");
	    	$("#productCode").val(temp[0].trim());
	    	$("#productName").val(temp[1].trim());
	    },
	    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
	    	$.jGrowl(errorThrown);
	    }
	});	

	$("#addCustomer-btn").click(function(){
		$("#addCustomerModalForm").modal("show");
	});	
	
	$("#addCustomerModalForm-create-btn").click(function(){
		var isAllFieldsEntered = true;
		$(".addCustomerModalFormInput").each(function(index){
			if ($(this).val() == ''){					
				isAllFieldsEntered = false;
				return false;
			}
		});	
		
		if (isAllFieldsEntered){
			var url = "${ctx}/services/api/customers/create";
			var customerJSON = {};
			var contactPersonJSON = {};
			customerJSON["fullName"] = $("#addCustomerModalForm_customerName").val();
			customerJSON["legalIdentity"] = $("#addCustomerModalForm_customerLegalId").val();
			customerJSON["firstName"] = $("#addCustomerModalForm_contactFirstName").val();
			customerJSON["lastName"] = $("#addCustomerModalForm_contactLastName").val();
	
			createCustomer_ajax(url, customerJSON, createCustomerCallback);
		} else {
			alert("Please fill up all the required fields!");
		}
	});	
	
	$("#addProductCode-btn").click(function(){
		$("#addProductModalForm").modal("show");				
	});
	
	$("#addProductModalForm-create-btn").click(function(){
		var isAllFieldsEntered = true;
		$(".addProductModalFormInput").each(function(index){
			if ($(this).val() == ''){					
				isAllFieldsEntered = false;
				return false;
			}
		});					
		
		if (isAllFieldsEntered){
			var url = "${ctx}/services/api/products/create";
			var productJSON = {};
			productJSON["productName"] = $("#addProductModalForm_productName").val();
			productJSON["productCode"] = $("#addProductModalForm_productCode").val();
	
			createProduct_ajax(url, productJSON, createProductCallback);
		} else {
			alert("Please fill up all the required fields!");
		}					
	});	
	
	//$("#confirm_modal_form").modal('show');
	var method = "${param.method}";
	if (method == "New" || method == "Edit") $("#confirm_modal_form").modal('show');
});

	
	var createProductCallback = function(result){
		//alert(JSON.stringify(result));
	
		$(".addProductModalFormInput").each(function(index){
			$(this).val("");
		});
		
		$("#addProductModalForm").modal("hide");
		var id = result["id"];
		
		if ($.isNumeric(id)){
			$("#productCode").val(result["productCode"]);
		}
		
		alert(result["message"]);
	}
	
	function createProduct_ajax(url, productJSON, callback){
		var result = "";
		$.ajax({
			type: "post",
	        async: true,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			data: JSON.stringify(productJSON),
			dataType: "json",
			contentType: 'application/json',
		    mimeType: 'application/json',			
			success: function(response){
					callback(response);
			},
			error: function(data, status, err){
				$.jGrowl("createProduct_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}	
	
	var createCustomerCallback = function(result){
		$(".addCustomerModalFormInput").each(function(index){
			$(this).val("");
		});		
		
		$("#addCustomerModalForm").modal("hide");
		var id = result["id"];
		
		if ($.isNumeric(id)){
			$("#customerId").val(result["id"]);
			$("#customerName").val(result["fullName"]);
		}
		
		alert(result["message"]);
	}
	
	function createCustomer_ajax(url, customerJSON, callback){
		var result = "";
		$.ajax({
			type: "post",
	        async: true,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			data: JSON.stringify(customerJSON),
			dataType: "json",
			contentType: 'application/json',
		    mimeType: 'application/json',			
			success: function(response){
					callback(response);
			},
			error: function(data, status, err){
				$.jGrowl("createCustomer_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}	
</script>