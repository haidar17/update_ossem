<script type="text/javascript">
	var counter = 1;
	var addedProductComponentCode = [];
	$(document).ready(function(){
		$('#productComponent-id').autocomplete({
		    //lookup: customers,
			serviceUrl: '/services/api/productComponents/suggest.json',
			transformResult: function(response) {
				var realArray = $.parseJSON($.makeArray(response));
		        return {
		            suggestions: $.map(realArray, function(dataItem) {
		                return { value: dataItem.label, data: dataItem.value };
		            })
		        };
		    },				
		    onSelect: function (suggestion) {
		        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
		    	$("#productComponent-id").val(suggestion.data);
		    	//var temp = suggestion.value.split("|");
		    	//$("#customerFullName").val(temp[1].trim());			    	
		    },
		    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
		    	$.jGrowl(errorThrown);
		    }
		});	

		$("#productComponent-btn").click(function(){
			//$.jGrowl("addOrderItem");			
			var selectedProductComponentCode = $("#productComponent-id").val().trim();
			
			if (selectedProductComponentCode != ''){
				var productComponentCodeExists = isProductComponentExists(selectedProductComponentCode);
				if (productComponentCodeExists){
					if ($.inArray(selectedProductComponentCode, addedProductComponentCode) == -1){
						addedProductComponentCode.push(selectedProductComponentCode);
						var cloneRow = $("#productComponent-row").clone();
						$(cloneRow).insertAfter($("#productComponent-row"));
						
						// 1.
						$(cloneRow).find("#productComponent-id").attr("readonly", true);
						$(cloneRow).find("#productComponent-id").attr("name", "productComponent-" + counter + "-id");
						$(cloneRow).find("#productComponent-id").attr("id", "productComponent-" + counter + "-id");
						
						
						// 4.
						$(cloneRow).find("#productComponent-btn").html("&emsp;-&emsp;");
						$(cloneRow).find("#productComponent-btn").click(function(){
							addedProductComponentCode.pop(selectedProductComponentCode);
							$(cloneRow).remove();
						});
						$(cloneRow).find("#productComponent-btn").attr("name", "productComponent-" + counter + "-btn");
						$(cloneRow).find("#productComponent-btn").attr("id", "productComponent-" + counter + "-btn");
						
						// 5.
						$(cloneRow).attr("name", "productComponent-" + counter + "-row");
						$(cloneRow).attr("id", "productComponent-" + counter + "-row");
						
						// 6.
						$(cloneRow).find("#addProductComponent-btn").addClass("hidden");
						
						counter++;
					} else {
						//$.jGrowl("Already added!");
						alert("Product with product component code '" + selectedProductComponentCode + "' has already been added. Please choose different code.");
					}
				} else {
					var code = $("#productComponent-id").val();
					$.jGrowl("'" + code + "' could not be found. Please choose from existing product component codes!");
					$("#productComponent-id").focus();				
				}
			} else {
				if (selectedProductComponentCode == '') {
					$.jGrowl("Please enter valid product component code!");
					$("#productComponent-id").focus();
				}	
			}
		});		
		
		if ($("#productComponentsJSON").length > 0){
			var productComponentsJSON = $.parseJSON($("#productComponentsJSON").text());
			$(productComponentsJSON["productComponents"]).each(function(index, element){
				$("#productComponent-id").val(element['id']);
				$("#productComponent-btn").click();
			});	
			
			resetProductComponentForm();
		}
		
		$("#addProductComponent-btn").click(function(){
			$("#addProductComponentModalForm").modal("show");
		});		
		
		$("#addProductComponentModalForm-create-btn").click(function(){
			var isAllFieldsEntered = true;
			$(".addProductComponentModalFormInput").each(function(index){
				if ($(this).val() == ''){					
					isAllFieldsEntered = false;
					return false;
				}
			});	
			
			if (isAllFieldsEntered){
				var url = "${ctx}/services/api/productComponents/create";
				var productComponentJSON = {};
	
				productComponentJSON["id"] = $("#addProductComponentModalForm_id").val();
				productComponentJSON["description"] = $("#addProductComponentModalForm_description").val();
		
				createProductComponent_ajax(url, productComponentJSON, createProductComponentCallback);
			} else {
				alert("Please fill up all the required fields!");
			}
		});			
	});
	
	function resetProductComponentForm(){
		$("#productComponent-id").val("");
	}
	
	var createProductComponentCallback = function(result){
		$(".addProductComponentModalFormInput").each(function(index){
			$(this).val("");
		});		
		
		$("#addProductComponentModalForm").modal("hide");
		var id = result["id"];

		$("#addProductComponentModalForm_id").val(result["id"]);
		
		alert(result["message"]);
	}
	
	function createProductComponent_ajax(url, productComponentJSON, callback){
		var result = "";
		$.ajax({
			type: "post",
	        async: true,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			data: JSON.stringify(productComponentJSON),
			dataType: "json",
			contentType: 'application/json',
		    mimeType: 'application/json',			
			success: function(response){
					callback(response);
			},
			error: function(data, status, err){
				$.jGrowl("createProductComponent_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}		
	
	function isProductComponentExists(productComponentCode){
		var result = getProductComponent_ajax(productComponentCode);

		return result["id"] != null;

	}
	
	function getProductComponent_ajax(productComponentCode){
		var result = {};
		$.ajax({
			type: "get",
	        async: false,
	        cache: false,
	        timeout: 30000, 		
			url: "${ctx}/services/api/productComponents/" + productComponentCode + ".json",		
			dataType: "text",
			success: function(response){
					result = $.parseJSON(response);
			},
			error: function(data, status, err){
				$.jGrowl("getProductComponent_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}	
</script>