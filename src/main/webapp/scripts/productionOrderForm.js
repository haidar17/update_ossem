<script type="text/javascript">
	var counter = 1;
	var counter2 = 1;
	var addedProductCode = [];
	
	$(document).ready(function(){
		try {
			$('#orderDate').datepicker();
			$('#adjustment-date').datepicker();
			
			$('#orderProduct-code').autocomplete({
			    //lookup: customers,
				serviceUrl: '/services/api/products/suggest.json',
				transformResult: function(response) {
					var realArray = $.parseJSON($.makeArray(response));
			        return {
			            suggestions: $.map(realArray, function(dataItem) {
			                return { value: dataItem.label, data: dataItem.value };
			            })
			        };
			    },				
			    onSelect: function (suggestion) {
			        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
			    	var temp = suggestion.value.split("|");
			    	$("#orderProduct-code").val(temp[0].trim());
			    },
			    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
			    	$.jGrowl(errorThrown);
			    }
			});

			// adjustment
			$('#adjustment-code').autocomplete({
			    //lookup: customers,
				serviceUrl: '/services/api/productionOrders/adjustmentCodes.json',
				transformResult: function(response) {
					var realArray = $.parseJSON($.makeArray(response));
			        return {
			            suggestions: $.map(realArray, function(dataItem) {
			                return { value: dataItem.label, data: dataItem.value };
			            })
			        };
			    },				
			    onSelect: function (suggestion) {
			        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
			    	$("#adjustment-code").val(suggestion.data);	
			    	$("#adjustment-reference").val(suggestion.value);
			    },
			    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
			    	$.jGrowl(errorThrown);
			    }
			});
			
			
			// this to remove all cloned row in addOrderItemBySizeModalForm
			$("#addOrderItemBySizeModalForm").on("hide.bs.modal", function(event){
		    	$(".clonedRow").each(function(){
		    		$(this).remove();
		    	});
		    	
		    	$(".addOrderItemBySizeModalFormInput").each(function(){
		    		$(this).val("");
		    	});
		    });
			
			$("#orderProduct-btn").click(function(){
				var selectedProductCode = $("#orderProduct-code").val();
				
				if ($.isNumeric(selectedProductCode)){
					var result = getProductSizeSet_ajax("${ctx}/services/api/products/" + selectedProductCode + "/productSizeSet.json");
					if ($.isNumeric(result["id"])){
						$(result["productSizes"]).each(function(index, element){
							var cloneRow = $("#orderItemBySizeRow").clone();
							$(cloneRow).removeClass("hidden");
							$(cloneRow).addClass("clonedRow");
							$(cloneRow).insertAfter($("#orderItemBySizeRow"));
							$(cloneRow).find("#addOrderItemBySizeModalForm_size").val(element["id"]);
							$(cloneRow).find("#addOrderItemBySizeModalForm_qty").val("0");
						});
						
						// set code
						$("#addOrderItemBySizeModalForm_productCode").val(selectedProductCode);
						
						// change button text
						$("#addOrderItemBySizeModalForm-create-btn").text("Add");
						
						// show modal form
						$("#addOrderItemBySizeModalForm").modal("show");
					} else {
						//alert("Product Size for product code '" + selectedProductCode + "' could not be found!");
						var message = "Product Size for product code '" + selectedProductCode + "' could not be found!";
						showPopoverAlert("#orderProduct-code", "Invalid!", message, "bottom", 2000);
						$("#orderProduct-code").focus();
					}
				} else {
					//alert("Please choose specify valid product code!");
					showPopoverAlert("#orderProduct-code", "Invalid!", "Please specify valid product code!", "bottom", 2000);
					$("#orderProduct-code").focus();
				}
			});
			// order items

			if ($("#productionOrderItemsJSON").length > 0){
				var productionOrderItemsJSON = $.parseJSON($("#productionOrderItemsJSON").text());
				$(productionOrderItemsJSON["productionOrderItems"]).each(function(index, element){
					$("#orderProduct-code").val(element['productCode']);
					$("#orderProduct-qty").val(element['quantity']);
					$("#orderProduct-qtyBySize").val(JSON.stringify(element["productionOrderQuantityBySizes"]));
					//$("#orderProduct-btn").click();
					displayExistingProductionOrderItem();
				});	
				
				resetProductionOrderItemForm();
			}		
			
			// adjustment
			$("#adjustment-btn").click(function(){
				//$.jGrowl("adjustments");
				var selectedAdjustmentDate = $("#adjustment-date").val();
				var selectedAdjustmentQty = $("#adjustment-qty").val();
				var validAdjustmentQty = $.isNumeric(selectedAdjustmentQty);
				//if (!$.isNumeric(selectedProductCode)) selectedProductCode = "";
				if (selectedAdjustmentDate != '' && validAdjustmentQty){
					var cloneRow = $("#adjustment-row").clone();
					$(cloneRow).insertAfter($("#adjustment-row"));	
					// 1.
					$(cloneRow).find("#adjustment-code").attr("readonly", true);
					$(cloneRow).find("#adjustment-code").attr("name", "adjustment-" + counter2 + "-code");
					$(cloneRow).find("#adjustment-code").attr("id", "adjustment-" + counter2 + "-code");
					
					// 1.
					$(cloneRow).find("#adjustment-date").attr("readonly", true);
					$(cloneRow).find("#adjustment-date").attr("name", "adjustment-" + counter2 + "-date");
					$(cloneRow).find("#adjustment-date").attr("id", "adjustment-" + counter2 + "-date");
					
					// 2.
					$(cloneRow).find("#adjustment-reference").attr("readonly", true);
					$(cloneRow).find("#adjustment-reference").attr("name", "adjustment-" + counter2 + "-reference");
					$(cloneRow).find("#adjustment-reference").attr("id", "adjustment-" + counter2 + "-reference");
					
					// 3.
					$(cloneRow).find("#adjustment-qty").attr("readonly", true);
					$(cloneRow).find("#adjustment-qty").attr("name", "adjustment-" + counter2 + "-qty");
					$(cloneRow).find("#adjustment-qty").attr("id", "adjustment-" + counter2 + "-qty");
					
					// 3.
					$(cloneRow).find("#adjustment-btn").html("&emsp;-&emsp;");
					$(cloneRow).find("#adjustment-btn").click(function(){
						$(cloneRow).remove();
					});
					$(cloneRow).find("#adjustment-btn").attr("name", "adjustment-" + counter2 + "-btn");
					$(cloneRow).find("#adjustment-btn").attr("id", "adjustment-" + counter2 + "-btn");
					
					// 5.
					$(cloneRow).attr("name", "adjustment-" + counter2 + "-row");
					$(cloneRow).attr("id", "adjustment-" + counter2 + "-row");
					
					counter2++;
				} else {
					if (selectedAdjustmentDate == '') {
						$.jGrowl("Please enter valid adjustment date!");
						$("#adjustment-date").focus();
					}
					if (!validAdjustmentQty) {
						$.jGrowl("Please enter valid adjustment quantity!");
						$("#adjustment-qty").focus();
					}					
				}
			});				
			
			if ($("#adjustmentsJSON").length > 0){
				var adjustmentsJSON = $.parseJSON($("#adjustmentsJSON").text());
				$(adjustmentsJSON["adjustments"]).each(function(index, element){
					$("#adjustment-code").val(element['adjustmentCode']);
					$("#adjustment-date").val($.format.date(element['adjustmentDate'], "MM/dd/yyyy"));
					$("#adjustment-note").val(element['adjustmentNote']);
					$("#adjustment-reference").val(element['adjustmentReference']);
					$("#adjustment-qty").val(element['adjustmentQty']);
					$("#adjustment-btn").click();
				});	
				
				resetAdjustmentForm();
			}
			
		
			
			$("#acceptOrder-btn").click(function(){
				var yes = confirm("Are sure to accept this order?");
				if (yes){
					$("#status").val("A");
					$("#productionOrderForm").submit();
				}
			});
			
			$("#confirmOrder-btn").click(function(){
				var yes = confirm("Are sure to confirm this order?");
				if (yes){
					$("#status").val("C");
					$("#productionOrderForm").submit();
				}
			});

			$("#completeOrder-btn").click(function(){
				var yes = confirm("Are sure to sett this order as completed?");
				if (yes){
					$("#status").val("E");
					$("#productionOrderForm").submit();
				}
			});		
			
			
			$("#rejectOrder-btn").click(function(){
				var yes = confirm("Are sure to reject this order?");
				if (yes){
					$("#status").val("R");
					$("#productionOrderForm").submit();
				}
			});	
			
			$("#addProductCode-btn").click(function(){
				$("#addProductModalForm").modal("show");				
			});
			
			$("#addProductModalForm-create-btn").click(function(){
				var isAllFieldsEntered = true;
				$(".addProductModalFormInput").each(function(index){
					if ($(this).val() == ''){					
						isAllFieldsEntered = false;
						return false;
					}
				});					
				
				if (isAllFieldsEntered){
					var url = "${ctx}/services/api/products/create";
					var productJSON = {};
					productJSON["productName"] = $("#addProductModalForm_productName").val();
					productJSON["productCode"] = $("#addProductModalForm_productCode").val();
			
					createProduct_ajax(url, productJSON, createProductCallback);
				} else {
					alert("Please fill up all the required fields!");
				}					
			});			
				
			
			$("#addOrderItemBySizeModalForm-create-btn").click(function(){
				var index = $("#addOrderItemBySizeModalForm_index").val();
				if ($.isNumeric(index)){
					//modify
					var productCode = $("#addOrderItemBySizeModalForm_productCode").val();
					var qtyBySize = $("#addOrderItemBySizeModalForm_qtyBySize").val();					
					var qtyBySizeJSON = $.parseJSON(qtyBySize);		
					var qty = 0;
					$(".clonedRow").each(function(){
						var clonedSize = $(this).find("#addOrderItemBySizeModalForm_size").val();
						var clonedOrderQty = $(this).find("#addOrderItemBySizeModalForm_qty").val();
						$(qtyBySizeJSON).each(function(){
							if (this["size"] == clonedSize){
								this["orderQuantity"] = ($.isNumeric(clonedOrderQty) ? parseInt(clonedOrderQty) : 0);
								qty += ($.isNumeric(clonedOrderQty) ? parseInt(clonedOrderQty) : 0);
							}
						});						
					});
					
					// update main page details
					//$("#orderProduct-" + index + "-code").val(productCode);// does not changed
					$("#orderProduct-" + index + "-qty").val(qty);
					$("#orderProduct-" + index + "-qtyBySize").val(JSON.stringify(qtyBySizeJSON));
					
					// hide modal
					$("#addOrderItemBySizeModalForm").modal("hide");
				} else {
					//new					
					var productCode = $("#addOrderItemBySizeModalForm_productCode").val();
					var totalQty = 0;
					var qtyBySizeJSON = [];
					$(".clonedRow").each(function(){
						var size = $(this).find("#addOrderItemBySizeModalForm_size").val();
						var qty = $(this).find("#addOrderItemBySizeModalForm_qty").val();
						totalQty += ($.isNumeric(qty) ? parseInt(qty) : 0);
						var temp = {
								"id":"null",
								"size": size,
								"orderQuantity": ($.isNumeric(qty) ? parseInt(qty) : 0)
						}
						qtyBySizeJSON.push(temp);
					});
					
					// set values for dynamic creation of row in main page
					$("#orderProduct-code").val(productCode);
					$("#orderProduct-qty").val(totalQty);
					$("#orderProduct-qtyBySize").val(JSON.stringify(qtyBySizeJSON));
					
					// dynamically create row
					displayExistingProductionOrderItem();	
					
					//hide modal
					$("#addOrderItemBySizeModalForm").modal("hide");
				}
			});
			
		} catch (err){
	       $.jGrowl(err);
		}
	});	
	
	function displayExistingProductionOrderItem(){
		//$.jGrowl("addOrderItem");
		var selectedProductCode = $("#orderProduct-code").val();
		var selectedProductQty = $("#orderProduct-qty").val();
		if (!$.isNumeric(selectedProductCode)) selectedProductCode = "";
		if (!$.isNumeric(selectedProductQty)) selectedProductQty = 0;
		if (selectedProductCode != '' && selectedProductQty > 0){
			if ($.inArray(selectedProductCode, addedProductCode) == -1){
				addedProductCode.push(selectedProductCode);
				var cloneRow = $("#orderProduct-row").clone();
				$(cloneRow).insertAfter($("#orderProduct-row"));
				
				// 1. product code
				$(cloneRow).find("#orderProduct-code").attr("readonly", true);
				$(cloneRow).find("#orderProduct-code").attr("name", "orderProduct-" + counter + "-code");
				$(cloneRow).find("#orderProduct-code").attr("id", "orderProduct-" + counter + "-code");
				
				// 2. product overall quantity
				$(cloneRow).find("#orderProduct-qty").attr("readonly", true);
				$(cloneRow).find("#orderProduct-qty").attr("name", "orderProduct-" + counter + "-qty");
				$(cloneRow).find("#orderProduct-qty").attr("id", "orderProduct-" + counter + "-qty");
				
				// 3. product qty by size JSON string holder
				$(cloneRow).find("#orderProduct-qtyBySize").attr("name", "orderProduct-" + counter + "-qtyBySize");
				$(cloneRow).find("#orderProduct-qtyBySize").attr("id", "orderProduct-" + counter + "-qtyBySize");	
				
				// 4. prepare edit button for added productionOrderItems
				$(cloneRow).find("#orderQtyBySize-edit-btn").attr("disabled", false);
				$(cloneRow).find("#orderQtyBySize-edit-btn").attr("data-index", counter);
				$(cloneRow).find("#orderQtyBySize-edit-btn").click(function(){
					var index = $(this).attr("data-index");
					var data = $.parseJSON($("#orderProduct-" + index + "-qtyBySize").val());
										
					$(data).each(function(index, element){
						var cloneRow = $("#orderItemBySizeRow").clone();
						$(cloneRow).removeClass("hidden");
						$(cloneRow).addClass("clonedRow");
						$(cloneRow).insertAfter($("#orderItemBySizeRow"));
						$(cloneRow).find("#addOrderItemBySizeModalForm_size").val(element["size"]);
						$(cloneRow).find("#addOrderItemBySizeModalForm_qty").val(element["orderQuantity"]);
					});
					
					//pass data
					$("#addOrderItemBySizeModalForm_qtyBySize").val(JSON.stringify(data));
					
					//pass index
					$("#addOrderItemBySizeModalForm_index").val(index);
					
					// pass product code
					$("#addOrderItemBySizeModalForm_productCode").val(selectedProductCode);
					
					// modify button
					$("#addOrderItemBySizeModalForm-create-btn").text("Modify");
					
					// show modal form
					$("#addOrderItemBySizeModalForm").modal("show");					
				});
				
				// 5. prepare removal button
				$(cloneRow).find("#orderProduct-btn").html("&emsp;-&emsp;");
				$(cloneRow).find("#orderProduct-btn").click(function(){
					addedProductCode.pop(selectedProductCode);
					$(cloneRow).remove();
				});
				
				// 6.prepare add button
				//$(cloneRow).find("#orderProduct-btn").attr("disabled", false);
				$(cloneRow).find("#orderProduct-btn").attr("name", "orderProduct-" + counter + "-btn");
				$(cloneRow).find("#orderProduct-btn").attr("id", "orderProduct-" + counter + "-btn");

				
				// 7. 
				$(cloneRow).attr("name", "orderProduct-" + counter + "-row");
				$(cloneRow).attr("id", "orderProduct-" + counter + "-row");
				
				// 8.
				$(cloneRow).find("#addProductCode-btn").addClass("hidden");
				
				counter++;
			} else {
				$.jGrowl("Already added!");
			}
		} else {
			if (selectedProductCode == '') {
				//$.jGrowl("Please enter valid product code!");
				showPopoverAlert("#orderProduct-code", "Invalid", "Please enter valid product code!", "bottom", 1000);
				$("#orderProduct-code").focus();
			}
			if (selectedProductQty < 1) {
				//$.jGrowl("Please enter valid product quantity!");
				showPopoverAlert("#orderProduct-qty", "Invalid", "Please specify valid product quantity!", "bottom", 1000);
				$("#orderProduct-qty").focus();
			}					
		}
	}
		
	
	function resetAdjustmentForm(){
		//reset
		$("#adjustment-code").val("");
		$("#adjustment-date").val("");
		$("#adjustment-note").val("");
		$("#adjustment-reference").val("");
		$("#adjustment-qty").val("1");			
	}
	
	function resetProductionOrderItemForm(){
		$("#orderProduct-code").val("");
		$("#orderProduct-qty").val("1");		
	}
	

	var createProductCallback = function(result){
		//alert(JSON.stringify(result));

		$(".addProductModalFormInput").each(function(index){
			$(this).val("");
		});
		
		$("#addProductModalForm").modal("hide");
		var id = result["id"];
		
		if ($.isNumeric(id)){
			$("#orderProduct-code").val(result["productCode"]);
		}
		
		alert(result["message"]);
	}
	
	function createProduct_ajax(url, productJSON, callback){
		var result = "";
		$.ajax({
			type: "post",
	        async: true,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			data: JSON.stringify(productJSON),
			dataType: "json",
			contentType: 'application/json',
		    mimeType: 'application/json',			
			success: function(response){
					callback(response);
			},
			error: function(data, status, err){
				$.jGrowl("createProduct_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;		
	}	
	
	var getProductSizeSetCallback = function(result){
		var id = result["id"];
	}
	
	function getProductSizeSet_ajax(url){
		var result = {};
		$.ajax({
			type: "get",
	        async: false,
	        cache: false,
	        timeout: 30000, 		
			url: url,
			dataType: "text",			
			success: function(response){
					result = $.parseJSON(response);
			},
			error: function(data, status, err){
				$.jGrowl("getProductSizeSet_ajax():error while request...");
				$.jGrowl("data:" + data);
				$.jGrowl("status:" + status);
				$.jGrowl("err:" + err);    
				result = status;
				return true;
			}
		});    
		
		return result;			
	}
	
	function showPopoverAlert(elementId, title, message, placement, delay){
		$(elementId).on('shown.bs.popover', function(){
			setTimeout(function(){
				$(elementId).popover('destroy');
			}, delay);
		});	
		// data-toggle="popover" data-content="Help!" data-placement="bottom" title="Required" 		
		$(elementId).attr("data-toggle", "popover");
		$(elementId).attr("data-content", message);
		$(elementId).attr("data-title", title);
		$(elementId).attr("data-placement", placement);
		$(elementId).popover('show');
	}
		
</script>