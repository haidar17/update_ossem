<script type="text/javascript">
	$(document).ready(function(){
		$("#currentCount").val("10");
		$("#lowerLimitCount").val("1");
		$("#upperLimitCount").val("99");
		
		$('#productCode').autocomplete({
		    //lookup: customers,
			serviceUrl: '/services/api/products/suggest.json',
			transformResult: function(response) {
				var realArray = $.parseJSON($.makeArray(response));
		        return {
		            suggestions: $.map(realArray, function(dataItem) {
		                return { value: dataItem.label, data: dataItem.value };
		            })
		        };
		    },				
		    onSelect: function (suggestion) {
		        //$.jGrowl('You selected: ' + suggestion.value + ', ' + suggestion.data);
		    	var temp = suggestion.value.split("|");
		    	$("#productCode").val(temp[0].trim());
		    	$("#productName").val(temp[1].trim());
		    },
		    onSearchError: function (query, jqXHR, textStatus, errorThrown) {
		    	$.jGrowl(errorThrown);
		    }
		});			
	});
</script>