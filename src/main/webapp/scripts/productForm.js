<script type="text/javascript">
	$(document).ready(function(){
		

		$("#productComponentSetId").chosen({allow_single_deselect:true});
		$("#productComponentSetId").trigger('chosen:updated');
		
		$("#productSizeSetId").chosen({allow_single_deselect:true});
		$("#productSizeSetId").trigger('chosen:updated');
		
				
		$("#productComponentSet").removeClass("active").removeClass("in");
		//fix height
		$("li.search-field input").css("height", "35px");
		$(".chosen-single").css("height", "35px");		
	});
</script>