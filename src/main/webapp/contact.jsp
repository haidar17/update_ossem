<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="contact.title"/></title>
</head>
<body>
    <div class="jumbotron">
      <div class="container">
        <h1><fmt:message key="contact.heading"/></h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
      	<div class="form-group">
      		<label>sales@ossem.my</label>
      	</div>
      </div>
    </div>
</body>    