package com.ag.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.ag.model.ContactPerson;
import com.ag.model.Email;

public class ContactDaoTest extends BaseDaoTestCase{
	@Autowired
	private ContactDao dao;
	
	@Test
	//@Rollback(value=false)
	public void testAddEmailAddress(){
		Email email = new Email();
	    email.setContactValue("hamid@microsun-tech.com");
	    
	    dao.save(email);
	}
}
