package com.ag.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ag.model.OfficeAddress;

public class AddresssDaoTest extends BaseDaoTestCase {
	@Autowired
	private AddresssDao dao;
	
	@Test
	//@Rollback(false)
	public void testAddNewOfficeAddress(){
		OfficeAddress officeAddress = new OfficeAddress();
		officeAddress.setAddress("2, Jalan Warisan Megah 1/8");
		officeAddress.setStreet("Kota Warisan");
		officeAddress.setCity("Sepang");
		officeAddress.setPostalCode("43900");
		
		Object address = dao.save(officeAddress);
		
		assertNotNull(address);
		assertTrue(address instanceof OfficeAddress);
	}
}
