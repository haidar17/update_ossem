package com.ag.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ag.model.BusinessCustomer;
import com.ag.model.ContactPerson;
import com.ag.model.Customer;
import com.ag.model.IndividualCustomer;
import com.ag.model.Party;
import com.ag.model.Website;

public class CustomerDaoTest extends BaseDaoTestCase {

	@Autowired
	private CustomerDao dao;
	
	@Test
	//@Rollback(value=false)
	public void testAddBusinessCustomer(){
		ContactPerson contactPerson = new ContactPerson("Hamid", "Seleman", "730917-14-5303");
		Website website = new Website("http://www.ossem.my");
		BusinessCustomer bc = new BusinessCustomer("Microsun Tech", "0000-X", contactPerson);
		bc.setWebsite(website);
		
		Party party = dao.save(bc);
		
		assertNotNull(party);
		assertTrue(party instanceof BusinessCustomer);		
	}
	
	@Test
	//@Rollback(value=false)
	public void testAddIndividualCustomer(){
		ContactPerson contactPerson = new ContactPerson("Hamid", "Seleman", "730917-14-5303");
		IndividualCustomer bc = new IndividualCustomer("Hamid", "Seleman",  "IndividualCustomer", contactPerson);
		
		Party party = dao.save(bc);
		
		assertNotNull(party);
		assertTrue(party instanceof IndividualCustomer);		
	}	
	
	@Test
	public void testGetCustomers(){
		List<Customer> result = dao.getAll();
		
		assertNotNull(result);
		assertTrue(result.size() == 2);
	}
}
